<%@page session="true"%>
<%
if(session==null || session.getAttribute("user")==null)
{
response.sendRedirect("welcome.jsp");
}
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
    
    
<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(value, colname) {

			var phoneRE=/\+94\d{9}$/;
			var phoneRX=/\d{10}$/;
			
			if(value.length==10 && value.match(phoneRX)){
				
				return [true,""];
			}
			else if(value.length==12 && value.match(phoneRE)){
				return [true,""];
			}
			else{
				return [false,"Mobile No is not valid"];
			}
			
		}

				    	
		    	 jQuery("#results").jqGrid({
				       	url:'ViewInstantSMS?',
				    	datatype: "json",
				       	colNames:['ID', 'Mobile No', 'Content','Date','Time','Status'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:80,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"mobile",index:"mobile",width:80,editable:true,editrules: {custom:true,custom_func:mobilecheck,required: true,number:true},editoptions: { size : 30,maxlength:12 }},
				              {name:"content",index:"content",width:200,editable:true ,editrules:{},editoptions:{maxlength:160,rows:"7",cols:"30"},edittype: 'textarea'},
				              {name:"date",index:"date",width:80,editable:false},
				              {name:"time",index:"time",width:80,editable:false},
				              {name:"status",index:"status",width:120,editable:false}			       		
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: true,
				        multiselect:false,
				        sortorder: "desc",
				        pager:'#pager',
				        editurl:"InstantSMSEdit",
				        caption:"Instant SMS",
				        loadError: function(xhr,status,error){alert(status+" "+error);}
		    	 		
		    	 });
		    	 
		    	 //jQuery("#results").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		    	 
		    	 
		    	 jQuery("#results").jqGrid('navGrid',"#pager",{edit:false,addicon: "ui-icon-mail-closed",refresh:false,search:false}, //options
			   			  {}, // edit options
			   			  {height:215,reloadAfterSubmit:true}, // add options
			   			  {reloadAfterSubmit:false}, // del options
			   			  {} // search options
			   			  );
		    	 	
	
});

</script>

<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		
		
		
		
	});
	
	</script>


<script type="text/javascript">
	$(function(){

		$("#repeat").buttonset();
		$("#btnLogout").button({icons: {primary: "ui-icon-bookmark"}});
		
		});
</script>

<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Send SMS</title>

</head>
<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">
<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center" align="center">
	
	<table>
		<tr>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all"> 
			
				<span id="repeat"> 
					<input type="radio" id="repeat0" name="repeat" checked="checked" /><label for="repeat0">Send SMS</label> 
					<input type="radio" id="repeat1" name="repeat" onclick="return document.location='ScheduledSMS.jsp';"/><label for="repeat1"><a href=ScheduledSMS.jsp>Schedule SMS</a></label> 
					<input type="radio" id="repeat2" name="repeat" onclick="return document.location='Customer.jsp';"/><label for="repeat2"><a href=Customer.jsp>Manage Customer</a></label> 
					<input type="radio" id="repeat3" name="repeat" onclick="return document.location='Group.jsp';"/><label for="repeat3"><a href=Group.jsp>Manage Groups</a></label>
					<input type="radio" id="repeat4" name="repeat" onclick="return document.location='ChangePassword.jsp';"/><label for="repeat4"><a href=ChangePassword.jsp>Settings</a></label>
				</span> 
			</span> 
			</td>
			<td style="width: 5px;"></td>
			<td>
			<span id="toolbar" class="ui-state-highlight ui-corner-all">
			<%=session.getAttribute("user") %> <button class="ui-state-default" type="button" id="btnLogout" onclick="return document.location='logout.jsp';" value="edit" >logout</button>
			</span>
			</td>
			<td></td>
		</tr>
	</table>
	
	<br/><br/>
	
	
	<div style="padding-bottom: 50px;" id="searchresults">
		
	<table id="results" width="700"></table>
	<div id="pager"> </div>	
		
	
	</div>
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
	
</div>
</body>
</html>