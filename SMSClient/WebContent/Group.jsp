<%@page session="true"%>
<%
if(session==null ||session.getAttribute("user")==null)
{
response.sendRedirect("welcome.jsp");
}
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
<script type="text/javascript">
$(document).ready(function() {

	var gid;
	var icid;
	var cid;
	
});

</script>
    
<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(value, colname) {
			if (value.length != 10) 
			   return [false,"Mobile no is not valid"];
			else 
			   return [true,""];
		}

		

				    	
		    	 jQuery("#results").jqGrid({
				       	url:'EditCustomer',
				    	datatype: "json",
				       	colNames:['ID','Name', 'Mobile No'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:50,search:true,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"name",search:true,index:"name",width:150,search:true,editable:true,editrules: { edithidden: true,required: true }},
				              {name:"mobile",index:"mobile",width:80,editable:true,search:true,editrules: {custom:true,custom_func:mobilecheck,required: true,number:true},editoptions: { size : 10,maxlength:10 }}
				              
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: true,
				        multiselect:false,
				        sortorder: "asc",
				        pager:'#pager',
				        caption:"Assigned Customers",
				        onSelectRow: function(id){		    		
					    	var selr = jQuery('#results').jqGrid('getGridParam','selrow');
					    	cid=selr;
				        },
				        loadError: function(xhr,status,error){alert(status+" "+error);}
		    	 });
		    	 
		    	 //jQuery("#results").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		    	 
		    	 jQuery("#results").jqGrid('navGrid',"#pager",{add:false,edit:false,del:false,refresh:false}, //options
		   			  {height:215,reloadAfterSubmit:true}, // edit options
		   			  {height:215,reloadAfterSubmit:true}, // add options
		   			  {reloadAfterSubmit:false}, // del options
		   			  {} // search options
		   			  );
		    	
	
});

</script>

<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(value, colname) {
			if (value.length != 10) 
			   return [false,"Mobile no is not valid"];
			else 
			   return [true,""];
		}

		

				    	
		    	 jQuery("#iresults").jqGrid({
				       	url:'EditCustomer',
				    	datatype: "json",
				       	colNames:['ID','Name', 'Mobile No'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:50,search:true,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"name",search:true,index:"name",width:150,search:true,editable:true,editrules: { edithidden: true,required: true }},
				              {name:"mobile",index:"mobile",width:80,editable:true,search:true,editrules: {custom:true,custom_func:mobilecheck,required: true,number:true},editoptions: { size : 10,maxlength:10 }}
				              
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: true,
				        multiselect:false,
				        sortorder: "asc",
				        pager:'#ipager',
				        caption:"Other Customers",
				        onSelectRow: function(id){		    		
					    	var selr = jQuery('#iresults').jqGrid('getGridParam','selrow');
					    	icid=selr;
				        },
				        loadError: function(xhr,status,error){alert(status+" "+error);}
		    	 });
		    	 
		    	 //jQuery("#results").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		    	 
		    	 jQuery("#iresults").jqGrid('navGrid',"#ipager",{add:false,edit:false,del:false,refresh:false}, //options
		   			  {height:215,reloadAfterSubmit:true}, // edit options
		   			  {height:215,reloadAfterSubmit:true}, // add options
		   			  {reloadAfterSubmit:false}, // del options
		   			  {} // search options
		   			  );
		    	
	
});

</script>


<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(value, colname) {
			if (value.length != 10) 
			   return [false,"Mobile no is not valid"];
			else 
			   return [true,""];
		}
		function gridReload(gid){

			jQuery("#results").jqGrid('setGridParam',{url:"RegEditCustomer?keyword="+gid}).trigger("reloadGrid");
		}
		function gridinReload(gid){

			jQuery("#iresults").jqGrid('setGridParam',{url:"UnRegEditCustomer?keyword="+gid}).trigger("reloadGrid");
		}
				    	
		    	 jQuery("#groups").jqGrid({
				       	url:'ViewGroups',
				    	datatype: "json",
				       	colNames:['ID','Name','Description'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:50,search:true,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"name",search:true,index:"name",width:150,search:true,editable:true,editrules: { edithidden: false,required: true },editoptions:{size:30}},
				              {name:"description",index:"description",width:80,editable:true,search:true,hidden:true,editrules: {edithidden: true},editoptions: {size : 10,maxlength:160,rows:"7",cols:"30"},edittype: 'textarea'}
				              			       		
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: false,
				        multiselect:false,
				        sortorder: "asc",
				        pgbuttons: false,
				        pgtext: null,
				        pager:'#gpager',
				        editurl:"GroupEdit",
				        caption:"Groups",
				        gridComplete: function (id) {
				        	var top_rowid = $('#groups tbody:first-child tr:nth-child(2)').attr('id');
					        $("#groups").setSelection(top_rowid);
					        },
				        onSelectRow: function(id){		    		
					    	var selr = jQuery('#groups').jqGrid('getGridParam','selrow');
					    	//alert(selr);
					    	gid=selr;
					    	gridReload(selr);
					    	gridinReload(selr);
				        },
				        loadError: function(xhr,status,error){alert(status+" "+error);
				        
					    }
		    	 });
		    	 
		    	 //jQuery("#results").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		    	 
		    	 jQuery("#groups").jqGrid('navGrid',"#gpager",{add:true,edit:true,del:true,refresh:false,search:false}, //options
		   			  {height:215,reloadAfterSubmit:true}, // edit options
		   			  {height:215,reloadAfterSubmit:true}, // add options
		   			  {reloadAfterSubmit:false}, // del options
		   			  {} // search options
		   			  );
		    	
	
});

</script>

<script type="text/javascript">
	$(function(){

		$("#repeat").buttonset();
		$("#btnLogout").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnl").button({icons: {primary: "ui-icon-seek-prev"}});
		$("#btnr").button({icons: {primary: "ui-icon-seek-next"}});
		
		});
</script>


<script type="text/javascript">

	function assign(){

	

		

		$(function(){ //jquery function
			
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "GCEdit",
				data: {operation : "add" ,gid : gid , cid : icid}
			
			});
				
			
			jq.success(function (response,status){

				
				jQuery("#results").jqGrid('setGridParam',{url:"RegEditCustomer?keyword="+gid}).trigger("reloadGrid");
				jQuery("#iresults").jqGrid('setGridParam',{url:"UnRegEditCustomer?keyword="+gid}).trigger("reloadGrid");

				
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				
			});

		});

	}

</script>

<script type="text/javascript">

	function remove(){

		

		$(function(){ //jquery function
			
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "GCEdit",
				data: {operation : "del" ,gid : gid , cid : cid}
			
			});
				
			
			jq.success(function (response,status){

				
				jQuery("#results").jqGrid('setGridParam',{url:"RegEditCustomer?keyword="+gid}).trigger("reloadGrid");
				jQuery("#iresults").jqGrid('setGridParam',{url:"UnRegEditCustomer?keyword="+gid}).trigger("reloadGrid");

				
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				
			});

		});
	
	}

</script>

<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Customers</title>

</head >
<body onload="getuser();noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">
<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center" align="center">

	<table>
		<tr>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all"> 
				<span id="repeat"> 
					<input type="radio" id="repeat0" name="repeat" onclick="return document.location='InstantSMS.jsp';"/><label for="repeat0" ><a href=InstantSMS.jsp>Send SMS</a></label> 
					<input type="radio" id="repeat1" name="repeat" onclick="return document.location='ScheduledSMS.jsp';"/><label for="repeat1"><a href=ScheduledSMS.jsp>Schedule SMS</a></label> 
					<input type="radio" id="repeat2" name="repeat" onclick="return document.location='Customer.jsp';"/><label for="repeat2"><a href=Customer.jsp>Manage Customer</a></label> 
					<input type="radio" id="repeat3" name="repeat" checked="checked"/><label for="repeat3">Manage Groups</label>
					<input type="radio" id="repeat4" name="repeat" onclick="return document.location='ChangePassword.jsp';"/><label for="repeat4"><a href=ChangePassword.jsp>Settings</a></label>
				</span> 
			</span> 
			</td>
			<td style="width: 5px;"></td>
			<td>
			<span id="toolbar" class="ui-state-highlight ui-corner-all">
			<%=session.getAttribute("user") %> <button class="ui-state-default" type="button" id="btnLogout" onclick="return document.location='logout.jsp';" value="edit" >logout</button>
			</span>
			</td>
			<td></td>
		</tr>
	</table>
	
	<br/><br/>
	
	<table>
	<tr>
	
	<td>
	
	<div style="padding-bottom: 50px; margin-right: 30px;" id="groupresults">
		
	<table id="groups" width="100"></table>
	<div id="gpager"> </div>	
			
	</div>
	
	</td>
	
	<td>
	<div style="padding-bottom: 50px;" id="searchresults">
		
	<table id="results" width="700"></table>
	<div id="pager"> </div>	
			
	</div>
	</td>
	
	<td>
	<table>
	<tr>
	<td>
	<button class="ui-state-default" style="height: 20px;" type="button" id="btnl" onclick="assign();" value="edit" ></button>
	</td>
	</tr>
	<tr>
	<td>
	<button class="ui-state-default" style="height: 20px;" type="button" id="btnr" onclick="remove();" value="edit" ></button>
	</td>
	</tr>
	</table>
	</td>
	
	<td>
	<div style="padding-bottom: 50px;" id="inverseresults">
		
	<table id="iresults" width="700"></table>
	<div id="ipager"> </div>	
			
	</div>
	</td>
	
	
	</tr>
	</table>
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
	
</div>
</body>
</html>