package pojo;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class ScheduledMessage implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long messageId;
	private String mobileNo;
	private String messagecontent;
	private String datescheduled;
	private String timescheduled;
	private String status;
	private long userId;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getMessagecontent() {
		return messagecontent;
	}
	public void setMessagecontent(String messagecontent) {
		this.messagecontent = messagecontent;
	}
	public String getDatescheduled() {
		return datescheduled;
	}
	public void setDatescheduled(String datescheduled) {
		this.datescheduled = datescheduled;
	}
	public String getTimescheduled() {
		return timescheduled;
	}
	public void setTimescheduled(String timescheduled) {
		this.timescheduled = timescheduled;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	



}
