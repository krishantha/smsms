package orm;
import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.HibernateTemplate;
import pojo.InstantMessage;


public class InstantSMSORM {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final InstantMessage iSMS) {
		hibernateTemplate.save(iSMS);
		
	}
	
	public void update(final InstantMessage iSMS){
		
		hibernateTemplate.update(iSMS);		
	}
	
	@SuppressWarnings("unchecked")
	public List<InstantMessage> fetchALLData() {
		return hibernateTemplate.loadAll(InstantMessage.class);
	}
	
	public void delete(final InstantMessage iSMS){
		
		hibernateTemplate.delete(iSMS);		
	}
	
	@SuppressWarnings("unchecked")
	public List<InstantMessage> getUserSMS(final long user){
				
		String queryString = "FROM instantmessage a WHERE a.userid = :user";
		return hibernateTemplate.findByNamedParam(queryString, "user", user);
		
	}
	
}
