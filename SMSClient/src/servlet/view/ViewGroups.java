package servlet.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import orm.GroupORM;
import pojo.Groups;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class EditCustomerClass
 */
@WebServlet("/ViewGroups")
public class ViewGroups extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewGroups() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		long userId=(Long) request.getSession().getAttribute("userid");
	
			
			
		int page=Integer.parseInt(request.getParameter("page"));
		
		int total_pages=0;
		
		System.out.println(page);
		
		
		int rid=Integer.parseInt(page-1+"0");
		
		
		
		JSONObject responsedata=new JSONObject();
		
		JSONArray cellarray=new net.sf.json.JSONArray();
	    		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		GroupORM groupORM=(GroupORM)context.getBean("groupORMBean");
		List <Groups> groupList =groupORM.fetchALLData();
		

		int count=0;
		
		
		try 
		{
			
			
									
		 	request.getSession().setAttribute("groupList",groupList);
		 	
	      
	        JSONArray cell=new net.sf.json.JSONArray();
	        JSONObject cellobj=new net.sf.json.JSONObject();
	        

			
	        
			if(!groupList.isEmpty())
			{
			
				for(Groups group:groupList)
				{
					
					long userid=group.getUserId();
					
					if(userid==userId){
						
					
					cellobj.put("id", group.getGroupId());
					cell.add(group.getGroupId());
					cell.add(group.getGroupName());
					cell.add(group.getDescription());
										
					
					cellobj.put("cell", cell);
					cell.clear();
					cellarray.add(cellobj);
						
					}
				}
					if(count>10){
					
					total_pages=count/10;
					
					if(count%10>0){
						
						total_pages++;
						
					}
								
				}
				
				if(page>total_pages){
					
					page=total_pages;
				}
				responsedata.put("total",total_pages);
		        responsedata.put("page",page);
		        responsedata.put("records",count);
				responsedata.put("rows",cellarray);

			}
		}
				
	
		catch (Exception e)
		{
			
		}
	
		
		response.getWriter().write(responsedata.toString());
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
	

}
