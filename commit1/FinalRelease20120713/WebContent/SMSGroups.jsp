<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    
    <!-- imported CSS files -->
	<link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link rel="stylesheet" href="css/demos.css"> 
    <link rel="Stylesheet" href="css/dialog.css" type="text/css" />
    <link rel="stylesheet" href="css/body.css">
    

    
	<script type="text/javascript">
		
	function loadtable(){
		
	$(function(){
		var jx=$.ajax({

			type : "GET",
			url : "LoadGroups"			
						
				});
				jx.success(function (response, status) {
					
					$("#groupTable").html(response);
					
					
				});

				jx.error(function(request, error) {
					
			});
	});
	
	}
	
	</script>
	
	<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var name = $( "#name" ),
			des = $( "#des" ),
			allFields = $( [] ).add( name ).add( des ),
			tips = $( ".validateTips" );

		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be between " +
					min + " and " + max + "." );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}
		
		$( "#dialogCreateGroup" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				
				"Add Group": function() {
					var bValid = true;
					allFields.removeClass( "ui-state-error" );

					bValid = bValid && checkLength( name, "group name", 3, 25 );
					bValid = bValid && checkLength( des, "description", 0, 80 );
					

					bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Group name may consist of a-z, 0-9, underscores, begin with a letter." );
					// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
					
					var grpname=$("#name").val();
					var description=$("#des").val();
				
				
					if ( bValid ) {
				
					var jq=$.ajax({
					
						type: "GET",
						url: "AddGroup",
						data: {groupname : grpname , desc : description}
				
						});
								
					
						jq.success(function (response,status){
							if(response=="groupExist"){
								
								name.addClass("ui-state-error");
								updateTips( "Group name \""+grpname+"\" is already exists, try another one." );
								
							}
							else{
								$().toastmessage('showSuccessToast', "Group Added!");
								$( "#dialogCreateGroup" ).dialog( "close" );
								$("#groupTable").html(response);
								
							}
							});
						
						jq.error(function (request,error){
								$().toastmessage('showErrorToast', "Failed to add group!");
						}); 
						
					}
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
				
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#btnAddGroup" )
			.button()
			.click(function() {
				document.getElementById('message').style.visibility='hidden';
				$( "#dialogCreateGroup" ).dialog( "open" );
			});
	});
	
	</script>
	
	
	<script type="text/javascript">
	
	$(function(){
		
		$("#btnDeleteGroup").click(function(){
			
			document.getElementById('message').style.visibility='hidden';
			
			if($('input:radio[name=radio]:checked').size()==0){
				
				$("#errorMessage").html("<lable style=\"font-size: 12px;font-weight: bold\";>Please select a group to delete</lable><br/><label>&nbsp;</label>");
				document.getElementById('message').style.visibility='visible';	
				
			}
			else{
				
				$( "#dialog:ui-dialog" ).dialog( "destroy" );
				
				$( "#dialogConfirmDelete" ).dialog({
					resizable: false,
					show:"drop",
					hide:"drop",
					height:140,
					modal: true,
					buttons: {
						
						"Delete": function() {
							$( this ).dialog( "close" );
							var rad=$('input:radio[name=radio]:checked').val();
							
							var del=$.ajax({
								
								type : "GET",
								url : "DelGrp",
								data : {dgrp : rad}										
							});

							del.success(function(response,status){			
								if(response=="notExist"){
									$().toastmessage('showErrorToast',"The system Cannot find the group!");
										
								}else{
									$("#groupTable").html(response);
									$().toastmessage('showSuccessToast', "Group deleted!");
									
								}
							});

							del.error(function(status,error) {
									$().toastmessage('showErrorToast',"Failed to delete group!");
							});
						},
						Cancel: function() {
							$( this ).dialog( "close" );
						}
						
					}
				});
				
				
				
			}
			
			
			
			
			
		});
		
	});
	
	</script>
	
	<script type="text/javascript">
		function highlight(x)
		{
			
			document.getElementById("x").style.backgroundColor="green";
		}
	</script>
	
	<script type="text/javascript">
	$(function(){
	
		$("#btnEditGroup").click(function(){
		
			document.getElementById('message').style.visibility='hidden';
			if($('input:radio[name=radio]:checked').size()==0){
				
				$("#errorMessage").html("<lable style=\"font-size: 12px;font-weight: bold\";>Please select a group to edit</lable><br/><label>&nbsp;<label/");
				document.getElementById('message').style.visibility='visible';	
			}
			else{
				var editid=$('input:radio[name=radio]:checked').val();
				
				var mn=$.ajax({

					type : "GET",
					url : "LoadGroups"			
								
						});
						mn.success(function (response, status) {
							
							$("#groupTable").html(response);
							
							
						});

						mn.error(function(request, error) {
							
					});
				
				
				window.location.href="EditGroup.jsp?groupid="+editid;
				
			}
		
		});
	
	});
	</script>
	

	<!-- script for adding icons for buttons -->
	<script type="text/javascript">
	
	$(function(){
		
		$("#btnAddGroup").button({icons: {primary: "ui-icon-circle-plus"}});
		$("#btnDeleteGroup").button({icons: {primary: "ui-icon-trash"}});
		$("#btnEditGroup").button({icons: {primary: "ui-icon-pencil"}});
	});
	
	</script>
	
	

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>SMS Management System</title>
</head>
<body onload="loadtable();">

<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">

			<div class="ui-widget">
				<div class="ui-widget-header">Manage customer groups</div>
				
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

						<form name="Groups" id="formGroup">
						
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button class="ui-state-default" type="button" id="btnAddGroup" value="addnew" >Add new group</button>
						<button class="ui-state-default" type="button" id="btnDeleteGroup" value="delete" >Delete group</button>
						<button class="ui-state-default" type="button" id="btnEditGroup" value="edit" >Edit group</button>
						
						<div align="center">
						<div style="visibility:hidden; width: 500px;" id="message">
							<div class="ui-widget message-container">
								<div class="ui-state-error ui-corner-all">
									<span class="ui-icon ui-icon-alert" style="float:left;"></span>
									<span id="errorMessage" class="message-text"></span>
								</div>
							</div>
						</div>
						</div>
						
						<div align="center" id="groupTable"></div>
						
						
						</form>
						</div>
						
						<div id="dialogConfirmDelete" title="Delete group!" style="display: none;">
							<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This group will be permanently deleted and cannot be recovered. Are you sure?</p>
						</div>
						
						
						<div align="left" id="dialogCreateGroup" title="Create new group" style="display: none;">
							<p align="left" class="validateTips">Name field is required.</p>

						<form>
							<fieldset>
								<label for="name">Group Name</label>
								<input type="text" name="name" id="name" maxlength="25" class="text ui-widget-content ui-corner-all" />
								<label for="des">Description</label>
								<input type="text" name="des" id="des" maxlength="180" class="text ui-widget-content ui-corner-all" />
							</fieldset>
						</form>
						</div>
						
					</div>
					
					

				</div>
			</div>
		</div>
</div>

						
</body>
</html>