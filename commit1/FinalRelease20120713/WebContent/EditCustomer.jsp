<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
body{
width:600px;
font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
}

</style>


<link rel="Stylesheet" href="css/jquery-ui-1.8.18.custom.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>

<script type="text/javascript">


$(function() {
   
	$("#autocomplete").autocomplete({
        source:"AutoCompleteCustomer",
        minLength: 2,
        select: function(event, ui) {	
        	$("#button_search").click();
        }
		
    });   
	
	
	$("#button_search").click(function(){
		
		var value=$("#autocomplete").attr('value');
		var jqxhr =
		    $.ajax({
		        url: "EditCustomer",
		      	type: "GET",
		      	data:"keyword="+value
		    })
		    .success (function(response,status) { $('#searchresults').html(response);
		    
		    })
				
		    .error   (function(request,error)     { $("#searchresults").html(request) ; });	
		
	});		
	
});

</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Customer</title>
</head>
<body>
<div class="ui-widget">
	<div class="ui-widget-header">
	Edit Customer Profile
	</div>
	
	<div class="ui-widget-content" style="min-height:600px;">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;">
	Search for :&nbsp;&nbsp;<input id="autocomplete" style="z-index: 100; position: relative;width:230px;" title="Search for customers" />&nbsp;&nbsp;
	<button id="button_search" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"><span class="ui-button-text">Search</span></button><br><br>
	Search Results<br><br>
	<div id="searchresults" style="width:560px;">
	</div>
	</div>
	</div>
</div>
<div id="dialog-form" title="Create new user">
	<p class="validateTips">All form fields are required.</p>

	<form>
	<fieldset>
		<label for="name">Name</label>
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
		<label for="email">Email</label>
		<input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" />
		<label for="password">Password</label>
		<input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />
	</fieldset>
	</form>
</div>
</body>
</html>