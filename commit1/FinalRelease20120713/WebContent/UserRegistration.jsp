<%@page import="java.util.ArrayList"%>
<%@page import="com.sms.userRegistration.RoleOptionsService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.toastmessage.js"></script>
<link href="css/sunny/jquery-ui-1.8.20.custom.css" rel="stylesheet"	type="text/css" />
<link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
<link rel="stylesheet" href="css/demos.css">
<link rel="stylesheet" type="text/css" media="screen" href="grid/css/ui.jqgrid.css" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Registration</title>

<!-- Hiding all error messages  -->
<script type="text/javascript">

	$(function() {
		document.getElementById('userNameError').style.visibility = 'hidden';
		document.getElementById('userFullNameError').style.visibility = 'hidden';
		document.getElementById('userPasswordError').style.visibility = 'hidden';
		document.getElementById('userPasswordConfirmError').style.visibility = 'hidden';
		document.getElementById('userNumberError').style.visibility = 'hidden';
		document.getElementById('AuthenticationError').style.visibility = 'hidden';
	});
	
</script>

<!-- Loading authentication levels to the form -->
<script type="text/javascript">

	$(function() {
		$.ajax({
			type : "GET",
			url : "RoleOptionsClass",
			data : {
				type : "getRoles"
			}
		}).success(function(response, status) {
			$('#levels').html(response);
		}).error(function() {
			
			$().toastmessage('showToast',{
				text : 'Cannot load user roles.This may be happened due to the database connection falier.Please try again.',
				sticky   : false,
				stayTime:  7000,
			    type     : 'error'
			}
					);
		});
	});
	
</script>

<!-- Register customer after doing the validations-->
<script type="text/javascript">

	$(function() {
		$("#btnRegister").click(function() {
			
			var name = $("#txtUserName").val();
			var fName = $("#txtUserFullName").val();
			var pw = $("#txtUserPassword").val();
			var no = $("#txtUserNumber").val();
			var checkedBoxArray = [];
			var checkResult = "checkedBoxArray";

			$('#levels :checked').each(function() {
				checkedBoxArray.push($(this).val());
			});
			
			var checkedBoxString = "" + checkedBoxArray;
			var input = $("#txtUserName").val();
			//Checking wether user name is available
			$.ajax({
				type : "GET",
				url : "UserClass",
				data : {
					msg : input,
					type : 'userNameCheck'
					},
					async : false
			})
			.success(function(response, status) {
				if (response == "true") {
					checkResult = "true";
				}
				else {
					checkResult = "false";
				}
			})
			.error(function() {});
			
			if (checkResult == "true") {
				
				$("#spnUserNameError").text("");
				$("#spnUserNameError").append("<label>&nbsp;&nbsp;The user name is already in use.Please enter a different name.</label><br><label>&nbsp;</label>");
				document.getElementById('userNameError').style.visibility = 'visible';
				
			}
			else if ($("#txtUserName").val() == "") {
				
				$("#spnUserNameError").text("");
				$("#spnUserNameError").append("<label>&nbsp;&nbsp;Please enter an user name.</label><br><label>&nbsp;</label>");
				document.getElementById('userNameError').style.visibility = 'visible';
			}
			else if ($("#txtUserFullName").val() == "") {
				
				$("#spnUserFullNameError").text("");
				$("#spnUserFullNameError").append("<label>&nbsp;&nbsp;Please enter your full-name.</label><br><label>&nbsp;</label>");
				document.getElementById('userFullNameError').style.visibility = 'visible';
			}
			else if ($("#txtUserPassword").val() == "") {
				
				$("#spnUserPasswordError").text("");
				$("#spnUserPasswordError").append("<label>&nbsp;&nbsp;Please enter a password.</label><br><label>&nbsp;</label>");
				document.getElementById('userPasswordError').style.visibility = 'visible';

			} 
			else if ($("#txtUserPassword").val() != $("#userPasswordConfirm").val()) {
				
				$("#spnUserPasswordConfirmError").text("");
				$("#spnUserPasswordConfirmError").append("<label>&nbsp;&nbsp;These passwords donot match.</label><br><label>&nbsp;</label>");
				document.getElementById('userPasswordConfirmError').style.visibility = 'visible';
			}
			else if ($("#txtUserNumber").val() == "") {
				
				$("#spnUserNumberError").text("");
				$("#spnUserNumberError").append("<label>&nbsp;&nbsp;Please enter your mobile number.</label><br><label>&nbsp;</label>");
				document.getElementById('userNumberError').style.visibility = 'visible';
			}
			else if ($("#txtUserNumber").val().length < 9) {
				
				$("#spnUserNumberError").text("");
				$("#spnUserNumberError").append("<label>&nbsp;&nbsp;Please enter a correct mobile number.You dont need to add 0 to begining.Ex:- 711234567 </label><br><label>&nbsp;</label>");
				document.getElementById('userNumberError').style.visibility = 'visible';
			}
			else if (isNaN($("#txtUserNumber").val())){
				
				$("#spnUserNumberError").text("");
				$("#spnUserNumberError").append("<label>&nbsp;&nbsp;The mobile number should not contain characters.</label><br><label>&nbsp;</label>");
				document.getElementById('userNumberError').style.visibility = 'visible';
				
			}
			else if (checkedBoxString == "") {
				
				$("#spnALevelError").text("");
				$("#spnALevelError").append("<label>&nbsp;&nbsp;Please select at least one role option.</label><br><label>&nbsp;</label>");
				document.getElementById('AuthenticationError').style.visibility = 'visible';
				
			}
			else {
				$.ajax({
					type : "GET",
					url : "UserClass",
					data : {
						type : "register",
						userName : name,
						fullName : fName,
						password : pw,
						number : no,
						levels : checkedBoxString
					}
				})
				.success(function(response, status) {
					
					$().toastmessage('showSuccessToast',"User registration completed.");
					document.getElementById("userRegistrationForm").reset();
					
				})
				.error(function() {
					$().toastmessage('showWarningToast',"Unable to register an user, Please try again.");
				});
			}			
		});
	});
	
</script>

<!-- Setting button icons -->
<script type="text/javascript">

	$(function() {		
		$("#btnRegister").button({
			icons : {primary : "ui-icon-person"}
		});
		$("#btnReset").button({
			icons : {primary : "ui-icon-refresh"}
		});
		$("#BtnCancel").button({
			icons : {primary : "ui-icon-close"}
		});
	});
	
</script>

<!-- Hiding error messages after displayed -->
<script type="text/javascript">

	$(function() {
		$("#txtUserName").click(function() {
			document.getElementById('userNameError').style.visibility = 'hidden';
		});
	});
	$(function() {
		$("#txtUserFullName").click(function() {document.getElementById('userFullNameError').style.visibility = 'hidden';
		});
	});
	$(function() {
		$("#txtUserPassword").click(function() {
			document.getElementById('userPasswordError').style.visibility = 'hidden';
		});
	});
	$(function() {
		$("#txtUserNumber").click(	function() {
			document.getElementById('userNumberError').style.visibility = 'hidden';
		});
	});
	$(function() {
		$("#userPasswordConfirm").click(function() {
			document.getElementById('userPasswordConfirmError').style.visibility = 'hidden';
		});
	});
	$(function() {
		$("#levels").click(function() {
			document.getElementById('AuthenticationError').style.visibility = 'hidden';
		});
	});
	
</script>

<!-- Validating user name while typing.
If user name already exists then text box border color will change to red -->
<script type="text/javascript">

	$(function() {

		$("#txtUserName").keyup(function() {

			var input = $("#txtUserName").val();
			$.ajax({
				type : "GET",
				url : "UserClass",
				data : {
					msg : input,
					type : 'userNameCheck'
				}

			})

			.success(function(response, status) {

				if (response == "true") {

					$("#txtUserName").css({
						"border" : "1px solid red"
					});

				} else {

					$("#txtUserName").css({
						"border" : "1px solid #736F6E"
					});

				}

			});

		});
	});
	
</script>

</head>

<body>

	<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">
			<div class="ui-widget">

				<div class="ui-widget-header">User Registration</div>
				<div class="ui-widget-content">
					<div style="padding: 10px 10px 10px 10px;">

						<Form id="userRegistrationForm">
							<br><br>
							User Name:
							<br>
							<input type="text" id="txtUserName" size="50" >
							
							<div style="visibility: visible; width: 290px;"
								id="userNameError">
								<div class="ui-widget message-container">
									<div class="ui-state-error ui-corner-all">
										<span class="ui-icon ui-icon-alert" style="float: left;">
										</span><span id="spnUserNameError" class="message-text">
										<label>&nbsp;&nbsp;These passwords donot match.</label>
										<br>
										<label>&nbsp;</label> </span>
									</div>
								</div>
							</div>

							Full Name: 
							<br> 
							<input type="text" id="txtUserFullName" size="50" >
							
							<div style="visibility: visible; width: 250px;" id="userFullNameError">
								<div class="ui-widget message-container">
									<div class="ui-state-error ui-corner-all">
										<span class="ui-icon ui-icon-alert" style="float: left;">
										</span><span id="spnUserFullNameError" class="message-text">
										<label>&nbsp;&nbsp;These passwords donot match.</label>
										<br>
										<label>&nbsp;</label></span>
									</div>
								</div>
							</div>

							Enter New Password: 
							<br>
							<input type="password" id="txtUserPassword" size="50" >
							
							<div style="visibility: visible; width: 250px;" id="userPasswordError">
								<div class="ui-widget message-container">
									<div class="ui-state-error ui-corner-all">
										<span class="ui-icon ui-icon-alert" style="float: left;">
										</span><span id="spnUserPasswordError" class="message-text">
										<label>&nbsp;&nbsp;These passwords donot match.</label>
										<br>
										<label>&nbsp;</label></span>
									</div>
								</div>
							</div>

							Confirm Password:
							<br>
							<input type="password" id="userPasswordConfirm" size="50" >

							<div style="visibility: visible; width: 250px;"	id="userPasswordConfirmError">
								<div class="ui-widget message-container">
									<div class="ui-state-error ui-corner-all">
										<span class="ui-icon ui-icon-alert" style="float: left;">
										</span><span id="spnUserPasswordConfirmError" class="message-text">
										<label>&nbsp;&nbsp;These passwords donot match.</label>
										<br>
										<label>&nbsp;</label></span>
									</div>
								</div>
							</div>

							Mobile Number: 
							<br>
							+94<input type="text" id="txtUserNumber" name="txtUserNumber" size="9" maxlength="9" >

							<div style="visibility: visible; width: 250px;"	id="userNumberError">
								<div class="ui-widget message-container">
									<div class="ui-state-error ui-corner-all">
										<span class="ui-icon ui-icon-alert" style="float: left;">
										</span><span id="spnUserNumberError" class="message-text">
										<label>&nbsp;&nbsp;These passwords donot match.</label><br>
										<label>&nbsp;</label></span>
									</div>
								</div>
							</div>
							
							Authentication Level:
							<br>
							<div id="levels" class="scroll_checkboxes"></div>

							<div style="visibility: visible; width: 250px;" id="AuthenticationError">
								<div class="ui-widget message-container">
									<div class="ui-state-error ui-corner-all">
										<span class="ui-icon ui-icon-alert" style="float: left;">
										</span><span id="spnALevelError" class="message-text">
										<label>&nbsp;&nbsp;These passwords donot match.</label>
										<br>
										<label>&nbsp;</label></span>
									</div>
								</div>
							</div>

							<button id="btnRegister" type="button">Register User</button>
							<button type="reset" id="btnReset">Reset</button>
							<button type="button" id="BtnCancel">Cancel</button>
							<br><br>
						</Form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>