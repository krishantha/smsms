<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    
    <!-- imported CSS files -->
	<link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link rel="stylesheet" href="css/demos.css"> 
    <link rel="Stylesheet" href="css/dialog.css" type="text/css" />
    <link rel="Stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/body.css">
    
    
    <!-- script for taking the variables from the URL.this method separates the variables parsed through URL and return the 
    value of the given key variable -->
    <script type="text/javascript">
    
    function getUrlVars() {
    	var vars = {};
    	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    		vars[key] = value;
    	
    	});
    	return vars;
    }
    
    </script>
    
    
    <!--  script for select or de-select all customers at once,  -->
    
    <script type="text/javascript">

	function chkall(name){


	if(name=="checkadd"){
		
		if(document.getElementById('addall').checked){
		
		var checkBoxList=document.getElementsByName("checkToAdd");
		
			
		for(var a=0;a<=checkBoxList.length;a++){
				
			checkBoxList[a].checked=true;
		
		}
	}
	else{
		var checkBoxList=document.getElementsByName("checkToAdd");
		
		
		for(var a=0;a<=checkBoxList.length;a++){
				
			checkBoxList[a].checked=false;
		
		}
	}
	
	
	
	}
	else if(name=="checkremove"){
		
		if(document.getElementById('removeall').checked){
				
		var checkBoxList=document.getElementsByName("checkToRemove");
		
		for(var b=0;b<=checkBoxList.length;b++){
					
			checkBoxList[b].checked=true;
			
		}
		}
		else{
			var checkBoxList=document.getElementsByName("checkToRemove");
			
			for(var b=0;b<=checkBoxList.length;b++){
						
				checkBoxList[b].checked=false;
				
			}
			
		}
			
	
	}
}

</script>
    
    
    <!-- script for loading the status bar with group name and description. group name will be taken from the URL and the
    description of that group will be retrieved from the database -->
    
    <script type="text/javascript">
		
	function loadinfo(){
	
		var grp = getUrlVars()["groupid"]; //take the group name from the URL using getUrlVars() method. 
		document.getElementById("gname").innerText=grp;
		
	$(function(){//jquery function
		
		var jx=$.ajax({//ajax call

			type : "GET",
			url : "GetDescription",
			data:{grpname : grp}
						
				});
				jx.success(function (response, status) {
					
					document.getElementById("gdesc").innerText=response;
					
					
				});

				jx.error(function(request, error) {
					
			});
	});
	
	}
	
	</script>
	
	<!-- loading the table with the customer details of selected group. this method uses an ajax call to retrieve the
	customer table which are in the selected group -->
	
	<script type="text/javascript">
	
	function loadtable(){
		var grp = getUrlVars()["groupid"];//take the group name from the URL using getUrlVars() method. 
		
		$(function(){//jquery function
			
			var jx=$.ajax({//ajax call

				type : "GET",
				url : "LoadGroupCustomers",
				data:{grpname : grp}
							
					});
					jx.success(function (response, status) {
						
						$("#tblAssignedCustomers").html(response);
						
						
					});

					jx.error(function(request, error) {
						
				});
		});
		
	}
	
	</script>
    
    
    
<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var cname = $( "#cname" ),
			des = $( "#des" ),
			allFields = $( [] ).add( name ).add( des ),
			tips = $( ".validateTips" );
		
		console.log(cname);
		
		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be between " +
					min + " and " + max + "." );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}
		
		$( "#dialogAddCustomer" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 500,
			width: 560,
			modal: true,
			buttons: {
				
				"Add to group": function() {
					document.getElementById('emessage').style.visibility='hidden';
										
					var id=0;
					var test = [];
					$("input:checkbox[name=checkToAdd]:checked").each(function()
							{
							    test.push($(this).val());
							    id=$(this).val()+","+id;
							});
					if(test.length==0){
						
						$("#errmsg").html("<label style=\"font-size: 12px; align:center; font-weight: bold;\">No customer is selected.Please select one or more.</label>");
						document.getElementById('emessage').style.visibility='visible';	
						document.getElementById("cname").value="";
						
					}
					
					else{
						
					var grp = getUrlVars()["groupid"];
				
					var jq=$.ajax({
					
					type: "GET",
					url: "AddToGroup",
					data: {groupname : grp , customers : id}
				
					});
								
					jq.success(function (response,status){
							
								$().toastmessage('showSuccessToast', "Customer(s) Added");
								$( "#dialogAddCustomer" ).dialog( "close" );
								$("#tblAssignedCustomers").html(response);
								
							});
						
						jq.error(function (request,error){
								$().toastmessage('showErrorToast', "Customer(s) not added");
						}); 				
					
					}
				},
				Cancel: function() {
					document.getElementById('emessage').style.visibility='hidden';
					document.getElementById("cname").value="";
					$( this ).dialog( "close" );
					
				}
				
			},
			close: function() {
				document.getElementById('emessage').style.visibility='hidden';
				document.getElementById("cname").value="";
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});
		$("#cname").keyup(function(){
			
				var grp = getUrlVars()["groupid"];
				var name=$("#cname").val();
				var js=$.ajax({

					type : "GET",
					url : "SearchToAdd",
					data:{grpname : grp,customer : name}
								
						});
						js.success(function (response, status) {
							
							$("#tblAddCustomer").html(response);
							
							
						});

						js.error(function(request, error) {
							
					});
			
		});
		
		$( "#btnAddCustomer" )
			.button()
			.click(function() {
				document.getElementById('message').style.visibility='hidden';
				$( "#dialogAddCustomer" ).dialog( "open" );
				
				var grp = getUrlVars()["groupid"];
				
				var jx=$.ajax({

					type : "GET",
					url : "LoadNGroupCustomers",
					data:{grpname : grp}
								
						});
						jx.success(function (response, status) {
							
							$("#tblAddCustomer").html(response);
							
							
						});

						jx.error(function(request, error) {
							
					});
				
				
			});
	});
	
	</script>
    
    
    <script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var cname = $( "#cname" ),
			des = $( "#des" ),
			allFields = $( [] ).add( name ).add( des ),
			tips = $( ".validateTips" );
		console.log(cname);
		
		
		
		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be between " +
					min + " and " + max + "." );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}
		
		$( "#dialogEditDescription" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				
				"Save": function() {
														
											
					var grp = getUrlVars()["groupid"];
					var desc=$("#udes").val();
					var jq=$.ajax({
					
					type: "GET",
					url: "UpdateDescription",
					data: {groupname : grp , description : desc}
				
					});
								
					jq.success(function (response,status){
							
								$().toastmessage('showSuccessToast', "Description Updated");
								$( "#dialogEditDescription" ).dialog( "close" );
								loadinfo();
								
							});
						
						jq.error(function (request,error){
								$().toastmessage('showErrorToast', "Description not Updated");
						}); 				
					
				}
				
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});
		
		
		$( "#btnEditDescription" ).button().click(function() {
				
				$( "#dialogEditDescription" ).dialog( "open" );
				
				var grp = getUrlVars()["groupid"];
				
				document.getElementById("uname").value=grp;
				
				
				var jx=$.ajax({

					type : "GET",
					url : "GetDescription",
					data:{grpname : grp}
								
						});
						jx.success(function (response, status) {
							
							document.getElementById("udes").value=response;
							
							
						});

						jx.error(function(request, error) {
							
					});
				
				
			});
	});
	
	</script>
    
    
    
<!-- script for adding icons for buttons -->
<script type="text/javascript">
	
	$(function(){
		
		$("#btnAddCustomer").button({icons: {primary: "ui-icon-circle-plus"}});
		$("#btnDeleteCustomer").button({icons: {primary: "ui-icon-circle-minus"}});
		$("#btnEditDescription").button({icons: {primary: "ui-icon-pencil"}});
		$("#btnBack").button({icons: {primary: "ui-icon-circle-arrow-w"}});
		
	});
	
</script>
	
    <script type="text/javascript">
    
    $(function(){
    	
    	$("#btnDeleteCustomer").button().click(function(){
    		document.getElementById('message').style.visibility='hidden';
    		var id=0;
			var test = [];
			$("input:checkbox[name=checkToRemove]:checked").each(function()
					{
					    test.push($(this).val());
					    id=$(this).val()+","+id;
					});
			if(test.length==0){
				
				$("#ermsg").html("<lable style=\"font-size: 12px;font-weight: bold\";>No customer is selected.Please select one or more.</lable><br/><label>&nbsp;<label/");
				document.getElementById('message').style.visibility='visible';	
				
			}
			
			else{
				
			var grp = getUrlVars()["groupid"];
		
			var jq=$.ajax({
			
			type: "GET",
			url: "RemoveFromGroup",
			data: {groupname : grp , customers : id}
		
			});
						
			jq.success(function (response,status){
					
						$().toastmessage('showSuccessToast', "Customer(s) removed");
						$("#tblAssignedCustomers").html(response);
						
					});
				
				jq.error(function (request,error){
						$().toastmessage('showErrorToast', "Customer(s) not removed");
				}); 				
			
			}
    		
    	});
    	
    });
    
    </script>
    
    <script type="text/javascript">
    
    $(function(){
    	
    	$("#btnBack").button().click(function(){
    		
    		window.location.href="SMSGroups.jsp";
    		
    	});
    	
    });
    
    </script>
    
    
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SMS Management System</title>
</head>
<body onload="loadinfo();loadtable();">

<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">

			<div class="ui-widget">
				<div class="ui-widget-header">Edit group</div>
				
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

						<form name="Groups" id="grp">
						
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 

							<p><span class="ui-icon ui-icon-comment" style="float: left; margin-right: .3em;"></span>

							<strong>Group name&nbsp;&nbsp;:&nbsp;</strong><label style="display: inline;" id="gname"></label>
							<strong>&nbsp;&nbsp;Description&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</strong><label style="display: inline;" id="gdesc"></label>
							
							</p>

						</div>
						<br/>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button class="ui-state-default" type="button" id="btnAddCustomer" value="addnew" >Add customer</button>
						<button class="ui-state-default" type="button" id="btnDeleteCustomer" value="delete" >Remove customer</button>
						<button class="ui-state-default" type="button" id="btnEditDescription" value="edit" >Edit Description</button>
						<button class="ui-state-default" style="float: right;" type="button" id="btnBack" value="back" >Back</button>
						
						
						<div align="center">
						<div style="visibility:hidden; width: 500px;" id="message">
							<div class="ui-widget message-container">
								<div class="ui-state-error ui-corner-all">
									<span class="ui-icon ui-icon-alert" style="float:left;"></span>
									<span id="ermsg" class="message-text"></span>
								</div>
							</div>
						</div>
						</div>
						
						<div align="center" id="tblAssignedCustomers"></div>
						
						
						</form>
						</div>
												
						<div align="left" id="dialogAddCustomer" title="Add Customers" style="display: none;">
						
						<form>
						
							<input type="text" placeholder="Search customer..." style="float: right;" name="cname" id="cname" class="text ui-widget-content ui-corner-all" /><br/>
						
						
							<fieldset>
							
								<div align="left">
									<div style="visibility:hidden;" id="emessage">
										<div class="ui-widget message-container">
											<div class="ui-state-error ui-corner-all">
												<span class="ui-icon ui-icon-alert" style="float:left;"></span>
												<span id="errmsg" class="message-text"></span>
											</div>
										</div>
									</div>
								</div>
								
								
								
								<div align="center" id="tblAddCustomer"></div>
							</fieldset>
						</form>
						</div>
						
					</div>
					<div align="left" id="dialogEditDescription" title="Edit description" style="display: none;">
							<p align="left" class="validateTips"></p>

						<form>
							<fieldset>
								<label for="name">Group Name</label>
								<input type="text" disabled="disabled" name="uname" id="uname" class="text ui-widget-content ui-corner-all" />
								<label for="des">Description</label>
								<input type="text" name="udes" id="udes" class="text ui-widget-content ui-corner-all" />
							</fieldset>
						</form>
						</div>
					

				</div>
			</div>
		</div>
</div>

</body>
</html>