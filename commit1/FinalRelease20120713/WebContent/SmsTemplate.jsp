<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.toastmessage.js"></script>
<link href="css/sunny/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
<link rel="stylesheet" href="css/demos.css">
<link rel="stylesheet" type="text/css" media="screen" href="grid/css/ui.jqgrid.css" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>SMS Template</title>

<!-- Hiding error messages -->
<script type="text/javascript">
	$(function() {
		document.getElementById('templateCategoryError').style.visibility = 'hidden';
		document.getElementById('templateNameError').style.visibility = 'hidden';
		document.getElementById('templateMsgError').style.visibility = 'hidden';
		document.getElementById('categoryNameError').style.visibility = 'hidden';
		document.getElementById('categoryDescriptionError').style.visibility = 'hidden';
		document.getElementById('templateNameError2').style.visibility = 'hidden';
		document.getElementById('categoryMessageError').style.visibility = 'hidden';
	});
</script>

<!-- Hiding confirm boxes -->
<script type="text/javascript">
	$(function() {
		$("#categoryDeleteDialog").dialog({
			autoOpen : false
		});
		$("#templateDeleteDialog").dialog({
			autoOpen : false
		});
	});
</script>

<!-- Initializing tabs -->
<script>
	$(function() {
		$("#tabs").tabs({
			fx : [ {
				opacity : 'toggle',
				duration : 'slow'
			}, {
				opacity : 'toggle',
				duration : 'slow'
			} ]
		}
	);
		CategoryFunction()
	});
</script>

<!-- Loading categories to combo-boxes -->
<script>

	function CategoryFunction() {
		
		$("#comboTemplateCategory1").html("");
		$("#comboTemplateCategory2").html("");
		$.ajax({
			type : "GET",
			url : "SmsTemplateCategoryClass",
			data : {
				type : "CategoryCombo"
			}

		})
		.success(function(response, status) {
				
			$("#comboTemplateCategory1").append(response);
			$("#comboTemplateCategory2").append(response);

		})

	}
</script>

<!-- auto complete style -->
<style>
.ui-autocomplete-input {
	margin: 0;
	padding: 0.48em 0 0.47em 0.45em;
}
</style>

<!-- auto complete function -->
<script>

	(function($) {
		$.widget("ui.combobox",{_create : function() {
			var self = this, select = this.element.hide(), selected = select.children(":selected"), value = selected.val() ? selected.text() : "";
			var input = this.input = $("<input>").insertAfter(select).val(value).autocomplete({
				delay : 0,
				minLength : 0,
				source : function(request,response) {
					var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term),"i");
					response(select.children("option").map(function() {
						var text = $(this).text();
						if (this.value && (!request.term || matcher.test(text)))
							return {label : text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+ $.ui.autocomplete.escapeRegex(request.term)
									+ ")(?![^<>]*>)(?![^&;]+;)","gi"),"<strong>$1</strong>"),
									value : text,
									option : this
									};
								}));
					},
					select : function(event, ui) {
						ui.item.option.selected = true;
						self._trigger("selected",event,{tem : ui.item.option});
					},
					change : function(event, ui) {
						if (!ui.item) {	var matcher = new RegExp("^"+ $.ui.autocomplete.escapeRegex($(this).val()) + "$","i"), valid = false;
						select.children("option").each(function() {
							if ($(this).text().match(matcher)) {
								this.selected = valid = true;
								return false;
							}
						});
						if (!valid) {
							// remove invalid value, as it didn't match anything
							$(this).val("");
							select.val("");
							input.data("autocomplete").term = "";
							return false;
						}
						}
						}
					})
					.addClass("ui-widget ui-widget-content ui-corner-left");
			input.data("autocomplete")._renderItem = function(ul, item) {
				return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
				};

				this.button = $("<button type='button'>&nbsp;</button>").attr("tabIndex", -1).attr("title", "Show All Items").insertAfter(input).button({
					icons : {primary : "ui-icon-triangle-1-s"},	text : false
				})
				.removeClass("ui-corner-all")
				.addClass("ui-corner-right ui-button-icon").click(function() {
					// close if already visible
					if (input.autocomplete("widget").is(":visible")) {
						input.autocomplete("close");
						return;
					}
					// work around a bug (likely same cause as #5265)
					$(this).blur();
					// pass empty string as value to search for, displaying all results
					input.autocomplete("search", "");
					input.focus();
					});
				},
				destroy : function() {
					this.input.remove();
					this.button.remove();
					this.element.show();
					$.Widget.prototype.destroy.call(this);
					}
				});
	})(jQuery);

	//Adding autocomplete function to comboboxes
	$(function() {
		$("#comboTemplateCategory1").combobox();
		$("#toggle").click(function() {
			$("#comboTemplateCategory1").toggle();
		});
	});
	
	//Templates in combobox are changing according to selection of template category combobox
	$(function() {
		$("#comboTemplateCategory2").combobox({
			
			selected : function(event, ui) {				
				var selectedCategory = $(this).val();
				$.ajax({
					type : "GET",
					url : "SmsTemplateClass",
					data : {
						type : "loadTemplateCombo",
						category : selectedCategory
					}
				})	
				.success(function(response, status) {
					$("#TemplateMessage").val("");
					$('#comboTemplates').val("");
					$('#comboTemplates').html("");
					$("#comboTemplates").append(response);
				})
				.error(function() {
					$().toastmessage('showWarningToast',"Unable to load data.This may be happend due to the database connection failer.Please try again.");
				});
			}
		});
		$("#toggle").click(function() {
			$("#comboTemplateCategory2").toggle();
		});
	});
	
	//When selecting a template loading particular message into textarea.
	$(function() {
		$("#comboTemplates")
				.combobox({
					selected : function(event, ui) {
						var selectedCategory = $("#comboTemplateCategory2").val();
						var selectedTemplate = $(this).val();
						$.ajax({
							type : "GET",
							url : "SmsTemplateClass",
							data : {
								type : "loadTemplate",
								category : selectedCategory,
								templateName : selectedTemplate
							}
						})
						.success(function(response, status) {
							$("#TemplateMessage").val(response);
						});
					}
				});

		$("#toggle").click(function() {
			$("#comboTemplates").toggle();
		});
	});
	
</script>

<!-- Create Category button click event: This will do the validations and create the caetgory-->
<script>

	$(function() {
		$("#btnCreateCategory").click(
				function() {
					var name = $("#CategoryName").val();
					var description = $("#CategoryDescription").val();
					var tName = $("#TemplateName").val();
					var message = $("#CategoryMessage").val();
					var check = "";
					
							$.ajax({
								type : "GET",
								url : "SmsTemplateCategoryClass",
								data : {
									msg : name,
									type : 'categoryCheck'
								},
								async : false
							})
							.success(function(response, status) {
								
								if (response == "true") {
									check = "true";
								} 
								else {
									ckeck = "false";
								}
							});
							
							if (check == "true") {
								$("#spnCategoryName2").text("");
								$("#spnCategoryName2")
								.append("<lable>&nbsp;&nbsp;Category name is already available.Please enter a different name.</lable><br><label>&nbsp;</label>");
								document.getElementById('categoryNameError').style.visibility = 'visible';
							}
							else if ($("#CategoryName").val() == "") {
								$("#spnCategoryName2").text("");
								$("#spnCategoryName2")
								.append("<lable>&nbsp;&nbsp;Please enter a category name.</lable><br><label>&nbsp;</label>");
								document.getElementById('categoryNameError').style.visibility = 'visible';
							} 
							else if ($("#CategoryDescription").val() == "") {
								$("#spnCategoryDescription2").text("");
								$("#spnCategoryDescription2")
								.append("<lable>&nbsp;&nbsp;Please enter a short description about the category.</lable><br><label>&nbsp;</label>");
								document.getElementById('categoryDescriptionError').style.visibility = 'visible';
							} 
							else if ($("#TemplateName").val() == "") {
								$("#spnTemplateName2").text("");
								$("#spnTemplateName2")
								.append("<lable>&nbsp;&nbsp;Please enter template name.</lable><br><label>&nbsp;</label>");
								document.getElementById('templateNameError2').style.visibility = 'visible';
							} 
							else if ($("#CategoryMessage").val() == "") {
								$("#spnCategoryMessage2").text("");
								$("#spnCategoryMessage2")
								.append("<lable>&nbsp;&nbsp;Please enter template message.</lable><br><label>&nbsp;</label>");
								document.getElementById('categoryMessageError').style.visibility = 'visible';
							}
							else {
								$.ajax({
									type : "GET",
									url : "SmsTemplateCategoryClass",
									data : {
										type : "adding",
										catname : name,
										catdescription : description,
										templatename : tName,
										msg : message
									}
								})
								.success(function(response, status) {
									$().toastmessage('showToast',{
										text : 'Sms template category successfully added to the system.',
										sticky : false,
										stayTime : 5000,
										type : 'success'
									});
									CategoryFunction();
									document.getElementById("smsTemplatesForm").reset();
								})
								.error(function() {
									$().toastmessage('showWarningToast',"Template category does not created,Please try again!");
								});
							}
						});
		});
	
</script>

<!-- Template save button click event: This will do the validations and create template ,
	Category delete button click event: -->
<script>
	$(function() {
		
		$("#btnSaveTemplate").click(function() {
			var category = $("#comboTemplateCategory1").val();
			var Title = $("#TemplateTitle").val();
			var message = $("#MessageTextArea").val();
			var check = "";
			$.ajax({
				type : "GET",
				url : "SmsTemplateClass",
				data : {
					msg : Title,
					category : category,
					type : 'templateCheck'
				},
				async : false
			})
			.success(function(response, status) {
				if (response == "true") {
					check = "true";
				}
				else {
					ckeck = "false";
				}
			});
			if ($("#comboTemplateCategory1").val() == "") {
				$("#spnTemplateCategory").text("");
				$("#spnTemplateCategory")
				.append("<lable>&nbsp;&nbsp;Please select a template category.</lable><br><label>&nbsp;</label>");
				document.getElementById('templateCategoryError').style.visibility = 'visible';
			} 
			else if (check == "true") {
				$("#spnTemplateName").text("");
				$("#spnTemplateName")
				.append("<lable>&nbsp;&nbsp;Template name is already available.Please enter a different name.</lable><br><label>&nbsp;</label>");
				document.getElementById('templateNameError').style.visibility = 'visible';
			}
			else if ($("#TemplateTitle").val() == "") {
				$("#spnTemplateName").text("");
				$("#spnTemplateName")
				.append("<lable>&nbsp;&nbsp;Please enter a template name.</lable><br><label>&nbsp;</label>");
				document.getElementById('templateNameError').style.visibility = 'visible';
			}
			else if ($("#MessageTextArea").val() == "") {
				$("#spnTemplateMsg").text("");
				$("#spnTemplateMsg")
				.append("<lable>&nbsp;&nbsp;You cannot create empty templates.</lable><br><label>&nbsp;</label>");
				document.getElementById('templateMsgError').style.visibility = 'visible';
			}
			else {
				$.ajax({
					type : "GET",
					url : "SmsTemplateCategoryClass",
					data : {
						type : "addingtemplate",
						catname : category,
						templatename : Title,
						msg : message
					}
				})
				.success(function(response, status) {
					
					$().toastmessage('showToast',{
						text : 'SMS template added successfully.',
						sticky : false,
						stayTime : 4000,
						type : 'success'
					});
					document.getElementById("smsTemplatesForm").reset();
									
				})				
				.error(function() {
					$().toastmessage('showWarningToast',"Template does not created, Please try again !");
				});
			}
		});

		//Delete Category button click event.
		
		$("#BtnDelCat").click(function() {
			
			$("#dialog:ui-dialog").dialog("destroy");
			$("#categoryDeleteDialog").dialog({
				autoOpen : true,
				resizable : false,
				height : 140,
				modal : true,
				buttons : {
					"Delete Message" : function() {
						var selectedCategory = $("#comboTemplateCategory2").val();
						$.ajax({
							type : "GET",
							url : "SmsTemplateCategoryClass",
							data : {
								type : "deleteCategory",
								catname : selectedCategory
							}
						})
						.success(function(response,status) {
							$().toastmessage('showToast',{
								text : 'Sms template category successfully deleted.',
								sticky : false,
								stayTime : 5000,
								type : 'success'
							});
							var index = $('#comboTemplateCategory2').get(0).selectedIndex;
							$('#comboTemplateCategory2 option:eq('+ index+ ')').remove();
							$("#comboTemplateCategory2").val("");
							$("#TemplateMessage").val("");
							$("#comboTemplates").val("");
							document.getElementById("smsTemplatesForm").reset();
						})
						.error(function() {
							$().toastmessage('showWarningToast',"Deletion not completed, Please try again !");
						});
						$(this).dialog("close");
					},	
					Cancel : function() {
						$(this).dialog("close");
					}
				}
			});
		});
		
		$("#BtnDelTem")
				.click(
						function() {

							$("#dialog:ui-dialog").dialog("destroy");
							$("#templateDeleteDialog")
									.dialog(
											{
												autoOpen : true,
												resizable : false,
												height : 140,
												modal : true,
												buttons : {

													"Delete Message" : function() {

														var selectedCategory = $(
																"#comboTemplateCategory2")
																.val();
														var selectedTemplate = $(
																"#comboTemplates")
																.val();
														$
																.ajax(
																		{
																			type : "GET",
																			url : "SmsTemplateClass",
																			data : {
																				type : "deleteTemplate",
																				category : selectedCategory,
																				templateName : selectedTemplate
																			}

																		})

																.success(
																		function(response,status) {
																			$().toastmessage('showToast',{
																				text : 'Sms template successfully deleted.',
																				sticky : false,
																				stayTime : 5000,
																				type : 'success'
																			});
																			var index = $('#comboTemplates').get(0).selectedIndex;
																			$('#comboTemplates option:eq('+ index+ ')').remove();
																			$("#TemplateMessage").val("");
																			$("#comboTemplates").val("");
																			document.getElementById("smsTemplatesForm").reset();
																		})

																.error(
																		function() {
																			$()
																					.toastmessage(
																							'showWarningToast',
																							"Deletion not completed, Please try again !");
																		});

														CategoryFunction();

														$(this).dialog("close");
													},
													Cancel : function() {
														$(this).dialog("close");
													}
												}
											});

						});
	});
</script>

<script type="text/javascript">
	$(function() {
		$("#comboTemplateCategory1")
				.click(
						function() {

							document.getElementById('templateCategoryError').style.visibility = 'hidden';

						});
	});
	$(function() {
		$("#TemplateTitle")
				.click(
						function() {

							document.getElementById('templateNameError').style.visibility = 'hidden';
							//document.getElementById('templateName').style.display = 'none';			

						});
	});
	$(function() {
		$("#MessageTextArea").click(function() {

			document.getElementById('templateMsgError').style.visibility = 'hidden';

		});
	});
	$(function() {
		$("#CategoryMessage")
				.click(
						function() {

							document.getElementById('categoryMessageError').style.visibility = 'hidden';

						});
	});
	$(function() {
		$("#TemplateName")
				.click(
						function() {

							document.getElementById('templateNameError2').style.visibility = 'hidden';

						});
	});
	$(function() {
		$("#CategoryDescription")
				.click(
						function() {

							document.getElementById('categoryDescriptionError').style.visibility = 'hidden';

						});
	});
	$(function() {
		$("#CategoryName")
				.click(
						function() {

							document.getElementById('categoryNameError').style.visibility = 'hidden';

						});
	});
</script>


<script>
	$(function() {

		$("#BtnEditTemp")
				.click(
						function() {

							var selectedCategory = $("#comboTemplateCategory2").val();
							var selectedTemplate = $("#comboTemplates").val();
							var msg = $("#TemplateMessage").val();

							$
									.ajax({
										type : "GET",
										url : "SmsTemplateClass",
										data : {
											type : "updateTemplate",
											category : selectedCategory,
											message : msg,
											templateName : selectedTemplate
										}

									})

									.success(
											function(response, status) {

												$()
														.toastmessage(
																'showToast',
																{
																	text : 'Sms template successfully updated.',
																	sticky : false,
																	stayTime : 5000,
																	type : 'success'
																});
												document.getElementById(
														"smsTemplatesForm").reset();

											})

									.error(
											function() {
												$()
														.toastmessage(
																'showWarningToast',
																"The template was not updated, Please try again !");
											});

						});

	});
</script>


<script type="text/javascript">
	$(function() {

		$("#TemplateTitle").keyup(function() {

			var input = $("#TemplateTitle").val();
			var categoryName = $("#comboTemplateCategory1").val();
			$.ajax({
				type : "GET",
				url : "SmsTemplateClass",
				data : {
					msg : input,
					category : categoryName,
					type : 'templateCheck'
				}

			})

			.success(function(response, status) {

				if (response == "true") {

					$("#TemplateTitle").css({
						"border" : "1px solid red"
					});

				} else {

					$("#TemplateTitle").css({
						"border" : "1px solid #736F6E"
					});

				}

			})

			.error(function() {

			});

		});
	});
</script>

<script type="text/javascript">
	$(function() {

		$("#CategoryName").keyup(function() {

			var input = $("#CategoryName").val();

			$.ajax({
				type : "GET",
				url : "SmsTemplateCategoryClass",
				data : {
					msg : input,
					type : 'categoryCheck'
				}

			})

			.success(function(response, status) {

				if (response == "true") {

					$("#CategoryName").css({
						"border" : "1px solid red"
					});

				} else {

					$("#CategoryName").css({
						"border" : "1px solid #736F6E"
					});

				}

			})

			.error(function() {

			});

		});
	});
</script>


<script type="text/javascript">
	$(function() {
		$("#btnSaveTemplate").button({
			icons : {
				primary : "ui-icon-disk"
			}
		});
		$("#BtnCancel1").button({
			icons : {
				primary : "ui-icon-close"
			}
		});
		$("#BtnReset1").button({
			icons : {
				primary : "ui-icon-refresh"
			}
		});
		$("#BtnCancel2").button({
			icons : {
				primary : "ui-icon-close"
			}
		});
		$("#BtnReset2").button({
			icons : {
				primary : "ui-icon-refresh"
			}
		});
		$("#btnCreateCategory").button({
			icons : {
				primary : "ui-icon-disk"
			}
		});

		$("#BtnDelCat").button({
			icons : {
				primary : "ui-icon-trash"
			}
		});
		$("#BtnDelTem").button({
			icons : {
				primary : "ui-icon-trash"
			}
		});
		$("#BtnEditTemp").button({
			icons : {
				primary : "ui-icon-copy"
			}
		});
		$("#BtnReset3").button({
			icons : {
				primary : "ui-icon-refresh"
			}
		});
		$("#Cancel3").button({
			icons : {
				primary : "ui-icon-close"
			}
		});

	});
</script>

</head>
<body>

	<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">
			<div class="ui-widget">
				<div class="ui-widget-header">Create Templates</div>
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

							<Form name="smsTemplatesForm" id="smsTemplatesForm"	style="display: inline;">


								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Create Template</a></li>
										<li><a href="#tabs-2">Create Template Category</a></li>
										<li><a href="#tabs-3">Delete Templates</a></li>
									</ul>
									<div id="tabs-1">
										<div>
											<div class="ui-widget-header">Create Template</div>
											<br>
											<br> Select Template Category: <br> <select
												id="comboTemplateCategory1">
											</select>

											<div style="visibility: visible; width: 250px;"
												id="templateCategoryError">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnTemplateCategory" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>



											Template Name: <br> <input type="text"
												id="TemplateTitle" name="TemplateTitle" size="50" class="text ui-widget-content ui-corner-all"> <br>
											<div style="visibility: visible; width: 250px;"
												id="templateNameError">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnTemplateName" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>


											Message: <br>
											<textarea id="MessageTextArea" rows="6" cols="50"
												name="MessageTextArea"></textarea>
											<br>
											<div style="visibility: visible; width: 250px;"
												id="templateMsgError">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnTemplateMsg" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>


											<br>
											<button id="btnSaveTemplate" type="button" name="BtnSend"
												value="Save Template">Save Template</button>
											<button type="reset" name="BtnReset" value="Reset"
												id="BtnReset1">Reset</button>
											<button type="button" id="BtnCancel1" value="Cancel">Cancel</button>
											<br>
											<br>
										</div>

									</div>
									<div id="tabs-2">
										<div>
											<div class="ui-widget-header">Create Template Category</div>
											<br>
											<br> Category Name: <br> <input type="text"
												id="CategoryName" name="CategoryName" size="50" class="text ui-widget-content ui-corner-all">

											<div style="visibility: visible; width: 250px;"
												id="categoryNameError">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnCategoryName2" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>


											Category Description: <br>
											<textarea id="CategoryDescription" rows="6" cols="50"
												name="CategoryDescription"></textarea>


											<div style="visibility: visible; width: 250px;"
												id="categoryDescriptionError">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnCategoryDescription2" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>


											Template Name: <br> <input type="text" id="TemplateName"
												name="TemplateName" size="50" class="text ui-widget-content ui-corner-all">

											<div style="visibility: visible; width: 250px;"
												id="templateNameError2">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnTemplateName2" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>



											Message: <br>
											<textarea id="CategoryMessage" rows="6" cols="50"
												name="CategoryMessage"></textarea>


											<div style="visibility: visible; width: 250px;"
												id="categoryMessageError">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnCategoryMessage2" class="message-text"><lable>&nbsp;&nbsp;Please
															select a item to edit.</lable><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>


											<br>
											<button id="btnCreateCategory" type="button"
												name="btnCreateCategory" value="Save Category">Save
												Category</button>
											<button type="reset" name="BtnReset" value="Reset"
												id="BtnReset2">Reset</button>
											<button type="button" id="BtnCancel2" value="Cancel">Cancel</button>
											<br>
											<br>
										</div>
									</div>
									<div id="tabs-3">
										<div>
											<div class="ui-widget-header">Update Template/Category</div>
											<br>
											<br> Select Template Category: <br> <select
												id="comboTemplateCategory2">
											</select>
											<button id="BtnDelCat" type="button" name="BtnDelCat"
												value="Delete Category">Delete Category</button>

											<br>
											<br> Select Template: <br> <select id="comboTemplates">
											</select>

											<button id="BtnDelTem" type="button" name="BtnDelTem"
												value="Delete Template">Delete Template</button>
											<br>
											<br> Message: <br>
											<textarea id="TemplateMessage" rows="6" cols="50"
												name="MessageTextArea">
												</textarea>
											<br>
											<br>
											<br>
											<button id="BtnEditTemp" type="button" name="BtnSend"
												value="Update Template">Update Template</button>
											<button type="reset" name="BtnReset" value="Reset"
												id="BtnReset3">Reset</button>
											<button type="button" name="BtnCancel" id="Cancel3">Cancel</button>
											<br>
											<br>
										</div>
									</div>
								</div>


							</Form>

							<div id="categoryDeleteDialog" title="Delete Category?">
								<p>
									<span class="ui-icon ui-icon-alert"
										style="float: left; margin: 0 7px 20px 0;"> </span>This
									category will be permanently deleted and cannot be recovered.
									Are you sure?
								</p>
							</div>

							<div id="templateDeleteDialog" title="Delete Category?">
								<p>
									<span class="ui-icon ui-icon-alert"
										style="float: left; margin: 0 7px 20px 0;"> </span>This
									template will be permanently deleted and cannot be recovered.
									Are you sure?
								</p>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>