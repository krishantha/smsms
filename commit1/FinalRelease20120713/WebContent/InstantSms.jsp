
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<script	src="js/jquery-1.7.2.min.js"></script>
<script	src="js/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.toastmessage.js"></script>
<link href="css/sunny/jquery-ui-1.8.20.custom.css"	rel="stylesheet" type="text/css" />
<link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
<link rel="stylesheet" href="css/demos.css">
<link rel="stylesheet" type="text/css" media="screen" href="grid/css/ui.jqgrid.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Instant SMS</title>


<script type="text/javascript">
	$(function() {
		document.getElementById('id').style.visibility = 'hidden';
		document.getElementById('message').style.visibility = 'hidden';
		document.getElementById('Gid').style.visibility = 'hidden';
		document.getElementById('gMsg').style.visibility = 'hidden';
	});
</script>

<script type="text/javascript">
	$(function() {
		$("#tabs").tabs({
			fx : [ {
				opacity : 'toggle',
				duration : 'slow'
			}, {opacity : 'toggle',
				duration : 'slow'
			} ]
		});
	});
</script>

<script type="text/javascript">
	$(function Category() {
		$.ajax({
			type : "GET",
			url : "SmsTemplateCategoryClass",
			data : {
				type : "CategoryCombo"
			}
		})

		.success(function(response, status) {
			$("#combobox2").append(response);
		})
		
		.error(function() {
			//error !!!!!
			$().toastmessage('showWarningToast', "Please try again ");
		});

	});
</script>

<script type="text/javascript">
	(function($) {
		$.widget("ui.combobox",{_create : function() {
			var self = this, 
			select = this.element.hide(),
			selected = select.children(":selected"), 
			value = selected.val() ? selected.text() : "";
			var input = this.input = $("<input>").insertAfter(select)
			.val(value).autocomplete(
					{delay : 0,minLength : 0,source : function(request,response) {
						var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term),
								"i");
						response(select.children("option").map(function() {
							var text = $(this).text();
							if (this.value && (!request.term || matcher.test(text)))
								return {label : text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("
										+ $.ui.autocomplete.escapeRegex(request.term)+
										")(?![^<>]*>)(?![^&;]+;)","gi"),
										"<strong>$1</strong>"),value : text,option : this};
										}));
						},
						select : function(event, ui) {
							ui.item.option.selected = true;
							self._trigger("selected",event,{
								item : ui.item.option});
							},change : function(event, ui) {
								if (!ui.item) {
									var matcher = new RegExp(
											"^"+ $.ui.autocomplete.escapeRegex($(this).val())+ "$",
											"i"),
											valid = false;
									select.children("option").each(function() {
										if ($(this).text().match(matcher)) {
											this.selected = valid = true;
											return false;
											}
										});
									if (!valid) {
										// remove invalid value, as it didn't match anything
										$(this).val("");
										select.val("");
										input.data("autocomplete").term = "";
										return false;
										}
									}
								}
							}).addClass("ui-widget ui-widget-content ui-corner-left");
			input.data("autocomplete")._renderItem = function(ul, item) {
				return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
				
			};
			this.button = $("<button type='button'>&nbsp;</button>")
			.attr("tabIndex", -1).attr("title", "Show All Items").insertAfter(input).button({
				icons : {
					primary : "ui-icon-triangle-1-s"},
					text : false
					}).removeClass("ui-corner-all").addClass("ui-corner-right ui-button-icon").click(function() {
						// close if already visible
						if (input.autocomplete("widget").is(":visible")) {
							input.autocomplete("close");
							return;
							}
						// work around a bug (likely same cause as #5265)
						$(this).blur();
						// pass empty string as value to search for, displaying all results
						input.autocomplete("search", "");
						input.focus();
						});
			},
			destroy : function() {
				this.input.remove();
				this.button.remove();
				this.element.show();
				$.Widget.prototype.destroy.call(this);
				}
			});
		})(jQuery);
	
	$(function() {
		$("#combobox2").combobox({
			// And supply the "selected" event handler at the same time.
			selected : function(event, ui) {
				var cat = $(this).val();
				$.ajax({
					type : "GET",
					url : "SmsTemplateClass",
					data : {
						type : "loadTemplateCombo",
						category : cat
						}
				})
				.success(function(response, status) {
					$("#TemplatePreviewMessage").val("");
					$('#combobox3').val("");
					$('#combobox3').html("");
					$("#combobox3").append(response);
					}
				)
				.error(function() {
					$().toastmessage('showWarningToast',"Unable to load data.Please try again !");
					}
				);
				}
		});
		$("#toggle").click(function() {
			$("#combobox2").toggle();		
		});
	});
	
	$(function() {
		$("#combobox3").combobox(
				{
					selected : function(event, ui) {
						var cat = $("#combobox2").val();
						var tem = $(this).val();
						$.ajax({
							type : "GET",
							url : "SmsTemplateClass",
							data : {
								type : "loadTemplate",
								category : cat,
								templateName : tem
								}
						})
						.success(function(response, status) {
							$("#TemplatePreviewMessage").val(response);
							}
						)
						.error(function() {
							$().toastmessage('showWarningToast',"Unable to load data.Please try again !");
							}
						);
						}
				});
		$("#toggle").click(function() {
			$("#combobox3").toggle();
			}
		);
		}
	);
</script>

<script type="text/javascript">
	$(function() {
		$(function() {
			$
					.ajax({
						type : "GET",
						url : "InstantClass",
						data : {
							type : 'allcustomers'
						}
					})
					.success(
							function(response, status) {
								var temp = response;
								var availableTags = temp.split(",");

								function split(val) {
									return val.split(/,\s*/);
								}
								function extractLast(term) {
									return split(term).pop();
								}

								$("#customerId")
										// don't navigate away from the field on tab when selecting an item
										.bind(
												"keydown",
												function(event) {
													if (event.keyCode === $.ui.keyCode.TAB
															&& $(this)
																	.data(
																			"autocomplete").menu.active) {
														event.preventDefault();
													}
												})
										.autocomplete(
												{
													minLength : 0,
													source : function(request,
															response) {
														// delegate back to autocomplete, but extract the last term
														response($.ui.autocomplete
																.filter(
																		availableTags,
																		extractLast(request.term)));
													},
													focus : function() {
														// prevent value inserted on focus
														return false;
													},
													select : function(event, ui) {
														var terms = split(this.value);
														// remove the current input
														terms.pop();
														// add the selected item
														terms
																.push(ui.item.value);
														// add placeholder to get the comma-and-space at the end
														terms.push("");
														this.value = terms
																.join(",");
														return false;
													}
												});

							})

					.error();

		});
	});
</script>

<script  type="text/javascript">
	$(function() {
		$(function() {
			$
					.ajax({
						type : "GET",
						url : "InstantClass",
						data : {
							type : 'allgroups'
						}
					})
					.success(
							function(response, status) {
								var temp = response;
								var availableTags = temp.split(",");

								function split(val) {
									return val.split(/,\s*/);
								}
								function extractLast(term) {
									return split(term).pop();
								}

								$("#groupsId")
										// don't navigate away from the field on tab when selecting an item
										.bind(
												"keydown",
												function(event) {
													if (event.keyCode === $.ui.keyCode.TAB
															&& $(this)
																	.data(
																			"autocomplete").menu.active) {
														event.preventDefault();
													}
												})
										.autocomplete(
												{
													minLength : 0,
													source : function(request,
															response) {
														// delegate back to autocomplete, but extract the last term
														response($.ui.autocomplete
																.filter(
																		availableTags,
																		extractLast(request.term)));
													},
													focus : function() {
														// prevent value inserted on focus
														return false;
													},
													select : function(event, ui) {
														var terms = split(this.value);
														// remove the current input
														terms.pop();
														// add the selected item
														terms
																.push(ui.item.value);
														// add placeholder to get the comma-and-space at the end
														terms.push("");
														this.value = terms
																.join(",");
														return false;
													}
												});

							})

					.error();

		});
	});
</script>

<script type="text/javascript">
	$(function() {
		
		$("#customerId").click(function() {
			document.getElementById('id').style.visibility = 'hidden';
		});
		
	});
	$(function() {
		
		$("#MessageTextArea1").click(function() {
			document.getElementById('message').style.visibility = 'hidden';
		});
		
	});
	$(function() {
		
		$("#MessageTextArea2").click(function() {
			document.getElementById('gMsg').style.visibility = 'hidden';
		});
		
	});
	$(function() {
		
		$("#groupsId").click(function() {
			document.getElementById('Gid').style.visibility = 'hidden';
		});
		
	});
</script>

<script type="text/javascript">
	$(function() {
		$("#BtnSendGroup").click(function() {
			var message = $("#MessageTextArea2").val();
			var users = $("#groupsId").val();
			var checkResult = "";
			var input = $("#groupsId").val();
			$.ajax({
				type : "GET",
				url : "InstantClass",
				data : {
					msg : input,
					type : 'groupIdCheck'
					},
				async : false
				})
				
				.success(function(response, status) {
					if (response == "false") {
						checkResult = "false";
						}
				})
				
				.error(function() {
					
				});
			
			if (checkResult == "false") {
				
				$("#spnGId").text("");
				$("#spnGId").append("<label>&nbsp;&nbsp;You have entered an incorrect group name</label><br><label>&nbsp;</label>");
				document.getElementById('Gid').style.visibility = 'visible';
				
			} else if ($("#groupsId").val() == "") {
				
				$("#spnGId").text("");
				$("#spnGId").append("<label>&nbsp;&nbsp;Please enter group name</label><br><label>&nbsp;</label>");
				document.getElementById('Gid').style.visibility = 'visible';
									
			} else if ($("#MessageTextArea2").val() == "") {
				
				$("#spnGMsg").text("");
				$("#spnGMsg").append("<label>&nbsp;&nbsp;Please enter a message to send</label><br><label>&nbsp;</label>");
				document.getElementById('gMsg').style.visibility = 'visible';
				
			} else if (getCid() != "") {
				
				var custormized = getCid();
				$.ajax({
					type : "GET",
					url : "InstantClass",
					data : {
						msg : message,
						user : users,
						custom : custormized,
						type : 'groupCustermized'
						}
				})
				.success(function() {
					$().toastmessage('showSuccessToast',"Messages have been sheduled !");
					document.getElementById("InstantSms").reset();
					
				})
				.error(function() {
					$().toastmessage('showWarningToast',"Messages not sheduled, Please try again.");
					
				});
				
			} else {
				$.ajax({
					type : "GET",
					url : "InstantClass",
					data : {
						msg : message,
						user : users,
						type : 'group'
						}
				})
				.success(function() {
					$().toastmessage('showSuccessToast',"Messages have been sheduled !");
					document.getElementById("InstantSms").reset();
					
				})
				.error(function() {
					$().toastmessage('showWarningToast',"Messages not sheduled, Please try again.");
					
				});
				
			}	
		});		
	});
</script>

<script type="text/javascript">
	$(function() {

		$("#groupsId").keyup(function() {

			var input = $("#groupsId").val();
			$.ajax({
				type : "GET",
				url : "InstantClass",
				data : {
					msg : input,
					type : 'groupIdCheck'
				}

			})
			.success(function(response, status) {

				if (response == "false") {
					$("#groupsId").css({
						"border" : "1px solid red"
					});

				} else {

					$("#groupsId").css({
						"border" : "1px solid #736F6E"
					});

				}

			})

			.error(function() {

			});

		});
	});

	$(function() {

		$("#customerId").keyup(function() {

			var input = $("#customerId").val();
			$.ajax({
				type : "GET",
				url : "InstantClass",
				data : {
					msg : input,
					type : 'customerIdCheck'
				}
			})
			.success(function(response, status) {

				if (response == "false") {

					$("#customerId").css({
						"border" : "1px solid red"
					});

				} else {
					$("#customerId").css({
						"border" : "1px solid #736F6E"
					});
				}
			})
			.error(function() {

			});
		});
	});
</script>

<script  type="text/javascript">

	$(function() {
		$("#BtnSendSingle").click(function() {
			
			var message = $("#MessageTextArea1").val();
			var users = $("#customerId").val();
			var check = "test";
			$.ajax({
				type : "GET",
				url : "InstantClass",
				data : {
					msg : users,
					type : 'customerIdCheck'
					},
					async : false
					})
					
			.success(function(response, status) {
				if (response == "false") {
					check = "false";
					} 
				else {
					check = "true";
					}
				})
			.error(function() {
				
			});
			
			if (check == "false") {
				
				$("#spnId").text("");
				$("#spnId").append("<label>&nbsp;&nbsp;You have entered an incorrect customer name</label><br><label>&nbsp;</label>");
				document.getElementById('id').style.visibility = 'visible';
				
			}
			else if ($("#customerId").val() == "") {
				
				$("#spnId").text("");
				$("#spnId").append("<label>&nbsp;&nbsp;Please enter customer id</label><br><label>&nbsp;</label>");								
				document.getElementById('id').style.visibility = 'visible';
				
			} 
			else if ($("#MessageTextArea1").val() == "") {
				
				$("#spnMsg").text("");
				$("#spnMsg").append("<label>&nbsp;&nbsp;Please enter a message to send</label><br><label>&nbsp;</label>");
				document.getElementById('message').style.visibility = 'visible';
				
			} 
			else {
				
				$.ajax({
					type : "GET",
					url : "InstantClass",
					data : {
						msg : message,
						user : users,
						type : 'single'
						}
				})
				.success(function() {
					
					$().toastmessage('showSuccessToast',"Message have been sheduled !");
					document.getElementById("InstantSms").reset();
					
				})
				.error(function() {
					$().toastmessage('showWarningToast',"Message not sheduled, Please try again.");
					
				});				
			}			
		});		
	});
</script>

<script type="text/javascript">
	
	var removingCid = "";
	function setCid(cid) {
		removingCid = cid;	
	}
	function getCid() {
		return removingCid;
	}
	
</script>

<script type="text/javascript">
	$(function() {
		$("#Remove-Customers").dialog({
			autoOpen : false,
			show: "drop",
			hide:"drop",
			height : 400,
			width : 400,
			modal : true,
			buttons : {
				
				"Remove" : function() {

					var test = [];
					$('#resultTable2 :checked').each(function() {
						test.push($(this).val());
					});

					var temp = "";
					temp = temp + test;

					setCid(temp);
					$("#groupsId").css({
						"background-color" : "#edece9"
					});
					$(this).dialog("close");

				},
				"Cancel" : function() {
					$(this).dialog("close");
				}

			},
			close : function() {
				allFields.val("").removeClass("ui-state-error");
			}

		});
	$("#BtnGroupCustormize").click(function() {
		
		var keyword = $("#groupsId").val();
		if (keyword == "") {
			$("#spnGId").text("");								
			$("#spnGId").append("<label>&nbsp;&nbsp;Please enter group id</label><br><label>&nbsp;</label>");
			document.getElementById('Gid').style.visibility = 'visible';
		} 
		else {
			$("#Remove-Customers").dialog("open");
			$.ajax({
				type : "GET",
				url : "InstantClass",
				data : {
					type : 'groupCustormize',
					user : keyword
					}
			})
			.success(function(response, status) {
				$('#resultTable2').html(response);
			})
			.error(function(request, error) {
				$().toastmessage('showWarningToast',"There is an error when connecting with the server.Please refresh page and try again.");
			});
			}
		});
	});
</script>

<script type="text/javascript">
	
	$(function() {
		var word = "";
		$("#dialog-form").dialog({
			autoOpen : false,
			height : 500,
			width : 400,
			show:"drop",
			hide:"drop",
			modal : true,
			buttons : {
				
				"Add Recipient" : function() {

					var test = [];
					$('#resultTable :checked').each(function() {
						test.push($(this).val());
					});

					//var rowCount = $('#resultTable tr').length;
					//alert(rowCount);
					//var test = "";
					//test = test + $("input:checked").val();
					//alert(test);
					var temp = $("#customerId").val();
					$("#customerId").val(temp + test);
					//alert("testing");					
					$(this).dialog("close");

				},

				"Cancel" : function() {
					$(this).dialog("close");
				}
				
			},
			close : function() {
				allFields.val("").removeClass("ui-state-error");
			}

		});
		$("#singleSearch").click(function() {
			$("#dialog-form").dialog("open");
			word = "single";
		});
		$("#search").click(function() {
			
			var keyword = $("#customerName").val();
			$.ajax({
				type : "GET",
				url : "SearchingClass",
				data : {
					type : word,
					key : keyword										
				}
			})
			.success(function(response, status) {
				$('#resultTable').html(response);				
				
			});
			
		});
	});
	
</script>


<script type="text/javascript">

$(function(){
	
	$('#dialog-form').submit(function(e){
	    return false;
	});

	$("#customerName").keyup(function(event){
		
	    if(event.keyCode == 13){
	        $("#search").click();
	    }
	});
	
});

</script>

<script type="text/javascript">

	$(function() {
		
		var button = "";
		$("#template-form").dialog({
			autoOpen : false,
			height : 350,
			show:"drop",
			hide:"drop",
			width : 400,
			modal : true,
			buttons : {				
				
				"Use Template" : function() {

					var temp = $("#TemplatePreviewMessage").val();

					if (button == "single") {
						$("#MessageTextArea1").val(temp);
					} else {
						$("#MessageTextArea2").val(temp);
					}
					//var rowCount = $('#resultTable tr').length;
					//alert(rowCount);
					//var test = "";
					//test = test + $("input:checked").val();
					//alert(test);

					//alert("testing");					
					$(this).dialog("close");

				},
				"Cancel" : function() {
					$(this).dialog("close");
				},

			},
			close : function() {
				allFields.val("").removeClass("ui-state-error");
			}
			
		});
		
		$("#BtnSelectTemplate").click(function() {
			$("#template-form").dialog("open");
			button = "single";

		});
		$("#BtnSelectTemplateG").click(function() {
			$("#template-form").dialog("open");
			button = "group";

		});
	});
	
</script>

<script type="text/javascript">

	$(function() {
		$("#singleSearch").button({
			icons : {primary : "ui-icon-circle-zoomin"}
		});
		$("#BtnSelectTemplate").button({
			icons : {primary : "ui-icon-clipboard"}
		});
		$("#BtnSendSingle").button({
			icons : {primary : "ui-icon-mail-closed"}
		});
		$("#BtnReset").button({
			icons : {primary : "ui-icon-refresh"}
		});
		$("#BtnCancelSingle").button({
			icons : {primary : "ui-icon-close"}
		});
		$("#BtnGroupCustormize").button({
			icons : {primary : "ui-icon-gear"}
		});
		$("#BtnSelectTemplateG").button({
			icons : {primary : "ui-icon-clipboard"}
		});
		$("#BtnSendGroup").button({
			icons : {primary : "ui-icon-mail-closed"}
		});
		$("#BtnResetG").button({
			icons : {primary : "ui-icon-refresh"}
		});
		$("#BtnCancelG").button({
			icons : {primary : "ui-icon-close"}
		});
		$("#search").button({
			icons : {primary : "ui-icon-circle-zoomin"}
		});
	});
	
</script>

</head>

<body>

	<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">
			<div class="ui-widget">
				<div class="ui-widget-header">Instant SMS</div>
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

							<Form id="InstantSms" style="display: inline;">

								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Single Messages</a></li>
										<li><a href="#tabs-2">Group Messages</a></li>
									</ul>
									
									<div id="tabs-1">
										<div>
											<div class="ui-widget-header">Single Messages</div>
											<br><br>
											
											<label id="labelCustomer">Customers ID:</label>
											<br>
											<input id="customerId" type="text" size="50" />
											<button id="singleSearch" type="button">Search Customers</button>

											<div style="visibility: visible; width: 250px;" id="id">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnId" class="message-text">
														<label>&nbsp;&nbsp;Please enter a template name.</label>
														<br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>
											
											Message:
											<br>
											<textarea id="MessageTextArea1" rows="6" cols="50" ></textarea>
											<br>
											<button type="button" id="BtnSelectTemplate">Select Template</button>

											<div style="visibility: visible; width: 250px;" id="message">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnMsg" class="message-text">
														<label>&nbsp;&nbsp;Please enter a template name.</label>
														<br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>

											<br>
											<button id="BtnSendSingle"	type="button" >Send</button>
											<button type="reset" id="BtnReset">Reset</button>
											<button type="button" id="BtnCancelSingle">Cancel</button>
											<br>
											<br>
										</div>
									</div>
									
									<div id="tabs-2">
										<div>
											<div class="ui-widget-header">Group Messages</div>
											<br>
											<br> <label id="labelGroup">Group Names:</label> 
											<br>
											<input type="text" size="50" id="groupsId">
											<button id="BtnGroupCustormize" type="button">Custormize Group</button>

											<div style="visibility: visible; width: 250px; display: '';"
												id="Gid">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnGId" class="message-text">
														<label>&nbsp;&nbsp;Please	enter a template name.</label><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>

											Message: 
											<br>
											<textarea id="MessageTextArea2" rows="6" cols="50"></textarea>
											<br>
											<button type="button" id="BtnSelectTemplateG">Select Template</button>

											<div style="visibility: visible; width: 250px;" id="gMsg">
												<div class="ui-widget message-container">
													<div class="ui-state-error ui-corner-all">
														<span class="ui-icon ui-icon-alert" style="float: left;">
														</span><span id="spnGMsg" class="message-text">
														<label>&nbsp;&nbsp;Please enter a template name.</label><br>
														<label>&nbsp;</label></span>
													</div>
												</div>
											</div>

											<br>
											<button id="BtnSendGroup" type="button">Send</button>
											<button type="reset" id="BtnResetG">Reset</button>
											<button type="button" id="BtnCancelG">Cancel</button>
											<br>
											<br>
											
										</div>
									</div>
								</div>
							</Form>
						</div>
						
						<div id="dialog-form" title="Search Contact">
							<form>
								<fieldset>
									<label for="name">Customer Name:</label>
									<input type="text" id="customerName" class="text ui-widget-content ui-corner-all"/>
									<button type="button" id="search">Search</button>
									<br><br><br>
									<div id="resultTable"></div>
								</fieldset>
							</form>
						</div>

						<div id="template-form" title="Add Template">
							<form>
								<fieldset>
									<div>
									Select Template Category: 
									<br>
									<select id="combobox2"></select>
									<br><br> 
									Select Template:
									<br>
									<select id="combobox3"></select>
									<br><br>
									Preview: 
									<br>
									<textarea id="TemplatePreviewMessage" rows="6" cols="50" readonly="readonly"></textarea>
									<br><br>
									</div>
								</fieldset>
							</form>
						</div>

						<div id="Remove-Customers" title="Custormize Customers">
							<form>
								<fieldset>
									<div id="resultTable2"></div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>