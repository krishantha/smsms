<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
body{
width:800px;
font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
}

</style>
<link rel="Stylesheet" href="css/jquery-ui-1.8.18.custom.css" type="text/css" />
<link rel="Stylesheet" href="css/ui.jqgrid.css" type="text/css" />
<link rel="stylesheet" href="css/body.css">
<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="js/grid.locale-en.js" type="text/javascript"  charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.jqGrid.min.js"></script>
<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;


var flag=false;

$(function() {
   
	$("#autocomplete").autocomplete({
        source:"AutoCompleteCustomer",
        minLength: 2,
        select: function(event, ui) {	
        	$("#button_search").click();
        }
		
    });   
	
	
			$("#button_search").click(function(){
		
				if(flag==false)
					{
			
						createGrid();
						flag=true;
					}
				
				
				else
					{
					
					jQuery("#results").jqGrid().setGridParam({url :'EditCustomer?keyword='+ $("#autocomplete").attr('value')}).trigger("reloadGrid");
					}
			

    		});

		    
		    function createGrid()
		    {
		    	
		    	 jQuery("#results").jqGrid({
				       	url:'EditCustomer?keyword='+ $("#autocomplete").attr('value'),
				    	datatype: "json",
				       	colNames:['Customer No','First Name', 'Middle Name', 'Last Name','DOB','Mobile No','Address'],
				       	colModel:[
				       		{name:'scustomerNo',index:'scustomerNo',width:90,editable:true,search:true, editrules: { edithidden: true,required: true }},
				       		{name:'sfirstName',index:'sfirstName',width:80,editable:true,search:true},
				       		{name:'smiddleName',index:'sMiddleName',width:100,editable:true,search:true},
				       		{name:'slastName',index:'slastName',width:100,editable:true,search:true},
				       		{name:'sdob',index:'sdob' ,width:80,editable:true,search:true, edittype: 'text',
				       		    editoptions: {
				       		      size: 10, maxlengh: 10,
				       		      dataInit: function(element) {
				       		        $(element).datepicker({dateFormat: 'yy-mm-dd'});
				       		      }
				       		    }	},
				       		{name:'smobileNo',index:'smobileNo' ,width:90,editable:true,search:true ,editrules: { regexp: /^[+]?\d*$/,required: true },editoptions: { size : 12 }},		
				       		{name:'saddress',index:'saddress' ,width:150,editable:true,search:true}	
				       		
				       			
				       	],
				       	rowNum:100, 
				       	height:400,
				       	sortname: 'scustomerNo',
				        viewrecords: true,
				        multiselect:false,
				        sortorder: "asc",
				        pager:'#pager',
				        editurl:"CustomerEdit",
				        caption:"Customers",
				        loadError: function(xhr,status,error){alert(status+" "+error);}
		    	 });
		    	 
		    	 
		    	 jQuery("#results").jqGrid('navGrid','#pager',{edit:true,add:true,del:true} );
		    }
		    
		    function getCellValue(rowId, cellId) {
		        var cell = jQuery('#' + rowId + '_' + cellId);        
		        var val = cell.val();
		        return val;
		    }
		    
		    
		    
	
});

</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Customer</title>

</head>
<body>
<div class="ui-widget">
	<div class="ui-widget-header">
	Edit Customer Profile
	</div>
	
	<div class="ui-widget-content" style="min-height:700px;">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;">
	Search for :&nbsp;&nbsp;<input id="autocomplete" style="z-index: 100; position: relative;width:230px;" title="Search for customers" />&nbsp;&nbsp;
	<button id="button_search" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"><span class="ui-button-text">Search</span></button><br><br>
	Search Results<br><br>
	<div id="searchresults">
	<table id="results" width="700">
	
	
	</table>
	<div id="pager"> </div>	
	
	</div>
	</div>
	</div>
</div>
</body>
</html>