<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to the SMS Gateway System</title>
<link rel="stylesheet" href="css/body.css">
<link rel="Stylesheet" href="css/jquery-ui-1.8.18.custom.css" type="text/css" />
<link rel="Stylesheet" href="css/styles_common.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript">

$(function(){
	
	function updateTips( t ) {
		tips
			.text( t )
			.addClass( "ui-state-highlight" );
		setTimeout(function() {
			tips.removeClass( "ui-state-highlight", 1500 );
		}, 500 );
	}
	
	
	function checkLength( o, n ) {
		if ( o.val().length <1) {
			o.addClass( "ui-state-error" );
			updateTips( n + " must be entered" );
			return false;
		} else {
			return true;
		}
	}
	
	var name = $( "#username" ),
	password = $( "#password" ),
	allFields = $( [] ).add( name ).add( password ),
	tips = $( ".validateTips" );
	
	$('#button_login').click(function(){
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );

		
		bValid = bValid && checkLength( name, "username");
		bValid = bValid && checkLength( password, "password" );
		
		if(!bValid)
			{
			return false;
			}
	
	
	
	});
	
	
	
	
	
	
	
$( "#dialog:ui-dialog" ).dialog( "destroy" );
	
$( "#dialog-form" ).dialog({
	autoOpen: true,
	height: 300,
	width: 300,
	modal: true,
	show: 'slide',
	
	close: function() {
		
		window.location.href = "login.jsp";
		
	}
});
});


</script>
</head>
<body>





<div id="dialog-form" title="Login First">
	<p class="validateTips">Provide your username and password to login to the system</p>

	<form action="Login" method="post">
	<fieldset>
		<label for="name">Username</label>
		<input type="text" name="username" id="username" class="text ui-widget-content ui-corner-all" />
		<label for="password">Password</label>
		<input type="password" name="password" id="password" class="text ui-widget-content ui-corner-all" />
		<button id="button_login" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover"><span class="ui-button-text">Login</span></button>
	</fieldset>
	
	</form>
</div>
</body>
</html>