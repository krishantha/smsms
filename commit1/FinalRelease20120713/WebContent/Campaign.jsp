<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    
    <!-- imported CSS files -->
	<link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
	<link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link rel="stylesheet" href="css/demos.css"> 
    <link rel="Stylesheet" href="css/dialog.css" type="text/css" />
    <link rel="stylesheet" href="css/body.css">




<script type="text/javascript">

function chkall(name){


	if(name=="checkadd"){
		
		if(document.getElementById('addall').checked){
		
		var checkBoxList=document.getElementsByName("repop");
		
			
		for(var a=0;a<=checkBoxList.length;a++){
				
			checkBoxList[a].checked=true;
		
		}
	}
	else{
		var checkBoxList=document.getElementsByName("repop");
		
		
		for(var a=0;a<=checkBoxList.length;a++){
				
			checkBoxList[a].checked=false;
		
		}
	}
	
	
	
	}
	else if(name=="checkedit"){
		
		if(document.getElementById('editall').checked){
				
		var checkBoxList=document.getElementsByName("repopx");
		
		for(var b=0;b<=checkBoxList.length;b++){
					
			checkBoxList[b].checked=true;
			
		}
		}
		else{
			var checkBoxList=document.getElementsByName("repopx");
			
			for(var b=0;b<=checkBoxList.length;b++){
						
				checkBoxList[b].checked=false;
				
			}
			
		}
			
	
	}
}

</script>



<script type="text/javascript">

	function changeState(id){
		
		var rad=$('input:radio[name=radio]:checked').val();
		
		if(id=="startcmp"){
			$(function(){
				
				var jx=$.ajax({

					type : "GET",
					url : "ChangeStatus",
					data:{ camp:rad, type:"act" }
								
						});
						jx.success(function (response, status) {
							
							loadtable();
							loadinfo();
							disable();
							
						});

						jx.error(function(request, error) {
							
					});
			});
			
			
		}
		else if(id=="stopcmp"){
			
			$(function(){
				
				var jx=$.ajax({

					type : "GET",
					url : "ChangeStatus",
					data:{ camp:rad, type:"inact" }
								
						});
						jx.success(function (response, status) {
							
							loadtable();
							loadinfo();
							disable();
							
						});

						jx.error(function(request, error) {
							
					});
			});
			
		}
		
	}

</script>



<script type="text/javascript">

	function disable(){
		
		$("#stopcmp").button( "disable" );
		$("#startcmp").button( "disable" );
	}

</script>


  <script type="text/javascript">
		
	function loadinfo(){
	
	
			
	$(function(){
		
		
		
		var jx=$.ajax({

			type : "GET",
			url : "LoadInfo",
			data:{}
						
				});
				jx.success(function (response, status) {
					
					
					var info=response.split(":");
					
					document.getElementById("act").innerText=info[0];
					document.getElementById("inact").innerText=info[1];
					
					
				});

				jx.error(function(request, error) {
					
			});
	});
	
	}
	
	</script>
	
	<script type="text/javascript">
	
	function chkstatus(id){
		
		$(function(){
			var jx=$.ajax({

				type : "GET",
				url : "CheckStatus",
				data :{ cid:id }
							
					});
					jx.success(function (response, status) {
						
						
						
						if(response=="inactive"){
							
							document.getElementById("stopcmp").checked=true;
							$("#stopcmp").button( "refresh" );
							$("#stopcmp").button( "enable" );
							$("#startcmp").button( "refresh" );
							$("#startcmp").button( "enable" );
							
						}
						else if(response=="active"){
							
							document.getElementById("startcmp").checked=true;
							$("#stopcmp").button( "refresh" );
							$("#stopcmp").button( "enable" );
							$("#startcmp").button( "refresh" );
							$("#startcmp").button( "enable" );
						}
						
						
					});

					jx.error(function(request, error) {
						
				});
		});		
		
	}
	
	
	</script>


<script type="text/javascript">
		
	function loadtable(){
		
	$(function(){
		var jx=$.ajax({

			type : "GET",
			url : "LoadCampaigns"			
						
				});
				jx.success(function (response, status) {
					
					$("#cmptble").html(response);
					
					
				});

				jx.error(function(request, error) {
					
			});
	});
	
	}
	
	</script>


<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var name = $( "#cmpname" ),
			des = $( "#cmpdes" ),
			scode = $( "#shortcode" ),
			sdate = $( "#stdate" ),
			edate = $( "#endate" ),
			rep=$("#reply"),
			allFields = $( [] ).add( name ).add( scode ).add( sdate ).add( edate ).add( rep ).add( des ),
			tips = $( ".validateTips" );

		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be between " +
					min + " and " + max + "." );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}
		
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 480,
			width: 350,
			modal: true,
			buttons: {
				
				"Create campaign": function() {
					var bValid = true;
					allFields.removeClass( "ui-state-error" );

					bValid = bValid && checkLength( name, "campaign name", 3, 25 );
					bValid = bValid && checkLength( des, "description", 0, 200 );
					bValid = bValid && checkLength( scode, "shortcode", 1, 15 );
					

					//bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Group name may consist of a-z, 0-9, underscores, begin with a letter." );
					// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
					
					if($("#stdate").val()!=""&&$("#endate").val()!=""){
						
						t1=$("#stdate").val();
					    t2=$("#endate").val();
					    
					    var one_day=1000*60*60*24;
					    
					    var x=t1.split("-");     
					    var y=t2.split("-");
					    
					    var date1=new Date(x[0],(x[1]-1),x[2]);  
					    var date2=new Date(y[0],(y[1]-1),y[2]);
					    var month1=x[1]-1;
					    var month2=y[1]-1;
					    
					    _Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day));
					    
					    if(_Diff<0){
					    	
					    	updateTips("Invalid duraton. Please check the start and end dates");
					    	sdate.addClass( "ui-state-error" );
					    	edate.addClass( "ui-state-error" );
					    	bValid=false;
					    }
						
					}
					
					
					var cmpname=$("#cmpname").val();
					var cmpdesc=$("#cmpdes").val();
					var shrtcode=$("#shortcode").val();
					
					if($("#stdate").val()==""){
						var stdate="N/A";
					}
					else{
						var stdate=$("#stdate").val();
					}
					
					if($("#endate").val()==""){
						var endate="N/A";
					}
					else{
						var endate=$("#endate").val();
					}
					
					
					var reply=$("#reply").val();
				
				
					if ( bValid ) {
				
					var jq=$.ajax({
					
						type: "GET",
						url: "AddCampaign",
						data: {cname : cmpname , cdesc : cmpdesc , startdate:stdate , enddate:endate , shortcode:shrtcode , rep:reply}
				
						});
								
					
						jq.success(function (response,status){
							if(response=="shrtcodeExist"){
								
								scode.addClass("ui-state-error");
								updateTips( "Short code \""+shrtcode+"\" is already exists, try another one." );
								
							}
							else{
								$().toastmessage('showSuccessToast', "Campaign Added");
								$( "#dialog-form" ).dialog( "close" );
								$("#cmptble").html(response);
								loadinfo();
								disable();
								
							}
							});
						
						jq.error(function (request,error){
								$().toastmessage('showErrorToast', "Campaign cannot be added");
						}); 
						
					}
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
				
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#btn_addcmp" )
			.button()
			.click(function() {
				document.getElementById('message').style.visibility='hidden';
				$( "#dialog-form" ).dialog( "open" );
			});
	});
	
	
	</script>
	
	
	
	
	
	<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		var name = $( "#cmpname1" ),
			des = $( "#cmpdes1" ),
			scode = $( "#shortcode1" ),
			sdate = $( "#stdate1" ),
			edate = $( "#endate1" ),
			rep=$("#reply1"),
			allFields = $( [] ).add( name ).add( scode ).add( sdate ).add( edate ).add( rep ).add( des ),
			tips = $( ".validateTips" );

		function updateTips( t ) {
			tips
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				tips.removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be between " +
					min + " and " + max + "." );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}
		
		$( "#dialog-form1" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 480,
			width: 350,
			modal: true,
			buttons: {
				
				"Update campaign": function() {
					var bValid = true;
					allFields.removeClass( "ui-state-error" );

					bValid = bValid && checkLength( name, "campaign name", 3, 25 );
					bValid = bValid && checkLength( des, "description", 0, 200 );
					bValid = bValid && checkLength( scode, "shortcode", 1, 15 );
					

					//bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Group name may consist of a-z, 0-9, underscores, begin with a letter." );
					// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
					
						if((!$("#stdate1").val()=="")&&(!$("#endate1").val()=="")){
						
						    
						t1=$("#stdate1").val();
					    t2=$("#endate1").val();
					    
					    var one_day=1000*60*60*24;
					    
					    var x=t1.split("-");     
					    var y=t2.split("-");
					    
					    var date1=new Date(x[0],(x[1]-1),x[2]);  
					    var date2=new Date(y[0],(y[1]-1),y[2]);
					    var month1=x[1]-1;
					    var month2=y[1]-1;
					    
					   	var Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day));
					    
					    
					    if(Diff<0){
					    	
					    	updateTips("Invalid duraton. Please check the start and end dates");
					    	sdate.addClass( "ui-state-error" );
					    	edate.addClass( "ui-state-error" );
					    	bValid=false;
					    }
						
					}
					
					
					var cmpname=$("#cmpname1").val();
					var cmpdesc=$("#cmpdes1").val();
					var shrtcode=$("#shortcode1").val();
					
					if($("#stdate1").val()==""){
						var stdate="N/A";
					}
					else{
						var stdate=$("#stdate1").val();
					}
					
					if($("#endate1").val()==""){
						var endate="N/A";
					}
					else{
						var endate=$("#endate1").val();
					}
					
					var reply=$("#reply1").val();
				
				
					if ( bValid ) {
				
					var jq=$.ajax({
					
						type: "GET",
						url: "EditCampaignClass",
						data: {cname : cmpname , cdesc : cmpdesc , startdate:stdate , enddate:endate , shortcode:shrtcode , rep:reply}
				
						});
								
					
						jq.success(function (response,status){
							if(response=="notExist"){
								
								scode.addClass("ui-state-error");
								updateTips( "The system cannot find the Short code \""+shrtcode+"\" " );
								
							}
							else{
								$().toastmessage('showSuccessToast', "Campaign Updated");
								$( "#dialog-form1" ).dialog( "close" );
								$("#cmptble").html(response);
								loadinfo();
								disable();
								
							}
							});
						
						jq.error(function (request,error){
								$().toastmessage('showErrorToast', "Campaign not updated");
						}); 
						
					}
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
				
			},
			close: function() {
				allFields.val( "" ).removeClass( "ui-state-error" );
			}
		});

		$( "#btn_editcmp" )
			.button()
			.click(function() {
								
				document.getElementById('message').style.visibility='hidden';
				if($('input:radio[name=radio]:checked').size()==0){
					
					$("#ermsg").html("<lable style=\"font-size: 12px;font-weight: bold\";>Please select a campaign to edit</lable><br/><label>&nbsp;</label>");
					document.getElementById('message').style.visibility='visible';
				}
				else{
					
					var rad=$('input:radio[name=radio]:checked').val();
					
					var jd=$.ajax({
						
						type: "GET",
						url: "GetCampaignClass",
						data: {shortcode:rad}
				
						});
								
					
						jd.success(function (response,status){
							if(response=="codeNotExist"){
								
								scode.addClass("ui-state-error");
								updateTips( "Sorry, the short code \""+shrtcode+"\" is not in use." );
								
							}
							else{
								
								var data=response.split("@:");
								
								document.getElementById("cmpname1").value=data[0];
								document.getElementById("cmpdes1").value=data[1];
								document.getElementById("shortcode1").value=data[2];
								document.getElementById("stdate1").value=data[3];
								document.getElementById("endate1").value=data[4];
								document.getElementById("reply1").value=data[5];
								
								$( "#dialog-form1" ).dialog( "open" );
								
							}
							});
						
						jd.error(function (request,error){
								$().toastmessage('showErrorToast', "Campaign cannot be updated");
						}); 
					
									
				}
								
			});
	});
	
	
	</script>
	
	
	
	<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		
		
		$( "#dialog-form2" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 480,
			width: 350,
			modal: true,
			buttons: {
				
				"Add": function() {
							
					var id=0;
					var test = [];
					$("input:checkbox[name=repop]:checked").each(function()
							{
							    test.push($(this).val());
							    id=$(this).val()+","+id;
							});
					
					
					
					if(test.length==0){
						document.getElementById('reply').value="";
						$( this ).dialog( "close" );
						
					}
					else{
						
						var newstr=id.substr(0,id.length-2);
						
						document.getElementById('reply').value=newstr;
						$( this ).dialog( "close" );
						
					}
					
					
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
				
			},
			close: function() {
				
				document.getElementById('erroptions').style.visibility='hidden';
			}
		});

		$( "#btn_addrep" )
			.button()
			.click(function() {
								
				document.getElementById('erroptions').style.visibility='hidden';
				
				
				var jd=$.ajax({
					
					type: "GET",
					url: "LoadReplyOptions",
					data: {type:"add"}
			
					});
							
				
					jd.success(function (response,status){
						if(response=="codeNotExist"){
							
							
							updateTips( "Sorry, The system cannot find reply options" );
							
						}
						else{
							
							$("#repoption").html(response);
							
							$( "#dialog-form2" ).dialog( "open" );
							
							
							var strx=$("#reply").val();
							
							var arrx=[];
							arrx=strx.split(",");
					
							for(var i=0;i<=arrx.length;i++){
																
								document.getElementById(arrx[i]).checked=true;							
							}
							
						}
						});
					
					jd.error(function (request,error){
							$().toastmessage('showErrorToast', "Campaign cannot be updated");
					}); 
				
							
				
				
								
			});
	});
	
	
	</script>
	
	
		<script type="text/javascript">
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
		
		
		
		$( "#dialog-form3" ).dialog({
			autoOpen: false,
			show:"drop",
			hide:"drop",
			height: 480,
			width: 350,
			modal: true,
			buttons: {
				
				"Add": function() {
							
					var idx=0;
					var testx = [];
					$("input:checkbox[name=repopx]:checked").each(function()
							{
							    testx.push($(this).val());
							    idx=$(this).val()+","+idx;
							});
					
					
					
					if(testx.length==0){
						document.getElementById('reply1').value="";
						$( this ).dialog( "close" );
						
					}
					else{
						
						var newstrx=idx.substr(0,idx.length-2);
						
						document.getElementById('reply1').value=newstrx;
						$( this ).dialog( "close" );
						
					}
					
					
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
				
			},
			close: function() {
								
				document.getElementById('erroptions1').style.visibility='hidden';
			}
		});

		$( "#btn_addrep1" ).button().click(function() {
			
				
								
				document.getElementById('erroptions1').style.visibility='hidden';
				
				
				var jd=$.ajax({
					
					type: "GET",
					url: "LoadReplyOptions",
					data: {type:"edit"}
			
					});
							
				
					jd.success(function (response,status){
						if(response=="codeNotExist"){
							
							
							updateTips( "Sorry, The system cannot find reply options" );
							
						}
						else{
							$( "#dialog-form3" ).dialog( "open" );
							
							$("#repoption1").html(response);
							
							var str=$("#reply1").val();
											
							var arr=[];
							arr=str.split(",");
					
							for(var i=0;i<=arr.length;i++){
																
								document.getElementById('x'+arr[i]).checked=true;							
							}
						
						}
						});
					
					jd.error(function (request,error){
							$().toastmessage('showErrorToast', "Campaign cannot be updated");
					}); 
				
				
				
				
				
			});
	});
	
	
	</script>
	

	<script type="text/javascript">
	
	$(function(){
		
		$("#btn_delcmp").click(function(){
			
			document.getElementById('message').style.visibility='hidden';
			
			if($('input:radio[name=radio]:checked').size()==0){
				
				$("#ermsg").html("<lable style=\"font-size: 12px;font-weight: bold\";>Please select a campaign to delete</lable><br/><label>&nbsp;</label>");
				document.getElementById('message').style.visibility='visible';	
				
			}
			else{
				
				$( "#dialog:ui-dialog" ).dialog( "destroy" );
				
				$( "#dialog-confirm" ).dialog({
					resizable: false,
					show:"drop",
					hide:"drop",
					height:140,
					modal: true,
					buttons: {
						
						"Delete": function() {
							$( this ).dialog( "close" );
							var rad=$('input:radio[name=radio]:checked').val();
							
							var del=$.ajax({
								
								type : "GET",
								url : "DeleteCampaign",
								data : {camp : rad}										
							});

							del.success(function(response,status){			
								if(response=="notExist"){
									$().toastmessage('showErrorToast',"Failed to delete group");
										
								}else{
									$("#cmptble").html(response);
									$().toastmessage('showSuccessToast', "Campaign deleted");
									loadinfo();
									disable();
									
								}
							});

							del.error(
									function(status,error) {
										
										$().toastmessage('showErrorToast',"Failed to delete campaign");
							});
						},
						Cancel: function() {
							$( this ).dialog( "close" );
						}
						
					}
				});
				
				
				
			}
			
			
			
			
			
		});
		
	});
	
	</script>

	<!-- script for adding icons for buttons -->
	<script type="text/javascript">
	
	$(function(){
		
		$("#btn_addcmp").button({icons: {primary: "ui-icon-circle-plus"}});
		$("#btn_delcmp").button({icons: {primary: "ui-icon-trash"}});
		$("#btn_editcmp").button({icons: {primary: "ui-icon-pencil"}});
		$("#startcmp").button({icons: {primary: "ui-icon-play"}});
		$("#stopcmp").button({icons: {primary: "ui-icon-stop"}});
		$("#dyes").button({icons: {primary: "ui-icon-check"}});
		$("#dno").button({icons: {primary: "ui-icon-closethick"}});
		$("#btn_addrep").button({icons: {primary: "ui-icon-circle-plus"}});
		$("#btn_addrep1").button({icons: {primary: "ui-icon-circle-plus"}});
	});
	
	</script>
	
	<script type="text/javascript">
	
	$(function(){
		$( "#duration" ).buttonset();
		$( "#cmpstatus" ).buttonset();
		
	});
	
	</script>
	
	
	
<!-- script for connecting date picker to date of birth field -->
	<script type="text/javascript">
	
	$(function(){
		
		$( "#stdate" ).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',minDate: "0"});
		$( "#endate" ).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',minDate: "0"});
		$( "#stdate1" ).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',minDate: "0"});
		$( "#endate1" ).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',minDate: "0"});
		
	});
	
	</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body onload="loadtable();loadinfo();disable();">
<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">

			<div class="ui-widget">
				<div class="ui-widget-header">Manage Campaigns</div>
				
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

						<form name="Groups" id="grp">
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
							
							<p><span class="ui-icon ui-icon-contact" style="float: left; margin-right: .3em;"></span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>Active campaigns&nbsp;&nbsp;:&nbsp;</strong><label style="display: inline; color: green;" id="act"></label>
							<strong>&nbsp;&nbsp;Inactive campaigns&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</strong><label style="display: inline; color: red;" id="inact"></label>
							
							</p>

						</div><br/>
						
						<button class="ui-state-default" type="button" id="btn_addcmp" value="addnew" >Add new campaign</button>
						<button class="ui-state-default" type="button" id="btn_delcmp" value="delete" >Delete campaign</button>
						<button class="ui-state-default" type="button" id="btn_editcmp" value="edit" >Edit campaign</button>
										
					<span style="float: right;" id="cmpstatus" class="ui-buttonset">
						<input type="radio" onclick="changeState(this.id);" id="startcmp" class="ui-helper-hidden-accessible" name="ststus" checked="checked" /><label for="startcmp" class="ui-button ui-widget ui-state-default ui-corner-left ui-state-active" aria-pressed="true" role="button" aria-disabled="false">Start</label>
						<input type="radio" onclick="changeState(this.id);" id="stopcmp" class="ui-helper-hidden-accessible" name="ststus" /><label for="stopcmp" class="ui-button ui-widget ui-state-default ui-corner-right" aria-pressed="false" role="button" aria-disabled="false">Stop</label>
					</span>
					
						<div align="center">
						<div style="visibility:hidden; width: 500px;" id="message">
							<div class="ui-widget message-container">
								<div class="ui-state-error ui-corner-all">
									<span class="ui-icon ui-icon-alert" style="float:left;"></span>
									<span id="ermsg" class="message-text"></span>
								</div>
							</div>
						</div>
						</div>
						
						<div align="center" id="cmptble"></div>
						
						
						</form>
						</div>
						
						<div id="dialog-confirm" title="Delete campaign!" style="display: none;">
							<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This campaign will be permanently deleted and cannot be recovered. Are you sure?</p>
						</div>
						
						
						<div align="left" id="dialog-form" title="Create new campaign" style="display: none;">
							<p align="left" class="validateTips">Please fill all the required fields *</p>

						<form>
							<fieldset>
								<label for="cmpname">Campaign Name *</label>
								<input type="text" name="txt_cmpname" id="cmpname" class="text ui-widget-content ui-corner-all" />
								<label for="cmpdes">Description</label>
								<input type="text" name="txt_cmpdes" id="cmpdes" class="text ui-widget-content ui-corner-all" />
								<label for="shortcode">Short code *</label>
								<input type="text" name="txt_shrtcode" id="shortcode" class="text ui-widget-content ui-corner-all" />
								<!--
								<label style="float: left;font-weight: bold;">Set Duration</label>
								  <span style="float: right;" id="duration" class="ui-buttonset">
									<input type="radio" id="dyes" class="ui-helper-hidden-accessible" name="dur" checked="checked" /><label for="dyes" class="ui-button ui-widget ui-state-default ui-corner-left ui-state-active" aria-pressed="true" role="button" aria-disabled="false">Yes</label>
									<input type="radio" id="dno" class="ui-helper-hidden-accessible" name="dur" /><label for="dno" class="ui-button ui-widget ui-state-default ui-corner-right" aria-pressed="false" role="button" aria-disabled="false">No</label>
								</span>-->
								
								<label for="stdate">Start date</label>
								<input type="text" name="txt_startdate" id="stdate" class="text ui-widget-content ui-corner-all" />
								<label for="endate">End date</label>
								<input type="text" name="txt_enddate" id="endate" class="text ui-widget-content ui-corner-all" />
								<label style="float: left;font-weight: bold;" for="reply">Add reply options</label>
								<button style="float: right;" class="ui-state-default" type="button" id="btn_addrep" value="add" >Add</button><br/>
								<input type="text" disabled="disabled" name="txt_reply" id="reply" class="text ui-widget-content ui-corner-all" />
								
							</fieldset>
						</form>
						</div>
						
						
						<div align="left" id="dialog-form1" title="Edit campaign" style="display: none;">
							<p align="left" class="validateTips">Please fill all the required fields *</p>

						<form>
							<fieldset>
								<label for="cmpname1">Campaign Name *</label>
								<input type="text" name="txt_cmpname1" id="cmpname1" class="text ui-widget-content ui-corner-all" />
								<label for="cmpdes1">Description</label>
								<input type="text" name="txt_cmpdes1" id="cmpdes1" class="text ui-widget-content ui-corner-all" />
								<label for="shortcode1">Short code *</label>
								<input disabled="disabled" type="text" name="txt_shrtcode1" id="shortcode1" class="text ui-widget-content ui-corner-all" />
								<!--
								<label style="float: left;font-weight: bold;">Set Duration</label>
								  <span style="float: right;" id="duration" class="ui-buttonset">
									<input type="radio" id="dyes" class="ui-helper-hidden-accessible" name="dur" checked="checked" /><label for="dyes" class="ui-button ui-widget ui-state-default ui-corner-left ui-state-active" aria-pressed="true" role="button" aria-disabled="false">Yes</label>
									<input type="radio" id="dno" class="ui-helper-hidden-accessible" name="dur" /><label for="dno" class="ui-button ui-widget ui-state-default ui-corner-right" aria-pressed="false" role="button" aria-disabled="false">No</label>
								</span>-->
								
								<label for="stdate1">Start date</label>
								<input type="text" name="txt_startdate1" id="stdate1" class="text ui-widget-content ui-corner-all" />
								<label for="endate1">End date</label>
								<input type="text" name="txt_enddate1" id="endate1" class="text ui-widget-content ui-corner-all" />
								<label style="float: left;font-weight: bold;" for="reply1">Add reply options</label>
								<button style="float: right;" class="ui-state-default" type="button" id="btn_addrep1" value="add" >Add</button><br/>
								<input type="text" disabled="disabled" name="txt_reply1" id="reply1" class="text ui-widget-content ui-corner-all" />
								
							</fieldset>
						</form>
						</div>
						
						
						
						<div align="left" id="dialog-form2" title="Reply options" style="display: none;">
							

						<form>
							<fieldset>
								
							<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
							
							<p><span class="ui-icon ui-icon-contact" style="float: left; margin-right: .3em;"></span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>Add reply options to this campaign</strong><br/><label style="color: red; visibility: hidden;" id="erroptions">Please select options to add</label>
							</p>
							
							

						</div><br/>
								<div align="center" id="repoption"></div>
								
								
							</fieldset>
						</form>
						</div>
						
						
						<div align="left" id="dialog-form3" title="Reply options" style="display: none;">
							

						<form>
							<fieldset>
								
							<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
							
							<p><span class="ui-icon ui-icon-contact" style="float: left; margin-right: .3em;"></span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<strong>Add reply options to this campaign</strong><br/><label style="color: red; visibility: hidden;" id="erroptions1">Please select options to add</label>
							</p>
							
							

						</div><br/>
								<div align="center" id="repoption1"></div>
								
								
							</fieldset>
						</form>
						</div>
						
					</div>
					
					

				</div>
			</div>
		</div>
</div>
</body>
</html>