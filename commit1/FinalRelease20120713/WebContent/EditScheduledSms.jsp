<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script	src="js/jquery-1.7.2.min.js"></script>
<script	src="js/jquery-ui-1.8.20.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.toastmessage.js"></script>
<link href="css/sunny/jquery-ui-1.8.20.custom.css"	rel="stylesheet" type="text/css" />
<link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
<link rel="stylesheet" href="css/demos.css">
<link rel="stylesheet" type="text/css" media="screen" href="grid/css/ui.jqgrid.css" />
<script src="grid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="grid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Scheduled SMS</title>

<script type="text/javascript">

	$(function() {
		
		$('#Dtime').timepicker({});
		$("#Ddate").datepicker({
			changeMonth : true,
			changeYear : true,
			dateFormat : 'yy-mm-dd',
			minDate : 0,
			showAnim: 'fold',
		});
		$('#Dtime3').timepicker({});
		$("#Ddate3").datepicker({
			changeMonth : true,
			changeYear : true,
			dateFormat : 'yy-mm-dd',
			minDate : 0,
			showAnim: 'fold',
		});
	});

</script>

<script type="text/javascript">

	$(function() { 
		$("#tabs").tabs({
			fx : [ 
			    	{opacity : 'toggle',duration : 'slow'},
					{opacity : 'toggle',duration : 'slow'} 
			    ]
		});});
	
</script>

<script type="text/javascript">        
  
  $(function () {
	  
	  document.getElementById('error').style.visibility='hidden';
	  document.getElementById('error2').style.visibility='hidden';
	  document.getElementById('error3').style.visibility='hidden';
	  $.ajax({
  		type : "GET",
  		url : "ScheduledClass",
  		data : {
  		type : "getSingleScheduledMsg"
  			}										
  	})
  	.success(function(response,status){	  
	  $("#list").jqGrid({	  	  
		  datatype: "jsonstring",
          datastr: response,
          colNames:['','ID','Message', 'DateToBeSent','TimeToBeSent','Status'],
          colModel: [
              {name:"count",index:"count",width:30},      
              {name:"id",index:"id",width:60},
              {name:"message",index:"message",width:185},
              {name:"date",index:"date",width:80},
              {name:"time",index:"time",width:80},
              {name:"status",index:"status",width:80}
              ],          
          jsonReader: {
              repeatitems: false,
          },
          multiselect:false,
          pager: '#pager',		  
		  viewrecords: true,
		  sortname: 'id',
		  sortorder: "desc",		    
		  caption: 'Single SMS',
		  onSelectRow: function(id){		    		
		    	var test = $("#list").getRowData(id);
		    	setRow(test);
		    	document.getElementById('error').style.visibility='hidden';
		    	}		    
      });
	  jQuery("#list").jqGrid('navGrid',"#pager",{edit:false,add:false,del:false});	 	 
  })
  .error(function() {});            	
});
  
</script>

<script type="text/javascript">

	var myId;
	function setRow(id){
		myId = id;
	}
	function getRow(){
		return myId;
	}

</script>

<script type="text/javascript">
        
  $(function() { 
	  
	  $.ajax({
  		type : "GET",
  		url : "ScheduledClass",
  		data : {
  		type : "getGroupScheduledMsg"
  			}										
  	})
  	.success(function(response,status){	
	  
	  $("#list2").jqGrid({
		  
		  datatype: "jsonstring",
          datastr: response,
          colNames:['No','ID','Message', 'DateToBeSent','TimeToBeSent','Status'],
          colModel: [
              {name:"count",index:"count",width:30},      
              {name:"id",index:"id",width:50},
              {name:"message",index:"message",width:195},
              {name:"date",index:"date",width:80},
              {name:"time",index:"time",width:80},
              {name:"status",index:"status",width:80}
              ],          
          jsonReader: {
              repeatitems: false
          },
          	pager: '#pager2',
          	rowNum:10,
          	viewrecords: true,
          	sortname: 'id',
		    sortorder: "desc",		    		    
		    caption: 'Induvidual Message Updating Panel',
		    onSelectRow: function(id){		    		
		    	var test = $("#list2").getRowData(id);
		    	setRow(test);
		    	document.getElementById('error2').style.visibility='hidden';
		    	}
		    
      });
	  jQuery("#list2").jqGrid('navGrid',"#pager2",{edit:false,add:false,del:false});	  
	 
  	})
	.error(function() {});            	
});
  
</script>

<script type="text/javascript">
        
  $(function() { 
	  
	  $.ajax({
  		type : "GET",
  		url : "ScheduledClass",
  		data : {
  		type : "getGroupWiseScheduledMsg"
  			}										
  	})
  	.success(function(response,status){	
	  
	  $("#list3").jqGrid({
		  
		  datatype: "jsonstring",
          datastr: response,
          colNames:['','Group','Message', 'DateToBeSent','TimeToBeSent','CreatedDate','CreatedTime','Status'],
          colModel: [
              {name:"count",index:"count",width:10},      
              {name:"group",index:"group",width:35},
              {name:"message",index:"message",width:110},
              {name:"date",index:"date",width:80},
              {name:"time",index:"time",width:80},
              {name:"cdate",index:"cdate",width:70},
              {name:"ctime",index:"ctime",width:70},
              {name:"status",index:"status",width:50}
              ],          
          jsonReader: {
              repeatitems: false
          },
          	pager: '#pager3',
          	rowNum:10,
          	viewrecords: true,
          	sortname: 'id',
		    sortorder: "desc",		    		    
		    caption: 'Group Message Updating Panel',
		    onSelectRow: function(id){		    		
		    	var test = $("#list3").getRowData(id);
		    	setRow(test);
		    	document.getElementById('error3').style.visibility='hidden';
		    	}
		    
      });
	  jQuery("#list3").jqGrid('navGrid',"#pager3",{edit:false,add:false,del:false});	  
	 
  	})
	.error(function() {});            	
});
  
</script>


<script type="text/javascript">

	$(function() {
		$("#dialog-confirm").dialog({autoOpen : false});
		$("#dialog-confirm3").dialog({autoOpen : false});
	});

</script>

<script type="text/javascript">

	$(function() {
		
		$("#dialog-form").dialog({
			autoOpen : false,
			show: "drop",
			hide:"drop",
			height : 400,
			width : 400,
			modal : true,
			buttons : {
				
				"Save" : function() { 
					
					var Row = getRow();
					var primary = Row.count;
					var cid = $("#Did").val();
					var msg = $("#Dmessage").val();
					var edate = $("#Ddate").val();
					var etime = $("#Dtime").val();
					
					 $.ajax({
					  		type : "GET",
					  		url : "ScheduledClass",
					  		data : {
					  		count:primary,
					  		id:cid,
					  		message:msg,
					  		date:edate,
					  		time:etime,
					  		type : "editScheduledMsg"
					  		}										
					  	})
					  	.success(function(response,status){					  		
					  		
					  		$().toastmessage('showSuccessToast',"Message updated.");
					  		window.setTimeout(function(){location.reload();},500);
					  		
					  	})
						.error(
								function() {
									$().toastmessage('showWarningToast',"Editing failed.Please try again.");
								});	
					
					$(this).dialog("close");
					
				},
				"Cancel" : function() { $(this).dialog("close");}

			},
			close : function() {allFields.val("").removeClass("ui-state-error");}

		});
		
		
		$("#dialog-form3").dialog({
			autoOpen : false,
			show: "drop",
			hide:"drop",
			height : 400,
			width : 400,
			modal : true,
			buttons : {
				
				"Save" : function() { 
					
					var Row = getRow();
					var primary = Row.count;
					var cid = $("#Did3").val();
					var msg = $("#Dmessage3").val();
					var edate = $("#Ddate3").val();
					var etime = $("#Dtime3").val();
					var date = Row.cdate;
					var time = Row.ctime;
					
					 $.ajax({
					  		type : "GET",
					  		url : "ScheduledClass",
					  		data : {
					  		count:primary,
					  		group:cid,
					  		message:msg,
					  		date:edate,
					  		time:etime,
					  		cdate :date,
					  		ctime : time,
					  		type : "editGroupScheduledMsg"
					  		}										
					  	})
					  	.success(function(response,status){					  		
					  		
					  		$().toastmessage('showSuccessToast',"Message updated.");
					  		window.setTimeout(function(){location.reload();},500);
					  		
					  	})
						.error(
								function() {
									$().toastmessage('showWarningToast',"Editing failed.Please try again.");
								});	
					
					$(this).dialog("close");
					
				},
				"Cancel" : function() { $(this).dialog("close");}

			},
			close : function() {allFields.val("").removeClass("ui-state-error");}

		});
		
		
		
		
		$("#editSingle").click(function() {
			
			var Row = getRow();			
			if(Row!=null){				
				$("#dialog-form").dialog("open");
				$("#Did").val(Row.id);
				$("#Dmessage").val(Row.message);
				$("#Ddate").val(Row.date);
				$("#Dtime").val(Row.time);
			}
			else{
				
				$("#spnerror").text("");
				$("#spnerror").append("<label>&nbsp;&nbsp;Please select a message to edit.</label><br><label>&nbsp;</label>");
				document.getElementById('error').style.visibility='visible';
				
			}			
		});
		$("#editGroup").click(function() {
			var Row = getRow();
			if(Row!=null){
				$("#dialog-form").dialog("open");
				$("#Did").val(Row.id);
				$("#Dmessage").val(Row.message);
				$("#Ddate").val(Row.date);
				$("#Dtime").val(Row.time);
			}
			else{
				
				$("#spnerror2").text("");
				$("#spnerror2").append("<label>&nbsp;&nbsp;Please select a message to edit.</label><br><label>&nbsp;</label>");
				document.getElementById('error2').style.visibility='visible';
				
			}
			
		});
		$("#editGroup3").click(function() {
			var Row = getRow();
			if(Row!=null){
				$("#dialog-form3").dialog("open");
				$("#Did3").val(Row.group);
				$("#Dmessage3").val(Row.message);
				$("#Ddate3").val(Row.date);
				$("#Dtime3").val(Row.time);
			}
			else{
				
				$("#spnerror3").text("");
				$("#spnerror3").append("<label>&nbsp;&nbsp;Please select a message to edit.</label><br><label>&nbsp;</label>");
				document.getElementById('error3').style.visibility='visible';
				
			}
			
		});
		$("#deleteSingle").click(function() {
			var Row = getRow();
			if(Row!=null){				
				$(function() {					
					$( "#dialog:ui-dialog" ).dialog( "destroy" );				
					$( "#dialog-confirm" ).dialog({
						autoOpen : true,
						resizable: false,
						show:"drop",
						hide:"drop",
						height:140,
						modal: true,
						buttons: {		
							
							"Delete Message": function() {
								var primary = Row.count;
								$.ajax({
							  		type : "GET",
							  		url : "ScheduledClass",
							  		data : {
							  		count:primary,							  		
							  		type : "deleteScheduledMsg"
							  		}										
							  	})
							  	.success(function(response,status){						  		
							  		
							  		$().toastmessage('showSuccessToast',"Message deleted.");
							  		window.setTimeout(function(){location.reload();},500);
							  		
							  	})
								.error(function() {
											$().toastmessage('showWarningToast',"Deleting failed.Please try again.");
								});								
								$( this ).dialog( "close" );
							},
							Cancel: function() {
								$( this ).dialog( "close" );
							}
						}
					});
				});
								
			}
			else{
				
				$("#spnerror").text("");
				$("#spnerror").append("<label>&nbsp;&nbsp;Please select a message to delete.</label><br><label>&nbsp;</label>");
				document.getElementById('error').style.visibility='visible';
				
			}			
		});
		$("#deleteGroup").click(function() {
			var Row = getRow();
			if(Row!=null){				
				$(function() {
					// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
					$( "#dialog:ui-dialog" ).dialog( "destroy" );				
					$( "#dialog-confirm" ).dialog({
						autoOpen : true,
						resizable: false,
						show:"drop",
						hide:"drop",
						height:140,
						modal: true,
						buttons: {	
							"Delete Message": function() {
								var primary = Row.count;
								$.ajax({
							  		type : "GET",
							  		url : "ScheduledClass",
							  		data : {
							  		count:primary,							  		
							  		type : "deleteScheduledMsg"
							  		}										
							  	})
							  	.success(function(response,status){
							  		$().toastmessage('showSuccessToast',"Message deleted.");
							  		window.setTimeout(function(){location.reload();},1000);
							  	})
								.error(function() {
											$().toastmessage('showWarningToast',"Deleting failed.Please try again.");
								});							
								$( this ).dialog( "close" );
							},
							"Cancel": function() {
								$( this ).dialog( "close" );
							}
						}
					});
				});								
			}
			else{
				
				$("#spnerror2").text("");
				$("#spnerror2").append("<label>&nbsp;&nbsp;Please select a message to delete.</label><br><label>&nbsp;</label>");
				document.getElementById('error2').style.visibility='visible';
				
			}			
		});
		
		$("#deleteGroup3").click(function() {
			var Row = getRow();
			if(Row!=null){				
				$(function() {
					// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
					$( "#dialog:ui-dialog" ).dialog( "destroy" );				
					$( "#dialog-confirm" ).dialog({
						autoOpen : true,
						resizable: false,
						show:"drop",
						hide:"drop",
						height:140,
						modal: true,
						buttons: {	
							"Delete Message": function() {
								var primary = Row.count;
								$.ajax({
							  		type : "GET",
							  		url : "ScheduledClass",
							  		data : {
							  		count:primary,							  		
							  		type : "deleteGroupScheduledMsg"
							  		}										
							  	})
							  	.success(function(response,status){
							  		$().toastmessage('showSuccessToast',"Message deleted.");
							  		window.setTimeout(function(){location.reload();},1000);
							  	})
								.error(function() {
											$().toastmessage('showWarningToast',"Deleting failed.Please try again.");
								});							
								$( this ).dialog( "close" );
							},
							"Cancel": function() {
								$( this ).dialog( "close" );
							}
						}
					});
				});								
			}
			else{
				
				$("#spnerror3").text("");
				$("#spnerror3").append("<label>&nbsp;&nbsp;Please select a message to delete.</label><br><label>&nbsp;</label>");
				document.getElementById('error3').style.visibility='visible';
				
			}			
		});
		
	});
	
</script>

<script type="text/javascript">

$(function() {
	
	$("#editSingle").button({icons: {primary: "ui-icon-pencil"}});
	$("#editGroup").button({icons: {primary: "ui-icon-pencil"}});
	$("#editGroup3").button({icons: {primary: "ui-icon-pencil"}});
	$("#deleteGroup").button({icons: {primary: "ui-icon-trash"}});
	$("#deleteGroup3").button({icons: {primary: "ui-icon-trash"}});
	$("#deleteSingle").button({icons: {primary: "ui-icon-trash"}});
	$("#cancelSingle").button({icons: {primary: "ui-icon-close"}});
	$("#cancelGroup").button({icons: {primary: "ui-icon-close"}});
	$("#cancelGroup3").button({icons: {primary: "ui-icon-close"}});

});

</script>

</head>
<body>

<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">
			<div class="ui-widget">
				<div class="ui-widget-header">Edit Scheduled SMS</div>
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

							<Form name="EditScheduledSMS" id="EditScheduledSMS" style="display: inline;">								
								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Edit Single Messages</a></li>
										<li><a href="#tabs-2">Edit Group Messages</a></li>
									</ul>
									<div id="tabs-1">
										<div>
											<div class="ui-widget-header">Edit Single Messages</div>
											<br> <br>										
											
											<table id="list"><tr><td/></tr></table> 
											<div id="pager"></div>
											 											
											<div style="visibility:visible; width: 495px;" id="error">
											<div class="ui-widget message-container">
											<div class="ui-state-error ui-corner-all">
											<span class="ui-icon ui-icon-alert" style="float:left;">
											</span><span id="spnerror" class="message-text">
											<label>&nbsp;&nbsp;Please select a item to edit.</label>
											<br>
											<label>&nbsp;</label></span></div></div></div>
																						
											<button id="editSingle" type="button" >Edit</button>
											<button id="deleteSingle" type="button" >Delete</button>
											<button id="cancelSingle" type="button" >Cancel</button>
											
										</div>
									</div>
									<div id="tabs-2">
										<div>
										
											<div class="ui-widget-header">Edit Group Messages</div>
											<br><br>
											
											<table id="list2"><tr><td/></tr></table> 
											<div id="pager2"></div>
											 
											<div style="visibility:visible; width: 495px;" id="error2">
											<div class="ui-widget message-container">
											<div class="ui-state-error ui-corner-all">
											<span class="ui-icon ui-icon-alert" style="float:left;">
											</span><span id="spnerror2" class="message-text">
											<label>&nbsp;&nbsp;Please select a item to edit.</label>
											<br><label>&nbsp;</label></span></div></div></div>
																						
											<button id="editGroup" type="button" >Edit</button>
											<button id="deleteGroup" type="button" >Delete</button>
											<button id="cancelGroup" type="button">Cancel</button>
											
											<br><br>
											
											<table id="list3"><tr><td/></tr></table> 
											<div id="pager3"></div>
											
											<div style="visibility:visible; width: 495px;" id="error3">
											<div class="ui-widget message-container">
											<div class="ui-state-error ui-corner-all">
											<span class="ui-icon ui-icon-alert" style="float:left;">
											</span><span id="spnerror3" class="message-text">
											<label>&nbsp;&nbsp;Please select a item to edit.</label>
											<br><label>&nbsp;</label></span></div></div></div>
											
											<button id="editGroup3" type="button" >Edit</button>
											<button id="deleteGroup3" type="button" >Delete</button>
											<button id="cancelGroup3" type="button">Cancel</button>
										</div>
										</div>
										</div>
								</Form>
								</div>
								
							<div id="dialog-form" title="Edit Details">
							<form>
								<fieldset>
											<label>ID:</label>
											<br>
											<input type="text" name="Did" id="Did" disabled="disabled"/> 
											<br><br>
											<label>Message:</label>
											<br>
											<textarea id="Dmessage" cols="35" rows="4"></textarea>
											<br><br>
											<label>Date:</label><br>
											<input type="text" id="Ddate"/>
											<br><br>
											<label>Time:</label>
											<br>
											<input type="text" id="Dtime"/>
											
									<br> <br>
																		
								</fieldset>
							</form>
						</div>
						
						<div id="dialog-form3" title="Edit Details">
							<form>
								<fieldset>
											<label>Group name:</label>
											<br>
											<input type="text" name="Did" id="Did3" disabled="disabled"/> 
											<br><br>
											<label>Message:</label>
											<br>
											<textarea id="Dmessage3" cols="35" rows="4"></textarea>
											<br><br>
											<label>Date to be sent:</label><br>
											<input type="text" id="Ddate3"/>
											<br><br>
											<label>Time to be sent:</label>
											<br>
											<input type="text" id="Dtime3"/>
											
									<br> <br>
																		
								</fieldset>
							</form>
						</div>
												
						<div id="dialog-confirm" title="Delete SMS?">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
						</span>This message will be permanently deleted and cannot be recovered. Are you sure?</p>
						</div>
													
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>