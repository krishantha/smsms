<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="style_mainpage.css" type="text/css" /> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Main Page</title>
</head>
<body>

<div id="wrapper">
<div id="content_box">
<div class="content_resize" >
<div class="main_banner" ></div>
<form action="LogOut" method="post"> 
<input type="submit" value="logout" style="float:right;margin-right:25px;" />
</form>

<br>
<br>

<%
//To check whether the user is authenticated or not
if((String)session.getAttribute("isAuthenticated")!="true")
{
	request.getSession().setAttribute("loginError", "Please login first");
	response.sendRedirect("login.jsp");
}

//Set the options appear as a menu 
try{

String[][] options=(String[][])session.getAttribute("useroptions");

String mainoption="";

boolean flag=false;
int horizontalCount=1;

for(int i=0;i<options.length;i++)
{%>
	<% 
	
	
	if(options[i][0].compareTo(mainoption)!=0)
	{
		if(flag)
		{
		
		%>

		</div>
		<% 
		
		horizontalCount++;
		
		if(horizontalCount>2)
		{
		horizontalCount=1;
		%>
		<br>
		<br>
		<%
		}
		
		}
		
	
		
		mainoption=options[i][0];
		%>
		
		<div class="options_box">
		<div class="options_box_header"><div class="options_box_header_text">
		<%=mainoption %> </div></div>	
		<%  flag=true; 
				
	}%>
	<div class="options_box_content">
	<!-- display the sub option and the image icon  -->
	
	<a href="<%=options[i][2] %>" style="text-decoration:none;"><img src="<%=options[i][3]%>" width="30px" height="30px"><%= options[i][1] %></a></div>
	<% 
	}   

} 


	catch(Exception ex)
				{
				}
//End of options menu definition
	%>

</div>
</div>
</div>
</body>
</html>