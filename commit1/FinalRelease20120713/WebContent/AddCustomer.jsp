<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link rel="stylesheet" href="css/demos.css">
    <link rel="stylesheet" href="css/body.css"> 


	<!-- script for adding icons for buttons -->
	
	<script type="text/javascript">
	
	$(function(){ //jquery function
		
		$("#btnAddCustomer").button({icons: {primary: "ui-icon-person"}});
		$("#btnBack").button({icons: {primary: "ui-icon-circle-arrow-w"}});
	});
	
	</script>
	
	
	
	<!-- script for validating customer id while typing.
	if the currently typed customer id is available in the database,
	an error message will be shown  -->
	
	<script type="text/javascript">
	
	$(function(){ //jquery function
		
		$("#customerId").keyup(function(){ //on key up function for customer id
			
			var vCustomer=$("#customerId");
			
			vCustomer.removeClass( "ui-state-error" );
			
			var vId=$("#customerId").val();
			
			var js=$.ajax({ // ajax call
				
				type : "GET",
				url : "CustomerExists",
				data:{customerid : vId}
							
					});
					js.success(function (response, status) {
						
						if(response=="exists"){
							document.getElementById('lblcustomerExist').innerHTML ="customer id \""+vId+"\" is already in use!";
							document.getElementById('lblcustomerExist').style.visibility='visible';
							
							
						}
						else{
							
							document.getElementById('lblcustomerExist').style.visibility='hidden';
							
						}
						
						
					});

					js.error(function(request, error) {
						
				});
		
	});
		
	});
	
	</script>
	
	
	
	<!-- script for submitting customer details.
	before submitting, validated any empty required fields or invalid mobile number format
	and an error message will be shown -->
	
	<script type="text/javascript">

	function addCustomer(){
		
		valid = true;
		
		var vFirstName=$("#firstName"),
			vMiddleName=$("#middleName"),
			vLastName=$("#lastName"),
			vDateOfBirth=$("#datePicker"),
			vCustomerId=$("#customerId"),
			vAddress=$("#address"),
			vMobileNo=$("#mobileNo");
		
			
		vFirstName.removeClass( "ui-state-error");
		vLastName.removeClass( "ui-state-error");
		vDateOfBirth.removeClass( "ui-state-error");
		vCustomerId.removeClass( "ui-state-error");
		vMobileNo.removeClass( "ui-state-error");
		vMiddleName.removeClass( "ui-state-error");
		vAddress.removeClass( "ui-state-error");
		
		document.getElementById('lblMobileNoFormat').style.visibility='hidden';
		
		
		
		if (document.AddCustomerForm.txtFirstName.value == "") {
				
			vFirstName.addClass( "ui-state-error" );
						
			valid = false;
		}
		if (document.AddCustomerForm.txtLastName.value == "") {
			
			vLastName.addClass( "ui-state-error" );
						
			valid = false;
		}

		if (document.AddCustomerForm.txtCustomerId.value == "") {
			
			vCustomerId.addClass( "ui-state-error" );
			
			valid = false;
		}
		if (document.AddCustomerForm.txtDateOfBirth.value == "") {
			
			vDateOfBirth.addClass( "ui-state-error" );
			
			valid = false;
		}

		if (document.AddCustomerForm.txtMobileNo.value == "")	{

			vMobileNo.addClass( "ui-state-error" );
			valid = false;
		}
		
		if (isNaN(document.AddCustomerForm.txtMobileNo.value)) {
			
			vMobileNo.addClass( "ui-state-error" );
			document.getElementById('lblMobileNoFormat').innerHTML ="This phone number format is not recognized.Please check the number. Ex : 712123456";
			document.getElementById('lblMobileNoFormat').style.visibility='visible';
			valid = false;
		}
		
		if (document.AddCustomerForm.txtMobileNo.value.length!=9) {
			
			vMobileNo.addClass( "ui-state-error" );
			document.getElementById('lblMobileNoFormat').innerHTML ="This phone number format is not recognized.Please check the number. Ex : 712123456";
			document.getElementById('lblMobileNoFormat').style.visibility='visible';
			valid = false;
		}
		
		//if true take all the text field values in to variables and make an ajax call to AddCustomer servlet		
		if(valid==true){
			
			$(function(){ //jquery function
				
					
				var vFirstName=$("#firstName").val();
				var vMiddleName=$("#middleName").val();
				var vLastName=$("#lastName").val();
				var vDateOfBirth=$("#datePicker").val();
				var vCustomerId=$("#customerId").val();
				var vAddress=$("#address").val();
				var vMobileNo=$("#mobileNo").val();
				var vTxtCustomerId=$("#customerId");
				
				
				var jq=$.ajax({  // ajax call
					
					type: "GET",
					url: "AddCustomer",
					data: {firstname : vFirstName , middlename : vMiddleName , lastname : vLastName , customerid : vCustomerId ,address : vAddress,dateofbirth : vDateOfBirth,mobileno : vMobileNo}
				
				});
					
				
				jq.success(function (response,status){
					if(response=="Customer exists"){//show an error message if customer id is in use
						
						document.getElementById('lblcustomerExist').innerHTML ="customer id \""+vId+"\" is already in use!";
						vTxtCustomerId.addClass( "ui-state-error" );
						document.getElementById('lblcustomerExist').style.visibility='visible';
						$().toastmessage('showErrorToast',"Customer id is already in use!");
						
					}
					else{//show success message
						
						$().toastmessage('showSuccessToast', response);
						document.getElementById("addCustomerfrm").reset();
						
					}
					});
				
				jq.error(function (request,error){//show error message, if any ajax request failure
					
						$().toastmessage('showErrorToast', "Customer not added");
				});

			});
		}
				
	}
		
	</script>
	
	
	

<!-- script for connecting date picker to date of birth field -->

<script>
	$(function() { //jquery function
		$( "#datePicker" ).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',maxDate: "-10Y"});
	});
</script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>Create new customer</title>


</head>

<body>

<div id="wrapper" style="height: 100%;">
		<div id="content_box" style="width: 600px;">

			<div class="ui-widget">
				<div class="ui-widget-header">Add New Customer</div>
				
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">
							<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 

							<p><span class="ui-icon ui-icon-lightbulb" style="float: left; margin-right: .3em;"></span>

									<strong>Please fill all the required fields</strong><label style="color: red;"> *</label>
									
							
							</p>

						</div>


						<form style="margin-left: 50px;margin-top: 25px; margin-bottom: 25px;" name="AddCustomerForm" id="addCustomerfrm" action="AddCustomer" method="post"	>
						
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;First Name-</label><label style="color: red;"> *</label><br/><input type="text" size="40" id="firstName"  name="txtFirstName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/><br/><br/>
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;Middle Name-</label><br/><input type="text" size="40" id="middleName" name="txtMiddleName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/><br/><br/>
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;Last Name-</label><label style="color: red;"> *</label><br/><input type="text" size="40" id="lastName" name="txtLastName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/><br/><br/>
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;Customer ID-</label><label style="color: red;"> *</label><br/><input type="text" size="40" id="customerId" name="txtCustomerId" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/>&nbsp;<label style="visibility: hidden;color: red;" id="lblcustomerExist"></label><br/><br/>
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;Date Of Birth-</label><label style="color: red;"> *</label><br/><input type="text" id="datePicker" size="40" name="txtDateOfBirth" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/><br/><br/>
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;Address-</label><br/><input type="text" size="40" id="address" name="txtAddress" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/><br/><br/>
						<label style="color: black;font-weight: bold;">&nbsp;&nbsp;&nbsp;Mobile No- +94</label><label style="color: red;"> *</label><br/><input type="text" size="40" placeholder="Ex : 712123456" id="mobileNo" name="txtMobileNo" maxlength="9" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/>&nbsp;<br/><label style="visibility: hidden;color: red;" id="lblMobileNoFormat"></label><br/>
						
						<br/><br/>
						<button class="ui-state-default" type="button" id="btnAddCustomer" value="edit" onclick="addCustomer();">Create customer</button>
						<button class="ui-state-default" type="button" id="btnBack" value="edit" >Back</button>
						
						
						</form>

						</div>
					
					
					
				</div>
			</div>
		</div>
	</div>
	
</div>


</body>
</html>