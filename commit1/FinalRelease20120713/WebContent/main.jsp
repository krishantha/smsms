<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>SMS Management System</title>
<link rel="Stylesheet" href="css/jquery-ui-1.8.18.custom.css" type="text/css" />
<link rel="Stylesheet" href="css/style_main.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">

$(function(){
	
		var jqxhr =
		    $.ajax({
		        url: "MainPage",
		      	type: "GET",
		      
		    })
		    .success (function(response,status) { $('#contents').html(response);
		    
		    
		    $("#contents").accordion({header: "h3"});
		    
		    
		    $('a.link').click(function(){	
				
				var c= $(this).attr("href");
				$("#frame").attr("src",c);
				return false;
			});	
		    
		    
		    })
		    .error   (function(request,error)     { $("#contents").html(request)   ; }) ;
		
		$('#logout').button({
            icons: {
                primary: "ui-icon-locked"}
            });
		
		

});


</script>
</head>
<body>
<div id="wrapper">

<div id="content_box">
<div class="content_resize" >
<div class="main_banner" ></div>
<div class="ui-widget" id="mainmenu" style="margin-left:20px;">
<br>
	<div class="ui-widget-header">Options Menu</div>
	<div class="ui-widget-content" style="min-height:700px;">

	<br>
	<br>
		<div id="contents" class="inline"><!-- This is to display the options menu -->
		</div> 
		<br><br><br>
		<div id="_logout">
		<form action="LogOut" method="post" >
		<button id="logout">logout</button></form></div>
		
</div>
</div>
		<div class="inline">
		<br>
		<iframe id="frame" src="images/backx.png" frameborder="0" scrolling="yes" height="800" width="850">
		</iframe>
		</div>
		
		
		</div>
		</div>
		<div class="footer" ></div>
		</div>
		
</body>

</html>