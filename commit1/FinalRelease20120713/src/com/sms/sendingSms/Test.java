package com.sms.sendingSms;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Test
 */
@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		String temp = "<?xml version=\'1.0\' encoding=\'utf-8\'?> <invoices><rows><row><cell>Anju1</cell> <cell>data2</cell><cell>data3</cell><cell>data4</cell><cell>data5</cell><cell>data6</cell></row></rows></invoices>";
		System.out.print("TYPE :::: "+request.getParameter("type"));
		response.setContentType("text/html;charset=UTF-8");
		//response.setContentType("text/xml");
		System.out.println(temp);
		response.getWriter().write(temp);
		*/
		
		String temp = "{total: \"1\",page: \"1\",records: \"2\",rows: [{id:\"476329\",name:\"K-CY-329\",metal:144,crystal:2746,deuterium:\"0\",distance:8.2,link:\"Link\"},"+
		"{id:\"505778\",name:\"K-EB-778\",metal:270,crystal:259,deuterium:59,distance:4.1,link:\"Link\"}]}";
		
		System.out.print("TYPE :::: "+request.getParameter("type"));
		response.setContentType("text/html;charset=UTF-8");	
		System.out.println(temp);
		response.getWriter().write(temp);		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
