package com.sms.sendingSms;

public class SmsMessages {

	private int messageId;
	private String customerId;
	private String messageContent;
	private String dateCreated;
	private String timeCreated;
	private String dateToBeSent;
	private String timeToBeSent;
	private String priority;//high or low
	private String status;// pending,sent,deleted,cancel
	private String msgType;//single,group
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}
	public String getDateToBeSent() {
		return dateToBeSent;
	}
	public void setDateToBeSent(String dateToBeSent) {
		this.dateToBeSent = dateToBeSent;
	}
	public String getTimeToBeSent() {
		return timeToBeSent;
	}
	public void setTimeToBeSent(String timeToBeSent) {
		this.timeToBeSent = timeToBeSent;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
	

	
}
