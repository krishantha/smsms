package com.sms.sendingSms;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.database.DbConnect;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sms.groups.Groups;

public class SmsMessagesService {

	private DbConnect dbCon = null;

	public SmsMessagesService() {

		dbCon = new DbConnect();

	}

	public ArrayList<Customers> getSelectedGropsCustomers(String groups) {

		ArrayList<Customers> customerArray = new ArrayList<Customers>();
		ArrayList<String> groupArray = new ArrayList<String>();
		StringTokenizer groupTokens = new StringTokenizer(groups, ",", false);
		while (groupTokens.hasMoreTokens()) {
			groupArray.add(groupTokens.nextToken());
		}
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt1;

		try {
			stmt1 = (Statement) con.createStatement();
			String query = ("select * from customers where customerid in (select distinct customerid from group_customers where ");

			for (int i = 0; i < groupArray.size(); i++) {

				query = query + "groupname= '" + groupArray.get(i).toString()
						+ "'";

				if (i != (groupArray.size() - 1))
					query = query + " or ";
			}
			query = query + ")";
			System.out.println(query);
			ResultSet rs = stmt1.executeQuery(query);
			while (rs.next()) {
				Customers temp = new Customers();
				temp.setFirstName(rs.getString(1));
				temp.setMiddleName(rs.getString(2));
				temp.setLastName(rs.getString(3));
				temp.setCustomerId(rs.getString(4));
				temp.setDateOfBirth(rs.getString(5));
				temp.setAddress(rs.getString(6));
				temp.setMobileNo(rs.getString(7));

				customerArray.add(temp);
			}
		} catch (SQLException ex) {
			System.out.println("error");
		}

		return customerArray;
	}

	public boolean addMessagesByDifferentUsers(String users,
			SmsMessages message, boolean isScheduled) {

		ArrayList<String> customerArray = new ArrayList<String>();
		StringTokenizer userTokens = new StringTokenizer(users, ",", false);
		while (userTokens.hasMoreTokens()) {
			customerArray.add(userTokens.nextToken());
		}
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();

			if (isScheduled) {
				System.out.println("Scheduled SMS");
				for (int i = 0; i < customerArray.size(); i++) {

					String query = "insert into smsmessages values ('"
							+ "0' , '" + customerArray.get(i).toString()
							+ "','" + message.getMessageContent() + "','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "'," + "'low'" + ","
							+ "'sending'" + ",'single'" + ");";

					System.out.println(query);
					stmt.executeUpdate(query);

				}
			} else {

				for (int i = 0; i < customerArray.size(); i++) {

					String query = "insert into smsmessages values ('"
							+ "0' , '" + customerArray.get(i).toString()
							+ "','" + message.getMessageContent() + "','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "'," + "'high'" + ","
							+ "'sending'" + ",'single'" + ");";

					System.out.println(query);
					stmt.executeUpdate(query);

				}

				return true;

			}
		} catch (SQLException e) {
			System.out
					.println("SQL exception fired when adding a Insatnt Message");
		} finally {

			dbCon.con_close(con);
		}

		return false;

	}

	public boolean addMessagesByCustomizedGroups(String groups,
			SmsMessages message, boolean isScheduled, String customId) {

		String custom = "";
		StringTokenizer CustormizeTokens = new StringTokenizer(customId, ",",
				false);
		
		ArrayList<String> removedCustomers = new ArrayList<String>();
		
		while (CustormizeTokens.hasMoreTokens()) {
			
			removedCustomers.add(CustormizeTokens.nextToken());
			
		}
		for(int i=0;i<removedCustomers.size();i++){
			
			custom = custom + "\'" + removedCustomers.get(i).toString() + "\'";
			if (i<removedCustomers.size()-1) {
				custom = custom + ", ";
			}
			
		}
		
	
		ArrayList<String> customerArray = new ArrayList<String>();
		ArrayList<String> groupArray = new ArrayList<String>();
		StringTokenizer groupTokens = new StringTokenizer(groups, ",", false);
		while (groupTokens.hasMoreTokens()) {
			groupArray.add(groupTokens.nextToken());
		}
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt1, stmt2,stmt3,stmt4;

		try {
			stmt1 = (Statement) con.createStatement();
			String query = ("select distinct (customerid) from group_customers where ( ");

			for (int i = 0; i < groupArray.size(); i++) {

				query = query + "groupname= '" + groupArray.get(i).toString()
						+ "'";

				if (i != (groupArray.size() - 1))
					query = query + " or ";
			}

			query = query + ") and customerid not in (" + custom + ")";
			System.out.println(query);
			ResultSet rs = stmt1.executeQuery(query);
			while (rs.next()) {
				customerArray.add(rs.getString(1));
			}
		} catch (SQLException ex) {
			System.out.println("error");
		}

		try {

			stmt2 = (Statement) con.createStatement();

			if (isScheduled) {
				/*
				 * 
				int max;
				stmt3 = (Statement) con.createStatement();
				String query2 = "select max(messageid) from smsmessages";
				ResultSet rs = stmt3.executeQuery(query2);
				if(rs.next()){
					max = Integer.parseInt(rs.getString(1));
				}
				for(int i=0;)
				query2 = "insert into groupsms values("+max+","+(customerArray.size()-1)+",'gt1','no','yes')"
				*/		
				
				for (int i = 0; i < customerArray.size(); i++) {

					String query = "insert into smsmessages values ('"
							+ "0' , '" + customerArray.get(i).toString()
							+ "','" + message.getMessageContent() + "','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "'," + "'low'" + ","
							+ "'sending'" + ",'group'" + ");";

					System.out.println(query);
					stmt2.executeUpdate(query);

				}
				
				stmt3 = (Statement)con.createStatement();
				stmt4 = (Statement) con.createStatement();
				String query2 = "";
				String query3 ="";
				for(int x=0;x<groupArray.size();x++){
					query2 = "insert into groupsmsmessages values('0','"+groupArray.get(x).toString()+"','" + message.getMessageContent() +
							"','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "','low','sending');";
					System.out.println(query2);
					stmt3.executeUpdate(query2);
					
					for(int y=0;y<removedCustomers.size();y++){
						
						query3 = "insert into removedcustomersgroupsms values('"+groupArray.get(x).toString()+"','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"+removedCustomers.get(y).toString()+"')";
						System.out.println(query3);
						stmt4.executeUpdate(query3);
					}
					
					
				}
				
			} else {

				for (int i = 0; i < customerArray.size(); i++) {

					String query = "insert into smsmessages values ('"
							+ "0' , '" + customerArray.get(i).toString()
							+ "','" + message.getMessageContent() + "','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "'," + "'high'" + ","
							+ "'sending'" + ",'group'" + ");";

					System.out.println(query);

					stmt2.executeUpdate(query);

				}

				return true;

			}
		} catch (SQLException e) {
			System.out
					.println("SQL exception fired when adding a Insatnt Message");
		} finally {

			dbCon.con_close(con);
		}

		return false;
	}

	public boolean addMessagesByGroups(String groups, SmsMessages message,
			boolean isScheduled) {

		ArrayList<String> customerArray = new ArrayList<String>();
		ArrayList<String> groupArray = new ArrayList<String>();
		StringTokenizer groupTokens = new StringTokenizer(groups, ",", false);
		while (groupTokens.hasMoreTokens()) {
			groupArray.add(groupTokens.nextToken());
		}
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt1, stmt2;

		try {
			stmt1 = (Statement) con.createStatement();
			String query = ("select distinct (customerid) from group_customers where ");

			for (int i = 0; i < groupArray.size(); i++) {

				query = query + "groupname= '" + groupArray.get(i).toString()
						+ "'";

				if (i != (groupArray.size() - 1))
					query = query + " or ";
			}
			System.out.println(query);
			ResultSet rs = stmt1.executeQuery(query);
			while (rs.next()) {
				customerArray.add(rs.getString(1));
			}
		} catch (SQLException ex) {
			System.out.println("error");
		}

		try {

			stmt2 = (Statement) con.createStatement();

			if (isScheduled) {

				for (int i = 0; i < customerArray.size(); i++) {

					String query = "insert into smsmessages values ('"
							+ "0' , '" + customerArray.get(i).toString()
							+ "','" + message.getMessageContent() + "','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "'," + "'low'" + ","
							+ "'sending'" + ",'group'" + ");";

					System.out.println(query);
					stmt2.executeUpdate(query);

				}
				
				
			} else {

				for (int i = 0; i < customerArray.size(); i++) {

					String query = "insert into smsmessages values ('"
							+ "0' , '" + customerArray.get(i).toString()
							+ "','" + message.getMessageContent() + "','"
							+ message.getDateCreated() + "','"
							+ message.getTimeCreated() + "','"
							+ message.getDateToBeSent() + "','"
							+ message.getTimeToBeSent() + "'," + "'high'" + ","
							+ "'sending'" + ",'group'" + ");";

					System.out.println(query);

					stmt2.executeUpdate(query);

				}

				return true;

			}
		} catch (SQLException e) {
			System.out
					.println("SQL exception fired when adding a Insatnt Message");
		} finally {

			dbCon.con_close(con);
		}

		return false;

	}

	public ArrayList<Customers> searchCustomer(String keyword) {

		ArrayList<Customers> results = new ArrayList<Customers>();
		Connection conn = (Connection) dbCon.getConnection();
		Statement stmt;
		try {

			stmt = (Statement) conn.createStatement();
			String query = ("select distinct * from customers where (customerid like \'%"
					+ keyword
					+ "%\') or (firstname like \'%"
					+ keyword
					+ "%\') or (middlename like \'%"
					+ keyword
					+ "%\') or (lastname like \'%" + keyword + "%\')");
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				Customers tempCustomer = new Customers();
				tempCustomer.setFirstName(rs.getString(1));
				tempCustomer.setMiddleName(rs.getString(2));
				tempCustomer.setLastName(rs.getString(3));
				tempCustomer.setCustomerId(rs.getString(4));
				tempCustomer.setDateOfBirth(rs.getString(5));
				tempCustomer.setAddress(rs.getString(6));
				tempCustomer.setMobileNo(rs.getString(7));

				results.add(tempCustomer);
			}

		} catch (SQLException ex) {
			System.out.println("Error in searchCustomer");
		}
		return results;

	}

	public ArrayList<Customers> getAllCustomers() {
		ArrayList<Customers> result = new ArrayList<Customers>();
		Connection conn = (Connection) dbCon.getConnection();
		Statement stmt;
		try {

			stmt = (Statement) conn.createStatement();
			String query = ("select distinct * from customers");
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				Customers tempCustomer = new Customers();
				tempCustomer.setFirstName(rs.getString(1));
				tempCustomer.setMiddleName(rs.getString(2));
				tempCustomer.setLastName(rs.getString(3));
				tempCustomer.setCustomerId(rs.getString(4));
				tempCustomer.setDateOfBirth(rs.getString(5));
				tempCustomer.setAddress(rs.getString(6));
				tempCustomer.setMobileNo(rs.getString(7));

				result.add(tempCustomer);
			}

		} catch (SQLException ex) {
			System.out.println("Error in searchCustomer");
		}

		return result;
	}

	public ArrayList<Groups> getAllGroups() {

		ArrayList<Groups> result = new ArrayList<Groups>();
		Connection conn = (Connection) dbCon.getConnection();
		Statement stmt;
		try {

			stmt = (Statement) conn.createStatement();
			String query = ("select distinct * from groups");
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				Groups tempGroup = new Groups();
				tempGroup.setGroupName(rs.getString(1));
				tempGroup.setDescription(rs.getString(2));
				result.add(tempGroup);
			}

		} catch (SQLException ex) {
			System.out.println("Error in searchCustomer");
		}

		return result;
	}

	public ArrayList<SmsMessages> getAllSingleScheduledSms() {

		ArrayList<SmsMessages> result = new ArrayList<SmsMessages>();
		Connection conn = (Connection) dbCon.getConnection();
		Statement stmt;
		try {

			stmt = (Statement) conn.createStatement();
			String query = ("select distinct * from smsmessages where priority=\'low\' and msgtype=\'single\'");
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				SmsMessages temp = new SmsMessages();
				temp.setMessageId(rs.getShort(1));
				temp.setCustomerId(rs.getString(2));
				temp.setMessageContent(rs.getString(3));
				temp.setDateToBeSent(rs.getString(6));
				temp.setTimeToBeSent(rs.getString(7));
				temp.setStatus(rs.getString(9));
				result.add(temp);
			}

		} catch (SQLException ex) {
			System.out.println("Error in searchCustomer");
		}

		return result;

	}

	public ArrayList<SmsMessages> getAllGroupScheduledSms() {

		ArrayList<SmsMessages> result = new ArrayList<SmsMessages>();
		Connection conn = (Connection) dbCon.getConnection();
		Statement stmt;
		try {

			stmt = (Statement) conn.createStatement();
			String query = ("select distinct * from smsmessages where priority=\'low\' and msgtype=\'group\'");
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				SmsMessages temp = new SmsMessages();
				temp.setMessageId(Integer.parseInt(rs.getString(1)));
				temp.setCustomerId(rs.getString(2));
				temp.setMessageContent(rs.getString(3));
				temp.setDateToBeSent(rs.getString(6));
				temp.setTimeToBeSent(rs.getString(7));
				temp.setStatus(rs.getString(9));
				result.add(temp);
			}

		} catch (SQLException ex) {
			System.out.println("Error in searchCustomer");
		}

		return result;

	}

	public ArrayList<GroupSmsMessage> getGroupwiseScheduledSms(){
		
		ArrayList<GroupSmsMessage> result = new ArrayList<GroupSmsMessage>();
		Connection conn = (Connection) dbCon.getConnection();
		Statement stmt;
		try {

			stmt = (Statement) conn.createStatement();
			String query = ("select distinct * from groupsmsmessages where priority=\'low\'");
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				
				GroupSmsMessage temp = new GroupSmsMessage();
				temp.setMessageId(Integer.parseInt(rs.getString(1)));
				temp.setGroupName(rs.getString(2));
				temp.setMessageContent(rs.getString(3));
				temp.setDateCreated(rs.getString(4));
				temp.setTimeCreated(rs.getString(5));
				temp.setDateToBeSent(rs.getString(6));
				temp.setTimeToBeSent(rs.getString(7));
				temp.setStatus(rs.getString(9));
				result.add(temp);
				
			}

		} catch (SQLException ex) {
			System.out.println("Error in searchCustomer");
		}

		return result;
		
	}
	
 	public void editSingleScheduledSms(String messageId, String customerId,
			String messageContent, String dateToBeSent, String timeToBeSent) {

		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			
			String query = "update smsmessages set customerid=\'"+customerId+"\', messagecontent=\'"+messageContent+"\', datetobesent='"+
			dateToBeSent+"', timetobesent='"+timeToBeSent+"' where messageid="+messageId;

			System.out.println(query);
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			System.out
					.println("SQL exception fired when adding a Insatnt Message");
		} finally {

			dbCon.con_close(con);
		}
	}
 	
	public void editGroupScheduledSms(String messageId, String group,
			String messageContent, String dateToBeSent, String timeToBeSent,String createdDate,String createdTime) {

		Connection con = (Connection) dbCon.getConnection();
		Statement stmt,stmt2;

		try {

			stmt = (Statement) con.createStatement();
			stmt2 = (Statement) con.createStatement();
			
			String query = "update smsmessages set messagecontent=\'"+messageContent+"\', datetobesent='"+
			dateToBeSent+"', timetobesent='"+timeToBeSent+"' where datecreated='"+createdDate+"' and timecreated='"+createdTime+"' and msgtype='group' and priority = 'low'";
			
			System.out.println(query);
			stmt.executeUpdate(query);
			
			String query2 = "update groupsmsmessages set messagecontent=\'"+messageContent+"\', datetobesent='"+
			dateToBeSent+"', timetobesent='"+timeToBeSent+"' where messageid = "+messageId;
			System.err.println(query2);
			stmt2.executeUpdate(query2);
			
			

		} catch (SQLException e) {
			System.out
					.println("SQL ERROR ");
		} finally {

			dbCon.con_close(con);
		}
	}
 	
	
	public void deleteSingleScheduledSms(String messageId) {

		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			
			String query = "delete from smsmessages where messageid="+messageId;

			System.out.println(query);
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			System.out
					.println("SQL exception fired when adding a Insatnt Message");
		} finally {

			dbCon.con_close(con);
		}
	}
	
	public void deleteGroupScheduledSms(String messageId) {

		Connection con = (Connection) dbCon.getConnection();
		Statement stmt1,stmt2,stmt3;

		try {

			stmt1 = (Statement) con.createStatement();
			stmt2 = (Statement) con.createStatement();
			stmt3 = (Statement) con.createStatement();
			
			String query1 = "select * from groupsmsmessages where messageid = " + messageId;
			
			ResultSet rs = stmt1.executeQuery(query1);
			System.out.println(query1);
			
			GroupSmsMessage msg = new GroupSmsMessage();
			if(rs.next()){
				msg.setMessageId(Integer.parseInt(rs.getString(1)));
				msg.setGroupName(rs.getString(2));
				msg.setMessageContent(rs.getString(3));
				msg.setDateCreated(rs.getString(4));
				msg.setTimeCreated(rs.getString(5));
				msg.setDateToBeSent(rs.getString(6));
				msg.setTimeToBeSent(rs.getString(7));
				msg.setPriority(rs.getString(8));
				msg.setStatus(rs.getString(9));
			}
			
			String query2 = "delete from smsmessages where datecreated = '"+msg.getDateCreated()+"' and timecreated = '"+msg.getTimeCreated()+"' and msgtype = 'group' and "+
			"priority = 'low'";
			
			System.out.println(query2);
			stmt2.executeUpdate(query2);
			
			String query = "delete from groupsmsmessages where messageid="+messageId;

			System.out.println(query);
			stmt3.executeUpdate(query);

		} catch (SQLException e) {
			System.out
					.println("SQL exception fired when adding a Insatnt Message");
		} finally {

			dbCon.con_close(con);
		}
	}

}
