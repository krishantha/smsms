package com.sms.sendingSms;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sms.userRegistration.UserService;


/**
 * Servlet implementation class ScheduledClass
 */
@WebServlet("/ScheduledClass")
public class ScheduledClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScheduledClass() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Do post called");
		boolean result = false;
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yy-MM-dd");
		Calendar calender = new GregorianCalendar();
		String systemTime = String.valueOf(calender.get(Calendar.HOUR)) + ":"
				+ String.valueOf(calender.get(Calendar.MINUTE)) + ":"+String.valueOf(calender.get(Calendar.SECOND));
		
		
		String scheduledTime =  request.getParameter("sTime");
		String scheduledDate = request.getParameter("sDate");
		String message = request.getParameter("msg");					   
		String users = request.getParameter("user");
		String type = request.getParameter("type");
		
		
		SmsMessagesService smsDao = new SmsMessagesService();
		SmsMessages smsDetails = new SmsMessages();
		smsDetails.setDateCreated(dateFormatter.format(new Date()));
		smsDetails.setDateToBeSent(scheduledDate);
		smsDetails.setMessageContent(message);
		smsDetails.setTimeCreated(systemTime);
		smsDetails.setTimeToBeSent(scheduledTime);			
		
		
		
		System.out.println(":::"+message+scheduledDate+scheduledTime+type+users);
		
		
		if(type.equals("single")){
			
			System.out.println("Single Message executing");				
			result = smsDao.addMessagesByDifferentUsers(users, smsDetails,true);
			
			
		}
		else if (type.equals("group")){
			
			result = smsDao.addMessagesByGroups(users, smsDetails, true);
			
		}else if(type.equals("getSingleScheduledMsg")){
			int totalpage=1;
			ArrayList<SmsMessages> msg = smsDao.getAllSingleScheduledSms();
			
			if(msg.size()>10){
				totalpage = (msg.size()/10);
			}
			
			System.out.println("Total Pa: "+totalpage+"\npages: "+(msg.size()-1));
			String resultData = "{total: \""+totalpage+"\",page: \"1\",records: \""+(msg.size()-1)+"\",rows: [";
			
			for(int i=0;i<msg.size();i++){
				//{ID:"", Message:"", Date,Time,status}
				resultData = resultData +
						"{count:\""+msg.get(i).getMessageId()+"\", id:\""+msg.get(i).getCustomerId()+"\", "+
						"message: \""+msg.get(i).getMessageContent()+"\", "+
						"date: \""+msg.get(i).getDateToBeSent()+"\", "+
						"time: \""+msg.get(i).getTimeToBeSent()+"\", "+
						"status: \""+msg.get(i).getStatus()+"\"}";
				System.out.println(resultData);
				
				if(i!=(msg.size()-1)){
					resultData = resultData + ",";
				}
			}
			
			resultData = resultData + "]}";
			System.out.println(resultData);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(resultData);
			
		}else if(type.equals("getGroupScheduledMsg")){
			int totalpage=1;
			ArrayList<SmsMessages> msg = smsDao.getAllGroupScheduledSms();
			if(msg.size()>10){
				totalpage = (msg.size()/10);
			}
			System.out.println("Total Pa: "+totalpage+"\npages: "+(msg.size()-1));
			String resultData = "{total: \""+totalpage+"\",page: \"1\",records: \""+(msg.size()-1)+"\",rows: [";
			for(int i=0;i<msg.size();i++){
				//{ID:"", Message:"", Date,Time,status}
				resultData = resultData +
						"{\"count\":\""+msg.get(i).getMessageId()+"\", \"id\":\""+msg.get(i).getCustomerId()+"\", "+
						"\"message\": \""+msg.get(i).getMessageContent()+"\", "+
						"\"date\": \""+msg.get(i).getDateToBeSent()+"\", "+
						"\"time\": \""+msg.get(i).getTimeToBeSent()+"\", "+
						"\"status\": \""+msg.get(i).getStatus()+"\"}";
				System.out.println(resultData);
				
				if(i!=(msg.size()-1)){
					resultData = resultData + ",";
				}
			}
			
			resultData = resultData + "]}";
			System.out.println(resultData);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(resultData);
			
		}
		else if(type.equals("getGroupWiseScheduledMsg")){
			
			int totalpage=1;
			ArrayList<GroupSmsMessage> msg = smsDao.getGroupwiseScheduledSms();
			if(msg.size()>10){
				totalpage = (msg.size()/10);
			}
			System.out.println("Total Pa: "+totalpage+"\npages: "+(msg.size()-1));
			String resultData = "{total: \""+totalpage+"\",page: \"1\",records: \""+(msg.size()-1)+"\",rows: [";
			for(int i=0;i<msg.size();i++){
				//{ID:"", Message:"", Date,Time,status}
				resultData = resultData +
						"{\"count\":\""+msg.get(i).getMessageId()+"\", \"group\":\""+msg.get(i).getGroupName()+"\", "+
						"\"message\": \""+msg.get(i).getMessageContent()+"\", "+
						"\"date\": \""+msg.get(i).getDateToBeSent()+"\", "+
						"\"time\": \""+msg.get(i).getTimeToBeSent()+"\", "+
						"\"cdate\": \""+msg.get(i).getDateCreated()+"\", "+
						"\"ctime\": \""+msg.get(i).getTimeCreated()+"\", "+
						"\"status\": \""+msg.get(i).getStatus()+"\"}";
				System.out.println(resultData);
				
				if(i!=(msg.size()-1)){
					resultData = resultData + ",";
				}
			}
			
			resultData = resultData + "]}";
			System.out.println(resultData);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(resultData);
			
		}
		else if(request.getParameter("type").equals("groupCustermized")){
			
			String groups = request.getParameter("user");
			String customId = request.getParameter("custom");
			System.out.println(request.getParameter("user")+"AND"+customId);
			smsDao.addMessagesByCustomizedGroups(groups, smsDetails, true, customId);
			
		} else if (request.getParameter("type").equals("groupCustormizeTable")) {
			// String result = "";
			
			ArrayList<Customers> customers = smsDao
					.getSelectedGropsCustomers(users);
			String table = "<table id=\"users\" class=\"ui-widget ui-widget-content\" "
					+ "align=\"center\" cellpadding=\"2\" cellspacing=\"10\"> "
					+ "<thead> <tr class=\"ui-widget-header\"> <th align=\"center\" ></th>"
					+ "<th align=\"center\">Customer ID</th><th align=\"center\">Name</th><th align=\"center\">Number</th>"
					+ "</tr></thead><tbody>";
			if (customers.size() == 0) {

				table = "No results found. Please try different word";

			} else {
				for (int i = 0; i < customers.size(); i++) {
					System.out.println("Runing For loop : " + i);
					String contect = "<tr><td><input type=\"checkbox\" value=\""
							+ customers.get(i).getCustomerId()
							+ "\"/></td><td>"
							+ customers.get(i).getCustomerId()
							+ "</td>"
							+ "<td>"
							+ customers.get(i).getFirstName()
							+ " "
							+ customers.get(i).getLastName()
							+ "</td>"
							+ "<td>"
							+ customers.get(i).getMobileNo() + "</td></tr>";
					table = table + contect;

				}

				table = table + " </tbody> </table>";

				response.setContentType("text/html;charset=UTF-8");
				response.getWriter().write(table);
			}
		}else if(request.getParameter("type").equals("editScheduledMsg")){
			
			String countEdit = request.getParameter("count");
			String idEdit = request.getParameter("id");
			String messageEdit = request.getParameter("message");
			String dateEdit = request.getParameter("date");
			String timeEdit = request.getParameter("time");
			
			smsDao.editSingleScheduledSms(countEdit, idEdit, messageEdit, dateEdit, timeEdit);
			
		}
else if(request.getParameter("type").equals("editGroupScheduledMsg")){
			
			String countEdit = request.getParameter("count");
			String group = request.getParameter("group");
			String messageEdit = request.getParameter("message");
			String dateEdit = request.getParameter("date");
			String timeEdit = request.getParameter("time");
			String createdDate = request.getParameter("cdate");
			String createdTime = request.getParameter("ctime");
			
			smsDao.editGroupScheduledSms(countEdit, group, messageEdit, dateEdit, timeEdit, createdDate, createdTime);
			
		}
else if(request.getParameter("type").equals("deleteScheduledMsg")){
			
			String countEdit = request.getParameter("count");
			
			smsDao.deleteSingleScheduledSms(countEdit);
			
		}
else if(request.getParameter("type").equals("deleteGroupScheduledMsg")){
	
	String countEdit = request.getParameter("count");
	
	smsDao.deleteGroupScheduledSms(countEdit);
	
}
		
		
		
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);
		
		
	}
    
}
