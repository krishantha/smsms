package com.sms.sendingSms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.font.EAttribute;
import com.google.gson.Gson;

/**
 * Servlet implementation class SearchingClass
 */
@WebServlet("/SearchingClass")
public class SearchingClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchingClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String keyword = request.getParameter("key");
		String type = request.getParameter("type");
		SmsMessagesService search = new SmsMessagesService();
		ArrayList<Customers> result = new ArrayList<Customers>();
		System.out.println("Finally Ajax awsome ::: "+keyword);
		String table = "";
		
		if(type.equals("single")){
		
		result = search.searchCustomer(keyword);
		 table = "<table id=\"users\" class=\"ui-widget ui-widget-content\" "+
					   "align=\"center\" cellpadding=\"2\" cellspacing=\"10\"> "+
					   "<thead> <tr class=\"ui-widget-header\"> <th align=\"center\" ></th>"+
					   "<th align=\"center\">Customer ID</th><th align=\"center\">Name</th><th align=\"center\">Number</th>"+
					   "</tr></thead><tbody>";
		
		if(result.size() == 0){
			
			table = "No results found. Please try different word";
			
		}else{
		for(int i=0;i<result.size();i++){
			System.out.println("Runing For loop : "+i);
			String contect = "<tr><td><input type=\"checkbox\" value=\""+result.get(i).getCustomerId()+"\"/></td><td>"+result.get(i).getCustomerId()+"</td>"+
					"<td>"+result.get(i).getFirstName()+" "+result.get(i).getLastName()+"</td>"+"<td>"+result.get(i).getMobileNo()+"</td></tr>";
			table = table + contect;
			
		}
		
		table = table + " </tbody> </table>" ;
		System.out.println("Writing Data........\n");
		System.out.println(table);
		}
		}
		else if(type.equals("group")){
			//result = search.;
			 table = "<table id=\"users\" class=\"ui-widget ui-widget-content\" "+
						   "align=\"center\" cellpadding=\"2\" cellspacing=\"10\"> "+
						   "<thead> <tr class=\"ui-widget-header\"> <th align=\"center\" ></th>"+
						   "<th align=\"center\">Group Name</th><th align=\"center\">Description</th>"+
						   "</tr></thead><tbody>";
			
			if(result.size() == 0){
				
				table = "No results found. Please try different word";
				
			}else{
			for(int i=0;i<result.size();i++){
				System.out.println("Runing For loop : "+i);
				String contect = "<tr><td><input type=\"checkbox\" /></td><td>"+result.get(i).getCustomerId()+"</td>"+
						"<td>"+result.get(i).getFirstName()+" "+result.get(i).getLastName()+"</td>"+"<td>"+result.get(i).getMobileNo()+"</td></tr>";
				table = table + contect;
				
			}
			
			table = table + " </tbody> </table>" ;
			System.out.println("Writing Data........\n");
			System.out.println(table);
		}}
		//response.getWriter().write(table);
		//***********************************************************************************
		//Map<String, Object> data = new HashMap<String, Object>();
	    
	    //data.put("messageRes", table);
	   // data.put("param", request.getParameter("foo"));
	     
	    // Write response data as JSON.
	   // response.setContentType("application/json");
	   // response.setCharacterEncoding("UTF-8");
	   // response.getWriter().write(new Gson().toJson(data));
		
		//response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		//response.setHeader("Pragma", "no-cache");
		response.setContentType("text/html;charset=UTF-8");
	    response.getWriter().write(table);
	    
		return;
		//when i passing string with html tags from servlet to jsp it's not working
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		doGet(request,response);
		
		
	}

}
