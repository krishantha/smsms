package com.sms.sendingSms;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sms.groups.Groups;

/**
 * Servlet implementation class InstantClass
 */
@WebServlet("/InstantClass")
public class InstantClass extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InstantClass() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		ArrayList<String> removingCustomers = new ArrayList<String>();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yy-MM-dd");
		Calendar calender = new GregorianCalendar();
		String systemTime = String.valueOf(calender.get(Calendar.HOUR)) + ":"
				+ String.valueOf(calender.get(Calendar.MINUTE)) + ":00";
		String scheduledTime = systemTime;

		SmsMessagesService smsDao = new SmsMessagesService();

		SmsMessages smsDetails = new SmsMessages();
		smsDetails.setDateCreated(dateFormatter.format(new Date()));
		smsDetails.setDateToBeSent(dateFormatter.format(new Date()));
		smsDetails.setMessageContent(request.getParameter("msg"));
		smsDetails.setTimeCreated(systemTime);
		smsDetails.setTimeToBeSent(scheduledTime);

		if (request.getParameter("type").equals("single")) {

			System.out.println("Single Message executing");
			String users = request.getParameter("user");
			System.out.println(request.getParameter("user"));
			smsDao.addMessagesByDifferentUsers(users, smsDetails, false);

		} else if (request.getParameter("type").equals("group")) {

			String groups = request.getParameter("user");
			System.out.println(request.getParameter("user"));
			smsDao.addMessagesByGroups(groups, smsDetails, false);

		} else if (request.getParameter("type").equals("allcustomers")) {
			ArrayList<Customers> allCustomers = smsDao.getAllCustomers();
			String result = "";
			for (int i = 0; i < allCustomers.size(); i++) {
				result = result + allCustomers.get(i).getCustomerId() + ",";
			}
			result = result.substring(0, result.length() - 1);
			System.out.println(result);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
		} else if (request.getParameter("type").equals("allgroups")) {
			ArrayList<Groups> allGroups = smsDao.getAllGroups();
			String result = "";
			for (int i = 0; i < allGroups.size(); i++) {
				result = result + allGroups.get(i).getGroupName() + ",";
			}
			result = result.substring(0, result.length() - 1);
			System.out.println(result);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
		} else if (request.getParameter("type").equals("groupCustormize")) {
			// String result = "";
			String users = request.getParameter("user");
			ArrayList<Customers> customers = smsDao
					.getSelectedGropsCustomers(users);
			String table = "<table id=\"users\" class=\"ui-widget ui-widget-content\" "
					+ "align=\"center\" cellpadding=\"2\" cellspacing=\"10\"> "
					+ "<thead> <tr class=\"ui-widget-header\"> <th align=\"center\" ></th>"
					+ "<th align=\"center\">Customer ID</th><th align=\"center\">Name</th><th align=\"center\">Number</th>"
					+ "</tr></thead><tbody>";
			if (customers.size() == 0) {

				table = "No results found. Please try different word";

			} else {
				for (int i = 0; i < customers.size(); i++) {
					System.out.println("Runing For loop : " + i);
					String contect = "<tr><td><input type=\"checkbox\" value=\""
							+ customers.get(i).getCustomerId()
							+ "\"/></td><td>"
							+ customers.get(i).getCustomerId()
							+ "</td>"
							+ "<td>"
							+ customers.get(i).getFirstName()
							+ " "
							+ customers.get(i).getLastName()
							+ "</td>"
							+ "<td>"
							+ customers.get(i).getMobileNo() + "</td></tr>";
					table = table + contect;

				}

				table = table + " </tbody> </table>";

				response.setContentType("text/html;charset=UTF-8");
				response.getWriter().write(table);
			}
		} else if (request.getParameter("type").equals("customerIdCheck")) {

			String result = "null";
			ArrayList<Customers> allCustomers = smsDao.getAllCustomers();
			String groups = request.getParameter("msg");
			ArrayList<String> groupArray = new ArrayList<String>();
			StringTokenizer groupTokens = new StringTokenizer(groups, ",",
					false);
			while (groupTokens.hasMoreTokens()) {
				groupArray.add(groupTokens.nextToken());
			}
			for (int i = 0; i < groupArray.size(); i++) {
				Inner:for (int x = 0; x < allCustomers.size(); x++) {
					
					System.out.println(groupArray.get(i).toString() + "::" + allCustomers.get(x).getCustomerId());
					
					if (groupArray.get(i).toString()
							.equals(allCustomers.get(x).getCustomerId())) {						
						result = "true";
						break Inner;
					} else {
						result = "false";
					}
					
				}
			}

			
			System.out.println("::" + result);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);

		}else if (request.getParameter("type").equals("groupIdCheck")) {

			String result = "null";
			ArrayList<Groups> allGroups = smsDao.getAllGroups();
			String groups = request.getParameter("msg");
			ArrayList<String> groupArray = new ArrayList<String>();
			StringTokenizer groupTokens = new StringTokenizer(groups, ",",
					false);
			while (groupTokens.hasMoreTokens()) {
				groupArray.add(groupTokens.nextToken());
			}
			for (int i = 0; i < groupArray.size(); i++) {
				Inner:for (int x = 0; x < allGroups.size(); x++) {
					
					System.out.println(groupArray.get(i).toString() + "::" + allGroups.get(x).getGroupName());
					
					if (groupArray.get(i).toString()
							.equals(allGroups.get(x).getGroupName())) {						
						result = "true";
						break Inner;
					} else {
						result = "false";
					}
					
				}
			}

			
			System.out.println("::" + result);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);

		}else if(request.getParameter("type").equals("groupCustermized")){
			
			String groups = request.getParameter("user");
			String customId = request.getParameter("custom");
			System.out.println(request.getParameter("user")+"AND"+customId);
			smsDao.addMessagesByCustomizedGroups(groups, smsDetails, false, customId);
			
		}

		// RequestDispatcher rd=request.getRequestDispatcher("InstantSms.jsp");
		// rd.forward(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		doGet(request, response);

	}

}
