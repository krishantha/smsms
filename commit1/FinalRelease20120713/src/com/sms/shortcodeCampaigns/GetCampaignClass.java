package com.sms.shortcodeCampaigns;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetCampaignClass
 */
@WebServlet("/GetCampaignClass")
public class GetCampaignClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCampaignClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String scode=request.getParameter("shortcode");
		CampaignDetails cd=new CampaignDetails();
		ShortcodeCampaignService srvce=new ShortcodeCampaignService();
		try{
		cd=srvce.getCampaign(scode);
		
		String reply=cd.campaignname+"@:"+cd.description+"@:"+cd.shortcode+"@:"+cd.startdate+"@:"+cd.enddate+"@:"+cd.reply;
		
		response.getWriter().write(reply);
		}
		catch(Exception ex){
			
			response.getWriter().write("codeNotExist");
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
