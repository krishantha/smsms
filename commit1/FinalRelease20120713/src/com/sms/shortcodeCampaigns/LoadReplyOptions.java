package com.sms.shortcodeCampaigns;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoadReplyOptions
 */
@WebServlet("/LoadReplyOptions")
public class LoadReplyOptions extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadReplyOptions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<ReplyOptions> rep=new ArrayList<ReplyOptions>();
		
		ShortcodeCampaignService ser=new ShortcodeCampaignService();
		
		rep=ser.getReplyOptionsList();
		
		String table;
		
		String type=request.getParameter("type");
		
		if(type.equalsIgnoreCase("add")){
			
		table = "<table width=\"300px\" id=\"RepOp\" "+
					   "align=\"center\" cellpadding=\"5\" cellspacing=\"2\"> "+
					   "<thead> <tr class=\"ui-widget-header\" height=\"30\"> <th align=\"center\" ><input type=\"checkbox\" id=\"addall\" name=\"checkadd\" onclick=\"chkall(this.name);\"></th>"+
					   "<th align=\"center\">Reply option</th></tr></thead><tbody>";
		
		try{
			
			for(int i=0;i<rep.size();i++){
				
				String content = "<tr class=\"ui-widget ui-widget-content\"><td align=\"center\"><input type=\"checkbox\" name=\"repop\" id=\""+rep.get(i).getReplyoption()+"\" value=\""+rep.get(i).getReplyoption()+"\"></td><td>"+rep.get(i).getReplyoption()+"</td></tr>";
				table = table + content;
				
			}
			table = table + " </tbody> </table>" ;
			
			System.out.println(table);
						
		}
		catch(Exception ex){
			ex.printStackTrace();
			response.getWriter().write("notExist");
		}
		
		}
		else{
			
		table = "<table width=\"300px\" id=\"RepOp\" "+
					   "align=\"center\" cellpadding=\"5\" cellspacing=\"2\"> "+
					   "<thead> <tr class=\"ui-widget-header\" height=\"30\"> <th align=\"center\" ><input type=\"checkbox\" id=\"editall\" name=\"checkedit\" onclick=\"chkall(this.name);\"></th>"+
					   "<th align=\"center\">Reply option</th></tr></thead><tbody>";
			try{
				
				for(int i=0;i<rep.size();i++){
					
					String content = "<tr class=\"ui-widget ui-widget-content\"><td align=\"center\"><input type=\"checkbox\" name=\"repopx\" id=\"x"+rep.get(i).getReplyoption()+"\" value=\""+rep.get(i).getReplyoption()+"\"></td><td>"+rep.get(i).getReplyoption()+"</td></tr>";
					table = table + content;
					
				}
				table = table + " </tbody> </table>" ;
				
				System.out.println(table);
							
			}
			catch(Exception ex){
				ex.printStackTrace();
				response.getWriter().write("notExist");
			}
			
			
			
		}
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(table);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
