package com.sms.shortcodeCampaigns;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteCampaign
 */
@WebServlet("/DeleteCampaign")
public class DeleteCampaign extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCampaign() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String campaign=request.getParameter("camp");
		
		ArrayList<CampaignDetails> cmp=new ArrayList<CampaignDetails>();
		ShortcodeCampaignService service=new ShortcodeCampaignService();
		
		
		service.removeCampaign(campaign);
		
		
		cmp=service.getCampaignlist();
		
		
		
		String table = "<table width=\"540px\" id=\"campaigns\" "+
				   "align=\"center\" cellpadding=\"2\" cellspacing=\"2\"> "+
				   "<thead> <tr class=\"ui-widget-header\" height=\"30\"> <th align=\"center\" >Select</th>"+
				   "<th align=\"center\">Campaign name</th><th align=\"center\">Short code</th><th align=\"center\">Start date</th><th align=\"center\">End date</th><th align=\"center\">Status</th></tr></thead><tbody>";
		try{
			
			for(int i=0;i<cmp.size();i++){
				
				String content = "<tr class=\"ui-widget ui-widget-content\"><td align=\"center\"><input type=\"radio\" name=\"radio\" onclick=\"chkstatus(this.value);\" value="+cmp.get(i).getShortcode()+"></td><td>"+cmp.get(i).getCampaignname()+"</td><td>"+cmp.get(i).getShortcode()+"</td>"+
			                      "<td>"+cmp.get(i).getStartdate()+"</td><td>"+cmp.get(i).getEnddate()+"</td><td>"+cmp.get(i).getStatus()+"</td></tr>";
				table = table + content;
				
			}
			table = table + " </tbody> </table>" ;
			
			System.out.println(table);
						
		}
		catch(Exception ex){
			ex.printStackTrace();
			response.getWriter().write("notExist");
		}
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(table);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
