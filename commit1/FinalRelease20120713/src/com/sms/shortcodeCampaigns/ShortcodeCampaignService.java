package com.sms.shortcodeCampaigns;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.sms.database.DbConnect;

public class ShortcodeCampaignService {

	DbConnect conn;
	
	public ShortcodeCampaignService(){
		
		conn=new DbConnect();
		
	}
	
public boolean addCampaign(String cmpname, String description,String shortcode,String startdate,String enddate,String reply) {
		
		
		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res = false;
		try {
			
			Statement s = con.createStatement();
			String queryString = "insert into campaigns (cmpname,cmpdesc,shrtcode,startdate,enddate,reply) values('" + cmpname + "','" + description + "','" + shortcode + "','" + startdate + "','" + enddate + "','" + reply + "');";
			System.out.println(queryString);
			int val = s.executeUpdate(queryString);

			if (val == 1) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return res;
		}
		finally{
			conn.conClose(con);
			
		}

		return res;
	}


public boolean updateCampaign(String cmpname, String description,String shortcode,String startdate,String enddate,String reply) {
	
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res = false;
	try {
		
		Statement s = con.createStatement();
		String queryString = "update campaigns set cmpname='" + cmpname + "',cmpdesc='" + description + "',startdate='" + startdate + "',enddate='" + enddate + "',reply='" + reply + "' where shrtcode='" + shortcode + "';";
		System.out.println(queryString);
		int val = s.executeUpdate(queryString);

		if (val == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		return res;
	}
	finally{
		conn.conClose(con);
		
	}

	return res;
}


public boolean ShortcodeExist(String shortcode) {

	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res = false;
	try {
		
		Statement s = con.createStatement();
		String queryString = "select * from campaigns where shrtcode='"+ shortcode + "'";
		System.out.println(queryString);
		s.executeQuery(queryString);
		ResultSet rs = s.getResultSet();

		if (rs.next()) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		return res;
	}
	finally{
				
			conn.conClose(con);
	}

	return res;
}


public ArrayList<CampaignDetails> getCampaignlist(){
	
	ArrayList<CampaignDetails> Cmparray=new ArrayList<CampaignDetails>();
	Connection con = (Connection)DbConnect.getConnection();// get connection
	Statement s;
	ResultSet rst=null;
	try
	{
	
	s=con.createStatement();
	String queryString="select * from campaigns;";
	rst=s.executeQuery(queryString);
	while(rst.next()){
		
		CampaignDetails g=new CampaignDetails();
		g.setCampaignname(rst.getString(1));
		g.setDescription(rst.getString(2));
		g.setShortcode(rst.getString(3));
		g.setStartdate(rst.getString(4));
		g.setEnddate(rst.getString(5));
		g.setReply(rst.getString(6));
		g.setStatus(rst.getString(7));
			
		Cmparray.add(g);
	}
	
	}
	catch(Exception e){
		System.out.println("can't get groups");
		e.printStackTrace();
		return null;
	}
	finally{
		
		conn.conClose(con);
	}
	
return Cmparray;
}

public String getInfo(){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	String inact="";
	String act="";
	String result="";
	ResultSet rs;
	try {
		
		Statement s = con.createStatement();
		String queryString1 = "select count(*) from campaigns where status='inactive';";
		rs=s.executeQuery(queryString1);
		if(rs.next()){
			
			inact=rs.getString(1);
			
		}
		String queryString2 = "select count(*) from campaigns where status='active';";
		rs=s.executeQuery(queryString2);
		if(rs.next()){
			
			act=rs.getString(1);
			
		}
		
		result=act+":"+inact;
		System.out.println(result);
		
	}
	catch(Exception ex){
		
		
	}
	finally{
		
		conn.conClose(con);
	}
	return result;
		
}


public boolean removeCampaign(String cmp){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res=false;
	
	try {
		
		Statement s = con.createStatement();
		String queryString1 = "delete from campaigns where shrtcode='"+cmp+"';";
		System.out.println(queryString1);
		int val1 = s.executeUpdate(queryString1);

		if (val1 == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		System.out.println("can't delete campaign");
		ex.printStackTrace();
		return res;
	}
	finally{
		
		conn.conClose(con);
	}
	return res;
	
}

public String getStatus(String code){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	String stat="";
	ResultSet rs;
	try {
		
		Statement s = con.createStatement();
		String queryString1 = "select status from campaigns where shrtcode='"+code+"';";
		rs=s.executeQuery(queryString1);
		if(rs.next()){
			
			stat=rs.getString(1);
			
		}
				
		
		System.out.println(stat);
		
	}
	catch(Exception ex){
		
		
	}
	finally{
		
		conn.conClose(con);
	}
	return stat;
		
}

public boolean deactivateCampaign(String cmp){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res=false;
	
	try {
		
		Statement s = con.createStatement();
		String queryString1 = "update campaigns set status='inactive' where shrtcode='"+cmp+"';";
		System.out.println(queryString1);
		int val1 = s.executeUpdate(queryString1);

		if (val1 == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		System.out.println("can't update campaign");
		ex.printStackTrace();
		return res;
	}
	finally{
		
		conn.conClose(con);
	}
	return res;
	
}

public boolean activateCampaign(String cmp){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res=false;
	
	try {
		
		Statement s = con.createStatement();
		String queryString1 = "update campaigns set status='active' where shrtcode='"+cmp+"';";
		System.out.println(queryString1);
		int val1 = s.executeUpdate(queryString1);

		if (val1 == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		System.out.println("can't update campaign");
		ex.printStackTrace();
		return res;
	}
	finally{
		
		conn.conClose(con);
	}
	return res;
	
}

public CampaignDetails getCampaign(String code){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	CampaignDetails camp=new CampaignDetails();
	ResultSet rs;
	try {
		
		Statement s = con.createStatement();
		String queryString1 = "select * from campaigns where shrtcode='"+code+"';";
		rs=s.executeQuery(queryString1);
		if(rs.next()){
			
			camp.campaignname=rs.getString(1);
			camp.description=rs.getString(2);
			camp.shortcode=rs.getString(3);
			camp.startdate=rs.getString(4);
			camp.enddate=rs.getString(5);
			camp.reply=rs.getString(6);
			
		}
				
			
	}
	catch(Exception ex){
		
		System.out.println("error in get Campaign");
	}
	finally{
		
		conn.conClose(con);
	}
	return camp;
	
}

public ArrayList<ReplyOptions> getReplyOptionsList(){
	
	ArrayList<ReplyOptions> Reparray=new ArrayList<ReplyOptions>();
	Connection con = (Connection)DbConnect.getConnection();// get connection
	Statement s;
	ResultSet rst=null;
	try
	{
	
	s=con.createStatement();
	String queryString="select * from reply_options;";
	rst=s.executeQuery(queryString);
	while(rst.next()){
		
		ReplyOptions g=new ReplyOptions();
		g.setReplyoption(rst.getString(1));
		g.setDescription(rst.getString(2));
					
		Reparray.add(g);
	}
	
	}
	catch(Exception e){
		System.out.println("can't get groups");
		e.printStackTrace();
		return null;
	}
	finally{
		
		conn.conClose(con);
	}
	
return Reparray;
}




}
