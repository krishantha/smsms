package com.sms.profileedit;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





/**
 * Servlet implementation class UpdateUser
 */
@WebServlet("/UpdateUserClass")
public class UpdateUserClass extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String mobile=request.getParameter("txtMobile_no");//get mobile no from web page
		String fullname=request.getParameter("txtFullName");//get Full Name from web page
		String username=(String)request.getSession().getAttribute("username"); //get username from session
		
		profileEditService db=new profileEditService(); //create new object on profileEditService
		
		boolean status=db.updateUserProfile(username,fullname,mobile);//call update function of profile details
		
		if(status)
		{
			response.getWriter().print("User profile Successfully updated..! ");
			return;
		}
		
		else
		{
			response.getWriter().print("Error... profile cannot be updated..! ");
			return;
			
		}
		
		
	}

}
