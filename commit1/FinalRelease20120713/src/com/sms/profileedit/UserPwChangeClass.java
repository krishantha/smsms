package com.sms.profileedit;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class UserPwChange
 */
@WebServlet("/UserPwChangeClass")
public class UserPwChangeClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				String currentpw=request.getParameter("currentpw");//get current password from web page
				String newpw=request.getParameter("newpw");//get new password from web page
				String confmPw=request.getParameter("confmPw");//get confirm password from web page
				String username=(String)request.getSession().getAttribute("username");//get username from session
				
				profileEditService db=new profileEditService(); //create new object on profileEditService
				
				boolean matchedPassword=false;
				if(newpw.equals(confmPw))//check whether new password and confirm password are equal
				{
					matchedPassword=true;
				}
				else
				{
					response.getWriter().print("Password does not match ");
					return;
				}
				
				boolean validPassword=db.checkPassword(username, currentpw);//check whether user entered correct password
				
				if(!validPassword)
				{
					response.getWriter().print("Invalid password ");
					return;
				}
				
				if(validPassword && matchedPassword)
				{
					boolean status=db.updateUserPw(username, newpw);
					
					if(status)
					{
						response.getWriter().print("Password successfully changed ");
						return;
					}
					
					else
					{
						response.getWriter().print("Error..! Password Cannot be changed ");
						return;
						
					}
				}
				else
				{
					response.getWriter().print("Password Cannot be changed ");
					return;
					
				}
		
	}

}
