package com.sms.profileedit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sms.login.DbConnect;



public class profileEditService {
	//update user's full Name and mobile no to the database
	public boolean updateUserProfile(String username,String fullName,String mobile_no){
		
		Connection con=null;
		
		try
		{
		con=DbConnect.getConnection();//get connection
		Statement s=con.createStatement();
		//query which pass to the database
		String queryString="update users set fullname='"+fullName+"', mobile='"+mobile_no+"' where username='"+username+"'";
		int no=s.executeUpdate(queryString);
		
		if(no==1)//check whether successfully executed
		{
			con.close();//close the connection
			return true;
		}
		
		else 
		{
			return false;
		}
		
		
		}
		catch(Exception ex)
		{

			return false;
		}
	}
//update new password to the database	
public boolean updateUserPw(String username,String password){
		
		Connection con=null;
		
		try
		{
		con=DbConnect.getConnection();//get connection
		Statement s=con.createStatement();
		//query which pass to the database
		String queryString="update users set password='"+password+"' where username='"+username+"'";
		int no=s.executeUpdate(queryString);
		
		if(no==1)//check whether successfully executed
		{
			con.close();//close the connection
			return true;
		}
		
		else 
		{
			return false;
		}
		
		
		}
		catch(Exception ex)
		{

			return false;
		}
	}
//check whether valid password
public boolean checkPassword(String username, String newPassword){
	Connection con=null;
	
	try
	{
		con=DbConnect.getConnection();// get connection
		Statement s=con.createStatement();
		//query which pass to the database
		String queryString="select password from users where username='"+username+"'";
		ResultSet rs=s.executeQuery(queryString);//assign data to the resultset object
		String currnetPw=null;
		
		while(rs.next())
		{
			currnetPw=rs.getString(1);//get current password from the database
		}
		if(currnetPw.equals(newPassword))// check whether provided password and current password equals 
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	catch(Exception ex)
	{
		return false;
	}
}

}
