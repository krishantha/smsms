package com.sms.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnect {

static Connection con=null;
	
	public static Connection getConnection()
	{
	
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/sms","root","pw");
			return con;
			
		}
		
		catch(Exception ex)
		{
			System.out.println("could not connect to DB");
			return null;
		}
	}
	
	public void conClose(Connection conn){
		
		try {
            conn.close();
        } catch (SQLException sQLException) {

        	System.out.println("could not close DB");
        }
	}
	
	
	
}
