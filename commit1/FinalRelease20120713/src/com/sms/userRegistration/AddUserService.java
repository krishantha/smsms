package com.sms.userRegistration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sms.login.DbConnect;


public class AddUserService {

	
	
	//Default constructor create the Database Connection Object
	public AddUserService() {
		
		
	}
	
	//Adding a new user to the database
	public boolean addUser(String name,String fullName,String password,String level,String mobileNo ){
		
		
		Connection con = (Connection) DbConnect.getConnection();
		Statement stmt;
		
		try {
			
			stmt = (Statement) con.createStatement();
		    String query = "insert into users values ('"+name+"','"+fullName+"','"+password+"','"+level+"','"+mobileNo+"');";
		    stmt.executeUpdate(query);
		    con.close();
		    return true;
		    
		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a user");
		} finally{
			
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		
		
		return false;
	}
	
	//Get all the user's user-names in the system
	public ArrayList<String> getUserNames(){

		ArrayList<String> result = new ArrayList<String>();
		Connection con = (Connection) DbConnect.getConnection();
		Statement stmt;
	    
		try {
			stmt = (Statement) con.createStatement();
			String query = "select username from users";
			ResultSet rSet = stmt.executeQuery(query);
			while(rSet.next()){
				result.add(rSet.getString(1));
			}
			
		} catch (SQLException e) {
			
			System.out.println("There is an error when getting user-names from the database");
		}
	    
		return result;
	}
	
	
	
}
