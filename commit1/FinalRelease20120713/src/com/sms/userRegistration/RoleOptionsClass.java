package com.sms.userRegistration;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RoleOptionsClass
 */
@WebServlet("/RoleOptionsClass")
public class RoleOptionsClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RoleOptionsClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(request.getParameter("type").equals("getRoles")){
			
			String result="";
			RoleOptionsService service = new RoleOptionsService();
			ArrayList<RoleOptions> allRoles = service.getAllOptions();
			for(int i=0;i<allRoles.size();i++){
				
				result = result + "<input type=\"checkbox\" value=\""+allRoles.get(i).getSubOption()+"\"/>"+allRoles.get(i).getSubOption()+"<br>";
				
			}
			
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
