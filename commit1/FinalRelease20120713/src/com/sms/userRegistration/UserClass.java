package com.sms.userRegistration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserClass
 */
@WebServlet("/UserClass")
public class UserClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String type = request.getParameter("type");
		String userName = request.getParameter("userName");
		String fullName = request.getParameter("fullName");
		String password = request.getParameter("password");
		String number = request.getParameter("number");
		String levels = request.getParameter("levels");
		
		if(type.equals("register")){
			
			System.out.println("Inside::"+userName+":"+
		fullName+":"+password+":"+number+":"+levels);
			
			UserService uService = new UserService();
			uService.addUser(userName, fullName, password, number);
			UserRoleOptionsService rService = new UserRoleOptionsService();
			StringTokenizer groupTokens = new StringTokenizer(levels, ",", false);
			while (groupTokens.hasMoreTokens()) {
				rService.addUserRoles(userName, groupTokens.nextToken());				
			}
		}else if(type.equals("userNameCheck")){
			
			String result = "";
			String input = request.getParameter("msg");
			UserService service = new UserService();
			ArrayList<String> users =  service.getUserNames();
			
			loop:for (int x = 0; x < users.size(); x++) {				
				
				if (users.get(x).equals(input)) {						
					result = "true";
					break loop;
				} else {
					result = "false";
				}
				
		}
		

		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(result);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
