package com.sms.userRegistration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.database.DbConnect;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public class UserService {

	private DbConnect dbCon = null;
	
	//Default constructor create the Database Connection Object
	public UserService() {
		dbCon = new DbConnect();
		
	}
	
	//Adding a new user to the database
	public boolean addUser(String name,String fullName,String password,String mobileNo ){
		
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;
		
		try {
			
			stmt = (Statement) con.createStatement();
		    String query = "insert into users values (\'"+name+"\',\'"+fullName+"\',\'"+password+"\',\'+94"+mobileNo+"\');";
		    System.out.println(query);
		    stmt.executeUpdate(query);
		    dbCon.con_close(con);
		    return true;
		    
		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a user");
		} finally{
			
			dbCon.con_close(con);
		}
		
		
		return false;
	}
	
	//Get all the user's user-names in the system
	public ArrayList<String> getUserNames(){

		ArrayList<String> result = new ArrayList<String>();
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;
	    
		try {
			stmt = (Statement) con.createStatement();
			String query = "select username from users";
			ResultSet rSet = stmt.executeQuery(query);
			while(rSet.next()){
				result.add(rSet.getString(1));
			}
			
		} catch (SQLException e) {
			
			System.out.println("There is an error when getting user-names from the database");
		}
	    
		return result;
	}
	
	
	
}
