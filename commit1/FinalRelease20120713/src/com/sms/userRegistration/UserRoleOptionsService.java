package com.sms.userRegistration;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.database.DbConnect;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class UserRoleOptionsService {

	private DbConnect dbCon = null;
	
	public UserRoleOptionsService(){
		dbCon = new DbConnect();
	}
	
public boolean addUserRoles(String userName,String mainRole){
		
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;
		
		
		try {
			
			
			
			stmt = (Statement) con.createStatement();
			
		    String query = "insert into user_roleoptions (username,suboption) values ('"+userName+"','"+mainRole+"');";
		    System.out.println(query);
		    stmt.executeUpdate(query);
		    dbCon.con_close(con);
		    return true;
		    
		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a user");
		} finally{
			
			dbCon.con_close(con);
		}
		
		
		return false;
	}
	
}
