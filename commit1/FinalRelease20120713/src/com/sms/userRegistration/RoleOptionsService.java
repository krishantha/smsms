package com.sms.userRegistration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.database.DbConnect;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class RoleOptionsService {

	private DbConnect dbCon = null;
	
	public RoleOptionsService(){
		dbCon = new DbConnect();
	}
	
	public ArrayList<RoleOptions> getAllOptions(){
		ArrayList<RoleOptions> result = new ArrayList<RoleOptions>();		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;	    
		try {
			stmt = (Statement) con.createStatement();
			String query = "select * from roleoptions";
			ResultSet resultSet = stmt.executeQuery(query);
			while(resultSet.next()){
				RoleOptions temp = new RoleOptions();
				temp.setMainOption(resultSet.getString(1));
				temp.setSubOption(resultSet.getString(2));
				temp.setLinkUrl(resultSet.getString(3));
				temp.setImgUrl(resultSet.getString(4));
				temp.setIndexRef(Integer.parseInt(resultSet.getString(5)));
				result.add(temp);
			}
			
		} catch (SQLException e) {
			
			System.out.println("There is an error when getting roles from the database");
		}
		
		return result;
	}
}
