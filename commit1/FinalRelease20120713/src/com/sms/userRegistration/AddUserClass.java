package com.sms.userRegistration;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserAddingServlet
 */
@WebServlet("/UserAdd")
public class AddUserClass extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddUserClass() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// Write in post method to get entered values and add user
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (request.getParameter("BtnConfirm").equals("Register")) {

			boolean result = false;
			String name = request.getParameter("UserName");

			if (Validations.validateUser(name)) {
				
				AddUserService user = new AddUserService();
				String password = request.getParameter("UserPassword");
				String fullName = request.getParameter("UserFullName");
				String level = request.getParameter("UserType");
				String mobileNo = request.getParameter("UserNumber");
				//PrintWriter out = response.getWriter();
				result = user.addUser(name,fullName, password, level,"+94"+mobileNo);
			}
			if (result){
				String message = "User added successfully !";			
				response.sendRedirect("AddUser.jsp?message=" + URLEncoder.encode(message, "UTF-8"));
			}
			else{
				String message = "The user-name you entered is already available.Please try another user-name.";
				response.sendRedirect("AddUser.jsp?message=" + URLEncoder.encode(message, "UTF-8"));
			}
			
			
		} else if (request.getParameter("BtnConfirm").equals("Cancel"))
			response.sendRedirect("MainPage.jsp");
	}

}
