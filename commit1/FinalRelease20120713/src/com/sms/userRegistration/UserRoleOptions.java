package com.sms.userRegistration;

public class UserRoleOptions {
	
	

	private String userName;
	private String mainOption;
	
	public UserRoleOptions(String name,String option){
		userName = name;
		mainOption = option;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSubOption() {
		return mainOption;
	}
	public void setSubOption(String subOption) {
		this.mainOption = subOption;
	}
	
	
	
}
