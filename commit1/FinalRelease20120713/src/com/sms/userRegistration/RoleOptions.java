package com.sms.userRegistration;

public class RoleOptions {


	private String mainOption;
	private String subOption;
	private String linkUrl;
	private String imgUrl;
	private int indexRef;
	
	public String getSubOption() {
		return subOption;
	}
	public void setSubOption(String subOption) {
		this.subOption = subOption;
	}
	public String getMainOption() {
		return mainOption;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public int getIndexRef() {
		return indexRef;
	}
	public void setMainOption(String mainOption) {
		this.mainOption = mainOption;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public void setIndexRef(int indexRef) {
		this.indexRef = indexRef;
	}
	
	
	
}
