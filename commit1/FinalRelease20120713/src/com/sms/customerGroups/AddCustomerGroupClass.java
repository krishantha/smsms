package com.sms.customerGroups;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddCustomerGroupClass
 */
@WebServlet("/AddGroup")
public class AddCustomerGroupClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCustomerGroupClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<GroupDetails> groups=new ArrayList<GroupDetails>();
		
		String groupname = request.getParameter("groupname");
		String description = request.getParameter("desc");
		
		CustomerGroupService serv = new CustomerGroupService();
		
		if(serv.GroupExist(groupname)){
			
			String reply="groupExist";
			response.getWriter().write(reply);
			
		}
		else{
			
			boolean addGroup = serv.addGroup(groupname, description);
			
			if(addGroup){
			
				groups=serv.getGrouplist();
				
				String table = "<table width=\"540px\" id=\"groups\" "+
						   "align=\"center\" cellpadding=\"2\" cellspacing=\"2\"> "+
						   "<thead> <tr class=\"ui-widget-header\" height=\"30\"> <th align=\"center\" >Select</th>"+
						   "<th align=\"center\">Group name</th><th align=\"center\">Description</th></tr></thead><tbody>";
				try{
					
					for(int i=0;i<groups.size();i++){
						
						String content = "<tr class=\"ui-widget ui-widget-content\"><td align=\"center\"><input type=\"radio\" name=\"radio\" value="+groups.get(i).getGroupname()+"></td><td>"+groups.get(i).getGroupname()+"</td>"+
					                      "<td>"+groups.get(i).getDescription()+"</td></tr>";
						table = table + content;
						
					}
					table = table + " </tbody> </table>" ;
					
					System.out.println(table);
								
				}
				catch(Exception ex){
					ex.printStackTrace();
					response.getWriter().write("notExist");
				}
				response.setContentType("text/html;charset=UTF-8");
				response.getWriter().write(table);
			}
			
			else{
				
				String reply="Failed";
				response.getWriter().write(reply);
				
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		doGet(request, response);
	}
	

			
}
				
		



