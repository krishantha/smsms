package com.sms.customerGroups;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteGroup
 */
@WebServlet("/DelGrp")
public class DeleteGroup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteGroup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String groupname = request.getParameter("dgrp");
		
		
		ArrayList<GroupDetails> groups=new ArrayList<GroupDetails>();
		
		CustomerGroupService srvice=new CustomerGroupService();
								
		boolean status=srvice.removeGroup(groupname);
		System.out.println(status);	
				groups=srvice.getGrouplist();
				String table = "<table width=\"540px\" id=\"groups\" "+
						   "align=\"center\" cellpadding=\"2\" cellspacing=\"2\"> "+
						   "<thead> <tr class=\"ui-widget-header\" height=\"30\"> <th align=\"center\" >Select</th>"+
						   "<th align=\"center\">Group name</th><th align=\"center\">Description</th></tr></thead><tbody>";
				try{
					
					for(int i=0;i<groups.size();i++){
						
						String content = "<tr class=\"ui-widget ui-widget-content\"><td align=\"center\"><input type=\"radio\" name=\"radio\" value="+groups.get(i).getGroupname()+"></td><td>"+groups.get(i).getGroupname()+"</td>"+
					                      "<td>"+groups.get(i).getDescription()+"</td></tr>";
						table = table + content;
						
					}
					table = table + " </tbody> </table>" ;
					
					System.out.println(table);
								
				}
				catch(Exception ex){
					ex.printStackTrace();
					response.getWriter().write("notExist");
				}
				response.setContentType("text/html;charset=UTF-8");
				response.getWriter().write(table);
			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
