package com.sms.customerGroups;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.sms.database.DbConnect;

public class CustomerGroupService {
	
	DbConnect conn;
	
	public CustomerGroupService(){
		
		conn=new DbConnect();
		
	}
	
	
	
	public boolean addGroup(String groupname, String description) {
		
		
		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res = false;
		try {
			
			Statement s = con.createStatement();
			String queryString = "insert into groups (groupname,description) values('" + groupname + "','" + description + "');";
			System.out.println(queryString);
			int val = s.executeUpdate(queryString);

			if (val == 1) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return res;
		}
		finally{
			conn.conClose(con);
			
		}

		return res;
	}
	
public ResultSet getCustomerlist(){
	Connection con = (Connection)DbConnect.getConnection();// get connection
	Statement s;
	ResultSet rs=null;
	try
	{
	
	s=con.createStatement();
	String queryString="select * from customers;";
	rs=s.executeQuery(queryString);
	
	}
	catch(Exception e){
		
		e.printStackTrace();
		return null;
	}
	finally{
		
		conn.conClose(con);
	}
	return rs;
	
}

	public boolean addGroupCustomers(String groupname, String customerid) {
		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res = false;
		try {
			
			Statement s = con.createStatement();
			String queryString = "insert into group_customers (groupname,customerid) values('" + groupname + "','" + customerid + "');";
			System.out.println(queryString);
			int val = s.executeUpdate(queryString);

			if (val == 1) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return res;
		}
		finally{
			
			conn.conClose(con);
		}

		return res;
	}
	
	public ResultSet SearchCustomerlist(String srch){
		Connection con = (Connection)DbConnect.getConnection();// get connection
		Statement s;
		ResultSet rs=null;
		String queryString;
		try
		{
		
		s=con.createStatement();
		if(srch==null){
		queryString="select * from customers;";
		}
		else{
		queryString="select distinct * from customers where (firstname LIKE '%"+srch+"%')||(middlename LIKE '%"+srch+"%')||(lastname LIKE '%"+srch+"%');";
		System.out.println(queryString);
		}
		rs=s.executeQuery(queryString);
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			return null;
		}
		finally{
			
			conn.conClose(con);
		}
		
		return rs;
	}
	
	public boolean GroupExist(String groupname) {

		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res = false;
		try {
			
			Statement s = con.createStatement();
			String queryString = "select * from groups where groupname='"+ groupname + "'";
			System.out.println(queryString);
			s.executeQuery(queryString);
			ResultSet rs = s.getResultSet();

			if (rs.next()) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return res;
		}
		finally{
					
				conn.conClose(con);
		}

		return res;
	}
	
	public ArrayList<GroupDetails> getGrouplist(){
		ArrayList<GroupDetails> grouparray=new ArrayList<GroupDetails>();
		Connection con = (Connection)DbConnect.getConnection();// get connection
		Statement s;
		ResultSet rst=null;
		try
		{
		
		s=con.createStatement();
		String queryString="select * from groups;";
		rst=s.executeQuery(queryString);
		while(rst.next()){
			
			GroupDetails g=new GroupDetails();
			g.setGroupname(rst.getString(1));
			g.setDescription(rst.getString(2));
			grouparray.add(g);
		}
		
		}
		catch(Exception e){
			System.out.println("can't get groups");
			e.printStackTrace();
			return null;
		}
		finally{
			
			conn.conClose(con);
		}
		
	return grouparray;
}
	
public boolean removeGroup(String group){
		
		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res=false;
		
		try {
			
			Statement s = con.createStatement();
			String queryString1 = "delete from group_customers where groupname='"+group+"';";
			String queryString2 = "delete from groups where groupname='"+group+"';";
			System.out.println(queryString1);
			System.out.println(queryString2);
			int val1 = s.executeUpdate(queryString1);
			int val2 = s.executeUpdate(queryString2);

			if (val1 == 1 && val2 == 1) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			System.out.println("can't delete group");
			ex.printStackTrace();
			return res;
		}
		finally{
			
			conn.conClose(con);
		}
		return res;
		
	}

public String getDescription(String group){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	String res="";
	ResultSet rs;
	try {
		
		Statement s = con.createStatement();
		String queryString = "select description from groups where groupname='"+group+"';";
		rs=s.executeQuery(queryString);
		if(rs.next()){
			
			res=rs.getString(1);
			
		}
	}
	catch(Exception ex){
		
		
	}
	finally{
		
		conn.conClose(con);
	}
	return res;
		
}

public ArrayList<CustomerDetails> getCustomerlist(String groupname){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	ArrayList<CustomerDetails> cusArray=new ArrayList<CustomerDetails>();
	Statement s;
	ResultSet rs=null;
	try
	{
	
	s=con.createStatement();
	String queryString="select * from customers where customerid in(select customerid from group_customers where groupname='"+groupname+"') ;";
	rs=s.executeQuery(queryString);
	
	while(rs.next()){
		
		
		CustomerDetails c=new CustomerDetails();
		c.setFirstName(rs.getString(1));
		c.setMiddleName(rs.getString(2));
		c.setLastName(rs.getString(3));
		c.setCustomerId(rs.getString(4));
		c.setDateOfBirth(rs.getString(5));
		c.setAddress(rs.getString(6));
		c.setMobileNo(rs.getString(7));
		cusArray.add(c);
					
	}
	
	}
	catch(Exception e){
		
		e.printStackTrace();
		return null;
	}
	finally{
		
		conn.conClose(con);
		
	}
	return cusArray;
}



public ArrayList<CustomerDetails> ngetCustomerlist(String groupname){
	
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	ArrayList<CustomerDetails> cusArray=new ArrayList<CustomerDetails>();
	Statement s;
	ResultSet rs=null;
	try
	{
	
	s=con.createStatement();
	String queryString="select * from customers where customerid not in(select customerid from group_customers where groupname='"+groupname+"') ;";
	rs=s.executeQuery(queryString);
	
	while(rs.next()){
		
		
		CustomerDetails c=new CustomerDetails();
		c.setFirstName(rs.getString(1));
		c.setMiddleName(rs.getString(2));
		c.setLastName(rs.getString(3));
		c.setCustomerId(rs.getString(4));
		c.setDateOfBirth(rs.getString(5));
		c.setAddress(rs.getString(6));
		c.setMobileNo(rs.getString(7));
		cusArray.add(c);
					
	}
	
	
	}
	catch(Exception e){
		
		e.printStackTrace();
		return null;
	}
	finally{
		
		conn.conClose(con);
		
	}
	
	return cusArray;

}

public ArrayList<CustomerDetails> srchngetCustomerlist(String groupname,String srch){
	
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	ArrayList<CustomerDetails> cusArray=new ArrayList<CustomerDetails>();
	Statement s;
	ResultSet rs=null;
	try
	{
	
	s=con.createStatement();
	String queryString="select distinct * from customers where ((firstname LIKE '%"+srch+"%')||(middlename LIKE '%"+srch+"%')||(lastname LIKE '%"+srch+"%')) and customerid not in(select customerid from group_customers where groupname='"+groupname+"') ;";
	rs=s.executeQuery(queryString);
	
	while(rs.next()){
		
		
		CustomerDetails c=new CustomerDetails();
		c.setFirstName(rs.getString(1));
		c.setMiddleName(rs.getString(2));
		c.setLastName(rs.getString(3));
		c.setCustomerId(rs.getString(4));
		c.setDateOfBirth(rs.getString(5));
		c.setAddress(rs.getString(6));
		c.setMobileNo(rs.getString(7));
		cusArray.add(c);
					
	}
	
	
	}
	catch(Exception e){
		
		e.printStackTrace();
		return null;
	}
	finally{
		
		conn.conClose(con);
		
	}
	
	return cusArray;

}


public boolean removeCustomer(String id,String group){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res=false;
	
	try {
		
		Statement s = con.createStatement();
		String queryString = "delete from group_customers where groupname='"+group+"' and customerid='"+id+"';";
		System.out.println(queryString);
		int val = s.executeUpdate(queryString);

		if (val == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		return res;
	}
	finally{
		
		conn.conClose(con);
		
	}
	
	return res;
	
}

public boolean insertCustomer(String id,String group){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res=false;
	
	try {
		
		Statement s = con.createStatement();
		String queryString = "insert into group_customers (groupname,customerid) values('"+group+"','"+id+"');";
		System.out.println(queryString);
		int val = s.executeUpdate(queryString);

		if (val == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		return res;
	}
	
	finally{
		
		conn.conClose(con);
		
	}
	
	return res;
	
}


public boolean setDescription(String group,String description){
	
	Connection con = (Connection)DbConnect.getConnection();// get connection
	boolean res=false;
	
	try {
		
		Statement s = con.createStatement();
		String queryString = "update groups set description='"+description+"' where groupname='"+group+"';";
		System.out.println(queryString);
		int val = s.executeUpdate(queryString);

		if (val == 1) {
			res = true;
		}

		else {
			res = false;
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		return res;
	}
	
	finally{
		
		conn.conClose(con);
		
	}
	
	return res;
	
}


}
