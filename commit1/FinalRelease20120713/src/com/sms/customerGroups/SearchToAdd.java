package com.sms.customerGroups;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SearchToAdd
 */
@WebServlet("/SearchToAdd")
public class SearchToAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchToAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String group=request.getParameter("grpname");
		String name=request.getParameter("customer");
		
		ArrayList<CustomerDetails> cust=new ArrayList<CustomerDetails>();
		CustomerGroupService serv=new CustomerGroupService();
		cust=serv.srchngetCustomerlist(group, name);
		
		String table = "<table width=\"500px\" id=\"group_cust\" "+
				   "align=\"center\" cellpadding=\"2\" cellspacing=\"2\"> "+
				   "<thead> <tr class=\"ui-widget-header\" height=\"30\"> <th align=\"center\" ><input type=\"checkbox\" id=\"addall\" name=\"checkadd\" onclick=\"chkall(this.name);\"></th>"+
				   "<th align=\"center\">Name</th><th align=\"center\">ID</th><th align=\"center\">Mobile No</th></tr></thead><tbody>";
		try{
			
			for(int i=0;i<cust.size();i++){
				
				String content = "<tr class=\"ui-widget ui-widget-content\"><td align=\"center\"><input type=\"checkbox\" name=\"checkToAdd\" value="+cust.get(i).getCustomerId()+"></td><td>"+cust.get(i).getFirstName()+" "+cust.get(i).getLastName()+"</td>"+
			                      "<td>"+cust.get(i).getCustomerId()+"</td><td>"+cust.get(i).getMobileNo()+"</td></tr>";
				table = table + content;
				
			}
			table = table + " </tbody> </table>" ;
			
			System.out.println(table);
						
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.out.println("Table cannot be loaded");
		}
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(table);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
