package com.sms.templates;

public class SmsTemplate {

	private String templateName;
	private String templateCategory;
	private String template;
	
	public SmsTemplate(){
		
	}
	public SmsTemplate(String templateName,String templateCategory,String message){
		this.templateName=templateName;
		this.templateCategory = templateCategory;
		template = message;
	}
	
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateCategory() {
		return templateCategory;
	}
	public void setTemplateCategory(String templateCategory) {
		this.templateCategory = templateCategory;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}	
	
}
