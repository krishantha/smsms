package com.sms.templates;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.database.DbConnect;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;

public class SmsTemplateService {
private DbConnect dbCon = null;
	
	public SmsTemplateService(){
		dbCon = new DbConnect();
	}
	
	public boolean addTemplate(SmsTemplate template){
		
		boolean result = true;
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			String query = "insert into smstemplates values('"+template.getTemplateName()+"','"+template.getTemplateCategory()+"','"+template.getTemplate()+"');";
			
			System.out.println(query);
			stmt.executeUpdate(query);
			dbCon.con_close(con);
			return true;

		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a Category to templates");
		} finally {

			dbCon.con_close(con);
		}
		return result; 
	}
	
	public ArrayList<SmsTemplate> getTempltes(String category){
		ArrayList<SmsTemplate> result = new ArrayList<SmsTemplate>();
		
		Connection conn = (Connection) dbCon.getConnection();
        Statement stmt;
        try {
            
            stmt = (Statement) conn.createStatement();
            String query = ("select * from smstemplates where templatecategory = \'"+category+"\'");
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
            	SmsTemplate temp = new SmsTemplate();
            	temp.setTemplateName(rs.getString(1));
            	temp.setTemplateCategory(rs.getString(2));
            	temp.setTemplate(rs.getString(3));
            	result.add(temp);
            }
            
        } catch (SQLException ex) {
            System.out.println("Error in sms template category services");
        }
		
		return result;
	}
	
	public String getTemplateMessage(String category,String templateName){
		String result ="";
		Connection conn = (Connection) dbCon.getConnection();
        Statement stmt;
        try {
            
            stmt = (Statement) conn.createStatement();
            String query = ("select template from smstemplates where templatecategory = \'"+category+"\' and templatename = \'"+templateName+"\' ");
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
            	result = rs.getString(1);
            }
            
        } catch (SQLException ex) {
            System.out.println("Error in sms template category services");
        }
		return result;
	}
	
	public void updateTemplateMessage(String category,String templateName,String Message){
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			String query = "update smstemplates set template = \'"+Message+"\' where templatecategory = \'"+category+"\' and templatename = \'"+templateName+"\'";
			stmt.executeUpdate(query);
			
			

		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a Category to templates");
		} finally {

			dbCon.con_close(con);
		}
		
	}
	
	public void removeTemplate(String category,String templateName){
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			String query2 = "delete from smstemplates where templatecategory = \'"+category+"\' and templatename =\'"+templateName+"\' "; 		
			stmt.executeUpdate(query2);
			
			

		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a Category to templates");
		} finally {

			dbCon.con_close(con);
		}
		
	}
	
	
}
