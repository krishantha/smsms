package com.sms.templates;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sun.java2d.DestSurfaceProvider;

import com.database.DbConnect;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.sms.sendingSms.Customers;

public class SmsTemplateCategoryService {

	private DbConnect dbCon = null;
	
	public SmsTemplateCategoryService(){
		dbCon = new DbConnect();
	}
	
	public boolean addCategory(SmsTemplateCategory category){
		
		boolean result = true;
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			String query = "insert into smstemplatecategories values('"+category.getCategoryName()+"','"+category.getDescription()+"');";
			
			System.out.println(query);
			stmt.executeUpdate(query);
			dbCon.con_close(con);
			return true;

		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a Category to templates");
		} finally {

			dbCon.con_close(con);
		}
		return result; 
	}
	
	public ArrayList<String> getAllCategories(){
		
		ArrayList<String> result = new ArrayList<String>();
		
		Connection conn = (Connection) dbCon.getConnection();
        Statement stmt;
        try {
            
            stmt = (Statement) conn.createStatement();
            String query = ("select categoryname from smstemplatecategories");
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
            	
            	result.add(rs.getString(1));
            }
            
        } catch (SQLException ex) {
            System.out.println("Error in sms template category services");
        }
		
		return result;
	}
	public void removeCategory(String category){
		
		Connection con = (Connection) dbCon.getConnection();
		Statement stmt;

		try {

			stmt = (Statement) con.createStatement();
			String query1 = "delete from smstemplates where templatecategory = \'"+category+"\'";
			String query2 = "delete from smstemplatecategories where categoryname = \'"+category+"\'"; 
			stmt.executeUpdate(query1);
			stmt.executeUpdate(query2);
			
			

		} catch (SQLException e) {
			System.out.println("SQL exception fired when adding a Category to templates");
		} finally {

			dbCon.con_close(con);
		}
		
	}
}
