package com.sms.templates;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sms.groups.Groups;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;

/**
 * Servlet implementation class SmsTemplateClass
 */
@WebServlet("/SmsTemplateClass")
public class SmsTemplateClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /** 
     * @see HttpServlet#HttpServlet()
     */
    public SmsTemplateClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//String tab = request.getParameter("message");
		String categoryName = request.getParameter("category");
		//String categoryDescription = request.getParameter("catdescription");
		String templateName = request.getParameter("templateName");
		String message = request.getParameter("message");
		String type = request.getParameter("type");
		
		//System.out.println("Doget executing : " + categoryName +"-"+categoryDescription+"-"
							//+templateName+"-"+message+"-"+type);
		
		if(type.equals("loadTemplateCombo")){
			SmsTemplateService service = new SmsTemplateService();
			ArrayList<SmsTemplate> temp = new ArrayList<SmsTemplate>();
			temp = service.getTempltes(categoryName);
			String result = "";
			for(int i=0;i<temp.size();i++){
				result= result +"\'<option value=\""+ temp.get(i).getTemplateName()+"\">"+temp.get(i).getTemplateName()+"</option>\'";
			}
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
		}else if(type.equals("loadTemplate")){
			SmsTemplateService service = new SmsTemplateService();
			String result = "";
			result = service.getTemplateMessage(categoryName, templateName);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
		}else if(type.endsWith("updateTemplate")){
			
			SmsTemplateService service = new SmsTemplateService();
			service.updateTemplateMessage(categoryName, templateName, message);
			
		}else if(type.equals("deleteTemplate")){
			SmsTemplateService service = new SmsTemplateService();
			service.removeTemplate(categoryName, templateName);
		}else if(type.equals("templateCheck")){
			
			SmsTemplateService service = new SmsTemplateService();			
			String result = "null";
			ArrayList<SmsTemplate> allTemplates = service.getTempltes(categoryName);
			String template = request.getParameter("msg");			
			
			loop:for (int x = 0; x < allTemplates.size(); x++) {				
					
					if (allTemplates.get(x).getTemplateName().equals(template)) {						
						result = "true";
						break loop;
					} else {
						result = "false";
					}
					
			}
			

			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
