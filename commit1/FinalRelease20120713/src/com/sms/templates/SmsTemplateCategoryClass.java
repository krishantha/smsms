package com.sms.templates;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SmsTemplateCategoryClass
 */
@WebServlet("/SmsTemplateCategoryClass")
public class SmsTemplateCategoryClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SmsTemplateCategoryClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String categoryName = request.getParameter("catname");
		String categoryDescription = request.getParameter("catdescription");
		String templateName = request.getParameter("templatename");
		String message = request.getParameter("msg");
		String type = request.getParameter("type");
		//System.out.println("Doget executing : " + categoryName +"-"+categoryDescription+"-"
							//+templateName+"-"+message+"-"+type);
		SmsTemplateCategory category = new SmsTemplateCategory(categoryName, categoryDescription);
		SmsTemplate template = new SmsTemplate(templateName, categoryName, message);
		
		if(type.equals("adding")){
			System.out.println("adding called");
			SmsTemplateCategoryService catService = new SmsTemplateCategoryService();			
			catService.addCategory(category);
			SmsTemplateService temService = new SmsTemplateService();
			temService.addTemplate(template);
			
		}
		else if(type.equals("CategoryCombo")){
			SmsTemplateCategoryService service = new SmsTemplateCategoryService();
			ArrayList<String> categoryList = new ArrayList<String>();
			categoryList = service.getAllCategories();
			String result = "";       // "<select id=\"combobox\">";			
			for(int i=0;i<categoryList.size();i++){
				result= result +"\'<option value=\""+ categoryList.get(i).toString()+"\">"+categoryList.get(i).toString()+"</option>\'";
			}
			//result = result+"</select>";
			//System.out.println(result);
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
			
		}else if(type.equals("addingtemplate")){
			SmsTemplateService service = new SmsTemplateService();
			service.addTemplate(new SmsTemplate(templateName, categoryName, message));
			
		}else if(type.equals("deleteCategory")){
			SmsTemplateCategoryService service = new SmsTemplateCategoryService();
			service.removeCategory(categoryName);
		}else if(type.equals("categoryCheck")){
			SmsTemplateCategoryService service = new SmsTemplateCategoryService();				
			String result = "null";
			ArrayList<String> allCategories = service.getAllCategories();
			String catName = request.getParameter("msg");			
			
			loop:for (int x = 0; x < allCategories.size(); x++) {				
					
					if (allCategories.get(x).equals(catName)) {						
						result = "true";
						break loop;
					} else {
						result = "false";
					}
					
			}
			

			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write(result);
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
