/**
 * 
 */
package com.sms.login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * @author user
 *
 */
public class LoginService {
	
	Connection con=null;
	Statement s;
	
	public boolean authenticateUser(String username,String password)
	{
		
		try
		{
		con=DbConnect.getConnection();//get connection
		s=con.createStatement();
		String queryString="select * from users where username='"+username+"' and password='"+password+"';";
		System.out.println(queryString);
		s.executeQuery(queryString);
		ResultSet rs=s.getResultSet();
		
		if(rs.next())
		{
	
			con.close();//close the connection
			return true;
		}
		
		else 
		{
			return false;
		}
		
		
		}
		catch(Exception ex)
		{

			return false;
		}
		
		
	}
	
	public ArrayList<String[]> getOptions(String username)
	{
		System.out.println("Running getOptions....");
		
		ArrayList<String[]> list=new ArrayList<String[]>();
		
		try{
			
		con=DbConnect.getConnection();//get connection
		s=con.createStatement();
		
	
		String queryString2="select r.mainoption,r.suboption,r.linkurl,r.imgurl from roleoptions r,user_roleoptions o where r.suboption=o.suboption and o.username='"+username+"' order by r.indexref asc";
		System.out.println("getOptions Method :"+queryString2);
		
		
		s.executeQuery(queryString2);		
		
		
		ResultSet rset=s.getResultSet();

		System.out.println("Came to while loop");
		while(rset.next())//traverse through the while loop
		{
			String[] _options=new String[4];
			_options[0]=rset.getString("r.mainoption");
			_options[1]=rset.getString("r.suboption");
			_options[2]=rset.getString("r.linkurl");
			_options[3]=rset.getString("r.imgurl");
			
			list.add(_options);
			
			
		}
		
		
		
		//returns the role options to the main page
		}
		
		catch(Exception ex)
		{
		
			
		}
	
		return list;
	}

}
