package com.sms.login;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainPageClass
 */
@WebServlet("/MainPage")
public class MainPageClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainPageClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String username=request.getSession().getAttribute("username").toString();
		LoginService loginService=new LoginService();
		
		System.out.println("User Name:" + username);
		ArrayList<String[]> options=loginService.getOptions(username);
		
		String mainoption="";
		StringBuilder stringbuilder=new StringBuilder("");
		boolean flag=false;
		
		//System.out.println("Income : "+loginService.getOptions(username));
		
		
		for(String[] optionsv:options)
		{
		
			try{
			if(optionsv[0].compareTo(mainoption)!=0)
			{
		
				mainoption=optionsv[0];
				
				if(flag)
				{
				stringbuilder.append("</p></div>");
				}
			
				
				
				stringbuilder.append("<h3><span class=\"subhead\">"+mainoption+"</span></h3><div><p>");
				
				flag=true;
				
				
			}
			}
			
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage()+" "+e.toString());
			}
			
			stringbuilder.append("<a href=\""+optionsv[2]+"\" class=\"link\" id=\"" +optionsv[1]+"\" >"+optionsv[1]+"</a><br>");
		}
		
		stringbuilder.append("</p></div>");
		
		System.out.println(stringbuilder.toString());
		response.getWriter().write(stringbuilder.toString());
	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);
	}

}
