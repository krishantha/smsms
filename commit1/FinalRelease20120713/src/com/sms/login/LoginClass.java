package com.sms.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.java.swing.plaf.windows.resources.windows;

/**
 * Servlet implementation class LoginClass
 */
@WebServlet("/Login")
public class LoginClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	
		String username=request.getParameter("username");
		String password=request.getParameter("password");//input details in the login page
		LoginService loginService=new LoginService();
		
		boolean result=loginService.authenticateUser(username, password);
		
		if(result)//when the login is successful
		{
			System.out.println("true");
			request.getSession().setAttribute("username", username);
			request.getSession().setAttribute("isAuthenticated", "true");
			response.sendRedirect("main.jsp");//redirect to the Main Page
			return;
		}
		
		else //when the login attempt fails
		{
			
			request.getSession().setAttribute("loginError", "Login failed! Please try again");
			request.getSession().setAttribute("isAuthenticated", "false");
			response.setStatus(400);
			response.sendRedirect("login.jsp");//redirect to the Login Page
			return;
			
			
		}
		
	}

}
