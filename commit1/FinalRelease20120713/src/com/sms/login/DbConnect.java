package com.sms.login;

import java.sql.Connection;
import java.sql.DriverManager;


public class DbConnect {
	
static Connection con=null;
	
	public static Connection getConnection()
	{
	
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/sms","root","pw");
			return con;
			
		}
		
		catch(Exception ex)
		{
			return null;
		}
	}

}
