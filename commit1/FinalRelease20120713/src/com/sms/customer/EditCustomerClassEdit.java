package com.sms.customer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sms.addCustomer.AddCustomerService;

/**
 * Servlet implementation class EditCustomerClassEdit
 */
@WebServlet("/CustomerEdit")
public class EditCustomerClassEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditCustomerClassEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String operation;
		String customerno;
		String firstname;
		String middlename;
		String lastname;
		String mobileno;
		String address;
		String DOB;
		
		

		operation=request.getParameter("oper").toString();
		
		
		System.out.println(operation);
		
		
		
		
		if(operation.contains("edit"))
		{
			EditCustomerService editCustomerService=new EditCustomerService();
			customerno=request.getParameter("scustomerNo").toString();
			firstname=request.getParameter("sfirstName").toString();
			middlename=request.getParameter("smiddleName").toString();
			lastname=request.getParameter("slastName").toString();
			mobileno=request.getParameter("smobileNo").toString();
			address=request.getParameter("saddress").toString();
			DOB=request.getParameter("sdob").toString();
			boolean status=editCustomerService.updateCustomerInfo(customerno, firstname, middlename, lastname, customerno, address, mobileno,DOB);
		}
		
		else if(operation.contains("add"))
		{
			AddCustomerService addustomerservice=new AddCustomerService();
			customerno=request.getParameter("scustomerNo").toString();
			firstname=request.getParameter("sfirstName").toString();
			middlename=request.getParameter("smiddleName").toString();
			lastname=request.getParameter("slastName").toString();
			mobileno=request.getParameter("smobileNo").toString();
			address=request.getParameter("saddress").toString();
			DOB=request.getParameter("sdob").toString();
			boolean status=addustomerservice.addCustomer(firstname, middlename, lastname, customerno, address, mobileno, DOB);
		}
		
		else if(operation.contains("del"))
		{	
			String strCusNo="";
			String _id=request.getParameter("id").toString();
			int id=Integer.parseInt(_id); 
			System.out.println(_id);
			
			EditCustomerService editCustomerService=new EditCustomerService();
			
				
				try {
					strCusNo=editCustomerService.getCustomerNo(id);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(strCusNo);
				editCustomerService.deleteCustomer(strCusNo);
		

			
			
		}
		
		response.setStatus(200);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request,response);
		
	}

}
