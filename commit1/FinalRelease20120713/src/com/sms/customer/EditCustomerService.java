package com.sms.customer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.sms.login.DbConnect;

public class EditCustomerService {
	
	
	Connection con=null;
	Statement statement;
	
	
	public int getTotalCustomers() throws SQLException
	{
		con=DbConnect.getConnection();
		
		String queryString="SELECT * FROM customers";
		
		statement=con.createStatement();
		
		statement.executeQuery(queryString);
		
		ResultSet resultSet=statement.getResultSet();
		
		int icount;
		resultSet.last();
		icount=resultSet.getRow();
		
		return icount;
		
	}
	
	
	public ArrayList<Customer> getSearchResults(String keyword,int limit,int rows) throws SQLException
	{
	
			
		con=DbConnect.getConnection();//get connection
		
		statement=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		
		String queryString="(SELECT customerid,firstname,middlename,lastname,dateofbirth,mobileno,address FROM customers WHERE firstname LIKE '%"+keyword+"%' )UNION (SELECT customerid,firstname,middlename,lastname,dateofbirth,mobileno,address FROM customers WHERE middlename LIKE '%"+keyword+"%') UNION (SELECT customerid,firstname,middlename,lastname,dateofbirth,mobileno,address FROM customers WHERE lastname LIKE '%"+keyword+"%' )UNION (SELECT customerid,firstname,middlename,lastname,dateofbirth,mobileno,address FROM customers WHERE mobileno LIKE '%"+keyword+"%') UNION (SELECT customerid,firstname,middlename,lastname,dateofbirth,mobileno,address FROM customers WHERE address LIKE '%"+keyword+"%' limit "+limit+","+rows+" );";

		statement.executeQuery(queryString);
		
		ResultSet resultSet=statement.getResultSet();
		
	

		clearTemp();
		
				ArrayList<Customer> customers=new ArrayList<Customer>();
				int id=1;
		
				while(resultSet.next())
				{
					
					Customer customer=new Customer();
					customer.setId(id++);
					customer.setScustomerNo(resultSet.getString("customerid"));
					customer.setSfirstName(resultSet.getString("firstname"));
					customer.setSmiddleName(resultSet.getString("middlename"));
					customer.setSlastName(resultSet.getString("lastname"));
					customer.setSaddress(resultSet.getString("address"));
					customer.setSmobileNo(resultSet.getString("mobileno"));
					customer.setDob(resultSet.getString("dateofbirth"));
					insertTemp(customer.getId(), customer.getScustomerNo());
					customers.add(customer);

				}
		
		
		con.close();

		return customers;
		
	}
	
	public  ArrayList<String[]> getMatches(String keyword) throws SQLException
	{

		ArrayList<String[]> list=null;
		
			
		con=DbConnect.getConnection();//get connection
		statement=con.createStatement();
		
		String queryString=getFilterString(keyword);
		
		statement.executeQuery(queryString);
		

		ResultSet resultSet=statement.getResultSet();

				list= new ArrayList<String[]>();
		
				while(!resultSet.isLast())
					{
					
						resultSet.next();
			
						String[] svalues=new String[1];
						svalues[0]=(resultSet.getString("value"));
						list.add(svalues);
			
					}
			
		
		con.close();
		
		return list;
		
	}
	
	public String getFilterString(String keyword)
	{
		return "(SELECT firstname as value from customers WHERE firstname LIKE '%"+keyword+"%' ) UNION (SELECT middlename as value FROM customers WHERE middlename LIKE '%"+keyword+"%') UNION (SELECT lastname as value FROM customers WHERE lastname LIKE '%"+keyword+"%' ) UNION (SELECT mobileno as value FROM customers WHERE mobileno LIKE '%"+keyword+"%') UNION (SELECT address as value FROM customers WHERE address LIKE '%"+keyword+"%');";
	}

	
	public boolean updateCustomerInfo(String customerno,String firstname,String middlename,String lastname,String customerid,String address,String mobileno,String DOB)
	{
		boolean status;
		
		try{
		con=DbConnect.getConnection();//get connection
		Statement s=con.createStatement();
		String queryString="update customers set firstname='"+firstname+"' , middlename='"+middlename+"' , lastname='"+lastname+"' , mobileno='"+mobileno+"' , address='"+address+"',dateofbirth='"+DOB+"' where customerid='"+customerno+"'";
		s.executeUpdate(queryString);
		con.close();
		status=true;
		}
		
		catch(Exception ex)
		{
			status=false;
			
		}
			
		return status;
		
	}
	
	
	public boolean deleteCustomer(String customerNo)
	{
		
		boolean status;
		
		try{
		con=DbConnect.getConnection();//get connection
		Statement s=con.createStatement();
		String queryString="delete from customers where customerid='"+customerNo+"'";
		s.executeUpdate(queryString);
		con.close();
		status=true;
		}
		
		catch(Exception ex)
		{
			status=false;
			
		}
			
		return status;
		
	}
	
	
	public boolean insertTemp(int id,String cno)
	{
		
		boolean status;
		
		try{
		con=DbConnect.getConnection();//get connection
		Statement s=con.createStatement();
		String queryString="insert into temp values("+id+",'"+cno+"');";
		s.executeUpdate(queryString);
		con.close();
		status=true;
		}
		
		catch(Exception ex)
		{
			status=false;
			
		}
			
		return status;
		
	}
	
	public boolean clearTemp()
	{
		
		boolean status;
		
		try{
		con=DbConnect.getConnection();//get connection
		Statement s=con.createStatement();
		String queryString="delete from temp";
		s.executeUpdate(queryString);
		con.close();
		status=true;
		}
		
		catch(Exception ex)
		{
			status=false;
			
		}
			
		return status;
		
	}
	
	public String getCustomerNo(int id) throws SQLException
	{
		con=DbConnect.getConnection();
		
		
		String queryString="SELECT * FROM temp where id="+id;
		
		statement=con.createStatement();
		
		statement.executeQuery(queryString);
		
		ResultSet resultSet=statement.getResultSet();
		
		
		resultSet.first();
	
		
		String returnval= resultSet.getString("customerno");
		
		
		
		return returnval;
	}

}
