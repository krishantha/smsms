package com.sms.customer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.google.gson.Gson;

/**
 * Servlet implementation class EditCustomerClass
 */
@WebServlet("/EditCustomer")
public class EditCustomerClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditCustomerClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated metdod stub
		String rows=request.getParameter("rows");
	    String pageno=request.getParameter("page");
	    
	    String cpage=pageno;
		JSONObject responsedata=new JSONObject();
		
		JSONArray cellarray=new net.sf.json.JSONArray();
	    
		String skeyword=request.getParameter("keyword");
		String sResponse="<div class='ui-widget'><div class='ui-state-highlight ui-corner-all' style='margin-top: 20px; padding: 0 .7em;'> <p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span><strong>Sorry!</strong>No Results found</p></div></div>";
		EditCustomerService editCustomerService=new EditCustomerService();
		ArrayList<Customer> resultList;
		
		
		try 
		{
			
			
			int icount=editCustomerService.getTotalCustomers();

			int ipageValue=(icount/Integer.parseInt(rows));
			
			int limitstart=(Integer.parseInt(rows)*Integer.parseInt(pageno))-Integer.parseInt(rows);
			
			
			 int total=icount/Integer.parseInt(rows);
		     String totalrow=String.valueOf(total+1);
		     
		 	resultList = editCustomerService.getSearchResults(skeyword,limitstart,Integer.parseInt(rows));
		 	
		 	
			
		 	request.getSession().setAttribute("customers",resultList);
		 	
	        responsedata.put("total",totalrow);
	        responsedata.put("page",cpage);
	        responsedata.put("records",icount);

	        JSONArray cell=new net.sf.json.JSONArray();
	        JSONObject cellobj=new net.sf.json.JSONObject();
	        

			int index=1;
	        
			if(!resultList.isEmpty())
			{
			
				for(Customer customer:resultList)
				{
					cellobj.put("id", index++);
					cell.add(customer.getScustomerNo());
					cell.add(customer.getSfirstName());
					cell.add(customer.getSmiddleName());
					cell.add(customer.getSlastName());
					cell.add(customer.getDob());
					cell.add(customer.getSmobileNo());
					cell.add(customer.getSaddress());
					
					cellobj.put("cell", cell);
					cell.clear();
					cellarray.add(cellobj);
				}
				
				responsedata.put("rows",cellarray);

			}
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		catch (Exception e)
		{
			
		}
	
		
		response.getWriter().write(responsedata.toString());
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated metdod stub
		doGet(request,response);
	}
	
	private String setResponse(ArrayList<String[]> list)
	{
		
			StringBuilder builder=new StringBuilder();
			builder.append("<link href=\"tablecloth/tablecloth.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" /><script type=\"text/javascript\" src=\"tablecloth/tablecloth.js\"> </script> this.clickAction = function(obj) {alert($(obj).closest('td').siblings(':first').text());};<script></script><table><tr><th>ID</th><th>First Name</th><th>Middle Name</th><th>Last Name</th><th>Phone Number</th><th>Address</th></tr>");
			String sresultString=null;
			String snoResults="<div class='ui-widget'><div class='ui-state-highlight ui-corner-all' style='margin-top: 20px; padding: 0 .7em;'> <p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span><strong>Sorry!</strong>No Results found</p></div></div>";
			
			try{
			
			for(String[] value:list)
		    { 	
				builder.append("<tr>");
		    	builder.append("<td>"+value[0]+"</td>");
		    	builder.append("<td>"+value[1]+"</td>");
		    	builder.append("<td>"+value[2]+"</td>");
		    	builder.append("<td>"+value[3]+"</td>");
		    	builder.append("<td>"+value[4]+"</td>");
		    	builder.append("<td>"+value[5]+"</td>");
		    	builder.append("</tr>");
		    }
			
			builder.append("</table>");
			sresultString=builder.toString();
			
			}
			
			catch(Exception ex)
			{
				sresultString=snoResults;
			}

			return sresultString;
	}
	
	private String finalString(ArrayList<Customer> list)
	{
		
		String sresponseString=null;
		int index=1;
		
		for(Customer customer:list)
		{
		
			sresponseString="{ \"id\":\""+index+"\" ,cell[";
			
		
		}
		
		
		return "OK";
		
	}
	

}
