package com.sms.customer;

public class Customer {
	
	private String scustomerNo;
	private String sfirstName;
	private String smiddleName;
	private String slastName;
	private String smobileNo;
	private String saddress;
	private String dob;
	private int id;
	
	public String getScustomerNo() {
		return scustomerNo;
	}
	public void setScustomerNo(String scustomerNo) {
		this.scustomerNo = scustomerNo;
	}
	public String getSfirstName() {
		return sfirstName;
	}
	public void setSfirstName(String sfirstName) {
		this.sfirstName = sfirstName;
	}
	public String getSmiddleName() {
		return smiddleName;
	}
	public void setSmiddleName(String smiddleName) {
		this.smiddleName = smiddleName;
	}
	public String getSlastName() {
		return slastName;
	}
	public void setSlastName(String slastName) {
		this.slastName = slastName;
	}
	public String getSmobileNo() {
		return smobileNo;
	}
	public void setSmobileNo(String smobileNo) {
		this.smobileNo = smobileNo;
	}
	public String getSaddress() {
		return saddress;
	}
	public void setSaddress(String saddress) {
		this.saddress = saddress;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	

}
