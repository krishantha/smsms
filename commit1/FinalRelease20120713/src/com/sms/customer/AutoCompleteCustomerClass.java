package com.sms.customer;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AutoCompleteCustomerClass
 */
@WebServlet("/AutoCompleteCustomer")
public class AutoCompleteCustomerClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutoCompleteCustomerClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		// TODO Auto-generated method stub
		
		String smatchingResult=null;
	    EditCustomerService editCustomerService=new EditCustomerService();
	    StringBuilder builder=new StringBuilder();
	   
	    	try
	    	{
	    		
	    		String skeyword= request.getParameter("term");
	    		System.out.println(skeyword);///remove
	    		ArrayList<String[]> suggestions=editCustomerService.getMatches(skeyword);
	    
	    		builder.append("[");
	    
	    		for(String[] v:suggestions)
	    		{ 	
	    			builder.append("\""+ v[0] +"\",");	   
	    		}
	    
	   
	    		builder.deleteCharAt(builder.lastIndexOf(","));
	    
	    		builder.append("]");
	    
	    		response.setContentType("application/json");
	    		
	    		smatchingResult=builder.toString();
	    		
	    
	    	}
	    
	    
	    	catch(NullPointerException e)
	    	{
	    		smatchingResult="[ \"No matches\"]";
	    	}
	    
	    	catch(Exception e)
	    	{
	    		smatchingResult="[ \"No matches\"]";
	    	}
	    	
	    	response.getWriter().write(smatchingResult);
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
