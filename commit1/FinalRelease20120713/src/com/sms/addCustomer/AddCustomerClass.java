package com.sms.addCustomer;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddCustomerClass
 */
@WebServlet("/AddCustomer")
public class AddCustomerClass extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddCustomerClass() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String strFirstName = request.getParameter("firstname");
		String strMiddleName = request.getParameter("middlename");
		String strLastName = request.getParameter("lastname");
		String strCustomerId = request.getParameter("customerid");
		String strAddress = request.getParameter("address");
		String strMobileNo = request.getParameter("mobileno");
		String strDateOfBirth=request.getParameter("dateofbirth");

		AddCustomerService serv = new AddCustomerService();

		boolean customerExist = serv.customerExist(strCustomerId);

		if (customerExist) {

				String strReply="Customer exists";
				response.getWriter().write(strReply);
	
				return;
		}

		else {

			boolean status = serv.addCustomer(strFirstName, strMiddleName, strLastName, strCustomerId, strAddress,"+94"+strMobileNo,strDateOfBirth);

			if (status) {
		
				String strReply="Customer added";
				response.getWriter().write(strReply);
		
				return;
			}

			else {
				
				return;

			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);

	}

}
