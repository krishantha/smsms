package com.sms.addCustomer;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.Statement;

import com.sms.database.DbConnect;

public class AddCustomerService {

	DbConnect conn;
	
	public AddCustomerService(){
		
		conn=new DbConnect();
		
	}
	
	public boolean addCustomer(String firstname, String middlename,
			String lastname, String customerid, String address, String mobileno,String dateofbirth) {
		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res = false;
		try {
			
			Statement s = con.createStatement();
			String queryString = "insert into customers(firstname,middlename,lastname,customerid,address,mobileno,dateofbirth) values('" + firstname + "','" + middlename + "','" + lastname + "','" + customerid + "','" + address + "','" + mobileno + "','" + dateofbirth + "');";
			System.out.println(queryString);
			int val = s.executeUpdate(queryString);

			if (val == 1) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			System.out.println("error in addCustomer");
			return res;
		}
		finally{
			
			conn.conClose(con);
		}
		return res;
	}

	public boolean customerExist(String customerid) {

		Connection con = (Connection)DbConnect.getConnection();// get connection
		boolean res = false;
		try {
			
			Statement s = con.createStatement();
			String queryString = "select * from customers where customerid='"+ customerid + "'";
			System.out.println(queryString);
			s.executeQuery(queryString);
			ResultSet rs = s.getResultSet();

			if (rs.next()) {
				res = true;
			}

			else {
				res = false;
			}

		} catch (Exception ex) {
			System.out.println("error in customerExist");
			return res;
		}
		finally{
			conn.conClose(con);
		}
		return res;
	}
}
