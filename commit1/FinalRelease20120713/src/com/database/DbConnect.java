package com.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DbConnect {
	
static Connection con=null;
//Creating a database connection
	public Connection getConnection()
	{
	
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/sms","root","pw");
			return con;
			
		}catch (SQLException e) {
			
			System.out.println("SQL Exception fired !");
			return null;
		}catch (ClassNotFoundException e) {
			
			System.out.println("Class Not Found Exception fired !");
			return null;
		}
		
		
	}
//Closing the database connection
    public void con_close(Connection dbConn) {
        try {
            dbConn.close();
        } catch (SQLException sQLException) {

        	System.out.println("SQL Exception fired !");
        }

    }

}
