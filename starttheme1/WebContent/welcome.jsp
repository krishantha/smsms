<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.8.0.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    
    
    
    


<script type="text/javascript">
	$(function()
	{
		$("#btnLogin").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnSign").button({icons: {primary: "ui-icon-circle-check"}});
	});
</script>

<script type="text/javascript">

function login()
{
	var valid=true;
	
	var user=$("#lgnUsername");
	var pw=$("#lgnPassword");

	
	document.getElementById("lblErr").innerHTML="";
	user.removeClass( "ui-state-error");
	pw.removeClass( "ui-state-error");
	document.getElementById('err').style.visibility='hidden';

	if (document.getElementById("lgnUsername").value == "") {
		
		user.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("lgnPassword").value == "") {
		
		pw.addClass( "ui-state-error" );
		valid = false;
	}

	if(valid==false)
	{
		document.getElementById("lblErr").innerHTML="Invalid Username or Password";
		$("#err").addClass("ui-state-error ui-corner-all");		
		document.getElementById('err').style.visibility='visible';
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "Login",
				data: {username : user.val() , password : pw.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="invalid"){
					
					document.getElementById("lblErr").innerHTML="Invalid Username or Password";
					$("#err").addClass("ui-state-error ui-corner-all");	
					document.getElementById('err').style.visibility='visible';
					
					
				}
				else{
					document.getElementById("lblErr").innerHTML="";
					document.getElementById('err').style.visibility='hidden';
					$("#err").addClass("ui-state-error ui-corner-all");	
					document.location='login.jsp?user='+response;

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="No response from server";
				$("#err").addClass("ui-state-error ui-corner-all");	
				document.getElementById('err').style.visibility='visible';
			});

		});
	}
}

</script>

<script type="text/javascript">

function signup()
{
	var valid=true;
	
	var name=$("#txtName");
	var company=$("#txtCompany");
	var username=$("#txtUsername");
	var password=$("#txtPassword");

	document.getElementById("lblErr").innerHTML="";
	name.removeClass( "ui-state-error");
	company.removeClass( "ui-state-error");
	username.removeClass( "ui-state-error");
	password.removeClass( "ui-state-error");
	document.getElementById('err').style.visibility='hidden';

	if (document.getElementById("txtUsername").value == "") {
		
		username.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtPassword").value == "") {
		
		password.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtName").value == "") {
		
		name.addClass( "ui-state-error" );
		valid = false;
	}
	if(valid==false)
	{
		document.getElementById("lblErr").innerHTML="Fill all the required fields";
		//$("#err").addClass("ui-state-error");	
		document.getElementById('err').style.visibility='visible';
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "UserEdit",
				data: {operation : "add" ,name : name.val() ,company : company.val() , username : username.val() , password : password.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="usernameExist"){
					
					document.getElementById("lblErr").innerHTML="Username not available";
					//$("#err").addClass("ui-state-error");	
					document.getElementById('err').style.visibility='visible';
					
					
				}
				else if(response=="userAdded"){
					
					document.getElementById("lblErr").innerHTML="Account created, please login to continue!";
					document.getElementById('err').style.visibility='visible';
					//$("#err").removeClass("ui-state-error");
					//$("#err").addClass("ui-state-highlight");
					document.reset();
					
					}
				else{
					
					document.location='welcome.jsp';

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="No response from server";
				//$("#err").addClass("ui-state-error");	
				document.getElementById('err').style.visibility='visible';
			});

		});
	}
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>
<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>
</head>
<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">



<div>
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div>
	
	
	<table cellpadding="1px">
	<tr>
	<td style="width: 900px;">
	<div style="padding: 20px; margin-bottom: 30px; margin-right: 10px;">
	<img src="images/welcome.jpg" alt="image not found" width="600px"/>
	</div>
	</td>
	<td>
	
	<div class="ui-state-highlight ui-corner-all" style="padding: 20px; margin-bottom: 30px; margin-right: 30px;">
	<table cellpadding="1px">
		
		<tr>
			<td>Username</td><td style="width: 5px;"></td><td><input type="text" id="lgnUsername" name="lgnUsername" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td>
		</tr>
		<tr>
			<td>Password<td/><td style="width: 5px;"><input type="password" id="lgnPassword" name="lgnPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td>
		</tr>
		<tr>
		<td  colspan = "3">
		<div id="err" class="ui-widget" style="padding: 0 .7em; visibility: hidden;">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Alert:</strong> <label id="lblErr" ></label></p>
		</div>
		</td>
		</tr>
		<tr>
			<td></td><td style="width: 5px;"></td><td><button type="button" id="btnLogin" onclick="login();" value="edit" style="float: right; margin-bottom: 20px;">Log In</button></td>
		</tr>
		<tr>
			<td></td><td style="width: 5px;"></td><td><a href=ForgotPassword.jsp style="float: right;">Forgot password</a></td><td></td>
		</tr>
		<tr>
			<td></td><td style="width: 5px;"></td><td><a href=SignUp.jsp style="float: right;">Sign up</a></td><td></td>
		</tr>
	</table>
	
	<table cellpadding="1px" height="200px" style="margin-top: 30px">
		
		
	</table>
	</div>
	</td>
	
	</tr>
	</table>
	
	<table cellpadding="1px">
	<tr>
	<td style="width: 865px;"></td>
	<td>
	
	
	</td>
	
	</tr>
	</table>
	
	
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; izone developers</div>
	
</div>
</body>
</html>