<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.8.0.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
    
    


<script type="text/javascript">
	$(function()
	{
		$("#btnLogin").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnSign").button({icons: {primary: "ui-icon-circle-check"}});
	});
</script>



<script type="text/javascript">

function recover()
{
	var valid=true;
	
	var name=$("#txtName");
	
	
	document.getElementById("lblErr").innerHTML="";
	name.removeClass( "ui-state-error");
	
	document.getElementById('lblErr').style.visibility='hidden';
	document.getElementById('err').style.visibility='hidden';
	
	if (document.getElementById("txtName").value == "") {
		
		name.addClass( "ui-state-error ui-corner-all" );
		valid = false;
	}
	
	if(valid==false)
	{
		document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Please enter username first.";
		$("#err").addClass("ui-state-error ui-corner-all");	
		document.getElementById('lblErr').style.visibility='visible';
		document.getElementById('err').style.visibility='visible';
	}
	else if(valid==true)
	{
		$(function(){ //jquery function
					
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "Recovery",
				data: {operation : "add" , username : name.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="invalidUser"){
					
					document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Username not available.";
					$("#err").addClass("ui-state-error ui-corner-all");	
					document.getElementById('lblErr').style.visibility='visible';
					document.getElementById('err').style.visibility='visible';
					
					
				}
				else if(response=="validated"){
					
					document.getElementById("lblErr").innerHTML="You will shortly receive a SMS with your password.";
					document.getElementById('lblErr').style.visibility='visible';
					document.getElementById('err').style.visibility='visible';
					$("#err").removeClass("ui-state-error ui-corner-all");
					$("#err").addClass("ui-state-highlight ui-corner-all");
					document.getElementById("txtName").value = "";
					
					}
				else{
					
					document.location='welcome.jsp';

				}
				});		
		});
	}
}
</script>



<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>

</head>
<body>



<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp;SMS management system</div>
	
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center">

		
	
	<div class="ui-state-highlight ui-corner-all" style="width: 300px; margin-bottom: 20px;">
	
	<br/>
	
	<table>
		
		
		<tr>
			<td>Username</td><td style="width: 5px;"></td><td><input type="text" id="txtName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td>
		</tr>
		<tr>
		<td  colspan = "3">
		<div id="err" class="ui-widget" style="padding: 0 .7em; visibility: hidden;">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Alert:</strong> <label id="lblErr" ></label></p>
		</div>
		</td>
		</tr>
		<tr><td></td><td></td>
			<td><button type="button" id="btnLogin" onclick="return document.location='logout.jsp';" value="edit" style="float: right; margin-bottom: 20px;">Log In</button><button type="button" id="btnSign" value="edit" onclick="recover();" style="float: right; margin-bottom: 20px;">Submit</button></td>
		</tr>
		
	</table>
	</div>
	
	
	
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; izone developers</div>
	
</div>
</body>
</html>