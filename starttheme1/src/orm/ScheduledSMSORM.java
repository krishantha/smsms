package orm;
import java.util.List;
import org.springframework.orm.hibernate3.HibernateTemplate;
import pojo.ScheduledMessage;


public class ScheduledSMSORM {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final ScheduledMessage iSMS) {
		hibernateTemplate.save(iSMS);
		
	}
	
	public void update(final ScheduledMessage iSMS){
		
		hibernateTemplate.update(iSMS);		
	}

	public List<ScheduledMessage> fetchALLData() {
		return hibernateTemplate.loadAll(ScheduledMessage.class);
	}
	
	public void delete(final ScheduledMessage iSMS){
		
		hibernateTemplate.delete(iSMS);		
	}
	
}
