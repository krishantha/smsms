package orm;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import pojo.GroupCustomer;



public class GroupCustomerORM {
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final GroupCustomer groups) {
		hibernateTemplate.save(groups);
	}
	
	
	public void update(final GroupCustomer groups){
		
		hibernateTemplate.update(groups);		
	}

	public List<GroupCustomer> fetchALLData() {
		return hibernateTemplate.loadAll(GroupCustomer.class);
	}
	
		public void delete(final GroupCustomer groups){
		
		hibernateTemplate.delete(groups);		
	}
	
}