package servlet.edit;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import orm.InstantSMSORM;
import orm.UserORM;
import pojo.Customer;
import pojo.InstantMessage;
import pojo.User;


/**
 * Servlet implementation class CustomerEdit
 */
@WebServlet("/Recovery")
public class Recovery extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recovery() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
		String operation;
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		InstantSMSORM instantORM=(InstantSMSORM)context.getBean("instantORMBean");
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		InstantMessage instant=new InstantMessage();
		List <User> userList = userORM.fetchALLData();
		
		String username=request.getParameter("username");
		String mobile="";
		String password="";
		long userid=-1;
		boolean res=false;
		
		for(User user:userList){
			
			if(user.getUsername().equalsIgnoreCase(username)){
				
				res=true;
				mobile=user.getMobile();
				password=user.getPassword();
				userid=user.getUserId();
			}
			
		}
		
		if(res){
		
		  Calendar currentDate = Calendar.getInstance();
		  SimpleDateFormat dformatter= new SimpleDateFormat("yy/MM/dd");
		  SimpleDateFormat tformatter= new SimpleDateFormat("HH:mm:ss");
		  String dateNow = dformatter.format(currentDate.getTime());
		  String timeNow = tformatter.format(currentDate.getTime());
		  
		operation=request.getParameter("operation").toString();
		
		
		System.out.println(operation);
		
		
		
		
		if(operation.contains("add"))
		{
					
			instant.setMobileNo(mobile);
			instant.setMessagecontent("Your SMSMS password is '"+password+"'");
			instant.setDatecreated(dateNow);
			instant.setTimecreated(timeNow);
			instant.setStatus("sending");
			instant.setUserId(userid);
			
						
			instantORM.saveOrUpdate(instant);
			
			response.getWriter().write("validated");
		}
		
		
		
		
		}
		
		else{
			
			
			response.getWriter().write("invalidUser");
		}
		}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
