package servlet.edit;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import orm.GroupORM;
import orm.UserORM;
import pojo.Customer;
import pojo.Groups;
import pojo.User;

/**
 * Servlet implementation class GroupEdit
 */
@WebServlet("/GroupEdit")
public class GroupEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GroupEdit() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String operation;
		
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		GroupORM groupORM=(GroupORM)context.getBean("groupORMBean");
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		List <User> userList = userORM.fetchALLData();
		long userId=(Long) request.getSession().getAttribute("userid");
		
		
		Groups group=new Groups();
		operation=request.getParameter("oper").toString();
		
		if(userId!=-1){
		if(operation.contains("edit"))
		{
			long id=Long.parseLong(request.getParameter("id"));
			group.setGroupId(id);
			group.setGroupName(request.getParameter("name"));
			group.setDescription(request.getParameter("description"));
			group.setUserId(userId);
			
			groupORM.update(group);			
		}
		
		else if(operation.contains("add"))
		{			
			group.setGroupName(request.getParameter("name"));
			group.setDescription(request.getParameter("description"));
			group.setUserId(userId);
			
			groupORM.saveOrUpdate(group);
		}
		
		else if(operation.contains("del"))
		{	
			long id=Long.parseLong(request.getParameter("id"));
			group.setGroupId(id);
			group.setGroupName(request.getParameter("name"));
			group.setDescription(request.getParameter("description"));
			group.setUserId(userId);
			
			
			groupORM.delete(group);
		}
		
		response.setStatus(200);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
