package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import pojo.Customer;


/**
 * Servlet implementation class CustomerServ
 */
@WebServlet(name = "Custo", urlPatterns = { "/Custo" })
public class CustomerServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		CustomerORM customerORM=(CustomerORM)context.getBean("customerORMBean");
		List <Customer> customerList = customerORM.fetchALLData();
		
	
				
		int totalpage=1;
		
		
		if(customerList.size()>10){
			totalpage = (customerList.size()/10);
		}
		
		
		
		System.out.println("Total Pa: "+totalpage+"\npages: ");
		String resultData = "{total: \""+totalpage+"\",page: \"1\",records: \""+(1)+"\",rows: [";
		
		
		for(Customer customer : customerList) {
			String name=customer.getCustomerName();
			String address=customer.getAddress();
			String mobile=customer.getMobile();
			String email=customer.geteMail();
			String dob=customer.getDateOfBirth();
			long cid=customer.getCustomerId();
			
					//{ID:"", Message:"", Date,Time,status}
			resultData = resultData +
					"{\"name\":\""+name+"\", \"id\":\""+cid+"\", "+
					"\"address\": \""+address+"\", "+
					"\"mobile\": \""+mobile+"\", "+
					"\"email\": \""+email+"\", "+
					"\"dob\": \""+dob+"\"}";
			System.out.println(resultData);
			
			
				resultData = resultData + ",";
			
		}
		
		resultData = resultData + "]}";
		System.out.println(resultData);
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().write(resultData);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
