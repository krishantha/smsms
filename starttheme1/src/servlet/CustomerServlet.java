package servlet;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import orm.CustomerORM;
import pojo.Customer;

public class CustomerServlet extends HttpServlet {
	
	
	
	
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("get method");
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		CustomerORM customerORM=(CustomerORM)context.getBean("customerORMBean");
		
		
		
		Customer customer=new Customer();
		customer.setCustomerName(request.getParameter("cName"));
		customer.setAddress(request.getParameter("cAdd"));
		customer.setMobile(request.getParameter("cMob"));
		customer.setUserId(1233);
		
		customerORM.saveOrUpdate(customer);
	
		List <Customer> customerList = customerORM.fetchALLData();
		
		request.setAttribute("customerData", customerList);
		RequestDispatcher rd=request.getRequestDispatcher("result.jsp");
		rd.forward(request, response);		
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
