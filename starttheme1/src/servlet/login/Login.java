package servlet.login;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.UserORM;
import pojo.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		
		List <User> userList = userORM.fetchALLData();
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		long userId=-1;
		
		boolean userExist=false;
		
		for(User ouser : userList){
			
			if ((ouser.getUsername().equals(username)) && (ouser.getPassword().equals(password))){
				
				userExist=true;
				userId=ouser.getUserId();
			}
		}
		if(userExist){
			
			System.out.println("valid");
			request.getSession().setAttribute("userid", userId);
			response.getWriter().write(username);
		}
		
		else
		{
			System.out.println("invalid");
			response.getWriter().write("invalid");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
