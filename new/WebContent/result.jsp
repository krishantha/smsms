<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="pojo.Customer" %>
    <%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link rel="stylesheet" href="css/demos.css"> 

	


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div id="wrapper" align="center" style="height: 100%;">
		<div id="content_box" style="width: 400px;">

			<div class="ui-widget">
				<div class="ui-widget-header">Customers</div>
				
				<div>
					<div class="ui-widget-content">
						<div style="padding: 10px 10px 10px 10px;">

<table border="1" width="200" height="180">
<tr><th>Name</th><th>Address</th><th>Mobile</th></tr>
<% 

		List<Customer> customerList=(List<Customer>)request.getAttribute("customerData");
		for(Customer customer : customerList) {
		String name=customer.getCustomerName();
		String address=customer.getAddress();
		String mobile=customer.getMobile();
		
%>
<tr><td><%=name%></td><td><%=address%></td><td><%=mobile%></td></tr>
<%
		}
%>
</table><br>
<a href="customer.jsp">Insert Another Record</a>

						</div>
					
												<form>
							<table width="200" height="180">
							<tr><td>Name : </td><td><input type="text" name="cName" id="nameId" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"></td></tr>
							<tr><td>Address : </td><td><input type="text" name="cAdd" id="addId" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"></td></tr>
							<tr><td>mobile : </td><td><input type="text" name="cMob" id="mobId" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"></td></tr>
							<tr><td><button class="ui-state-default" type="submit" id="submitId" >Add</button></td></tr>
							
							
							
							</table>
							
							
							
							
							</form>
					
				</div>
			</div>
		</div>
	</div>
	
</div>

</body>
</html>