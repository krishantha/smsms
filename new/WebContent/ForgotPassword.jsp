<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
    
    


<script type="text/javascript">
	$(function()
	{
		$("#btnLogin").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnSign").button({icons: {primary: "ui-icon-circle-check"}});
	});
</script>

<script type="text/javascript">

function login()
{
	var valid=true;
	
	var user=$("#lgnUsername");
	var pw=$("#lgnPassword");

	
	document.getElementById("lblErr").innerHTML="";
	user.removeClass( "ui-state-error");
	pw.removeClass( "ui-state-error");
	document.getElementById('lblErr').style.visibility='hidden';

	if (document.getElementById("lgnUsername").value == "") {
		
		user.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("lgnPassword").value == "") {
		
		pw.addClass( "ui-state-error" );
		valid = false;
	}

	if(valid==false)
	{
		document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Invalid Username or Password";
		$("#err").addClass("ui-state-error");	
		document.getElementById('lblErr').style.visibility='visible';
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "Login",
				data: {username : user.val() , password : pw.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="invalid"){
					
					document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Invalid Username or Password";
					$("#err").addClass("ui-state-error");	
					document.getElementById('lblErr').style.visibility='visible';
					
					
				}
				else{
					document.getElementById("lblErr").innerHTML="";
					document.getElementById('lblErr').style.visibility='hidden';
					$("#err").removeClass("ui-state-error");
					document.location='login.jsp?user='+response;

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;No response from server";
				$("#err").addClass("ui-state-error");	
				document.getElementById('lblErr').style.visibility='visible';
			});

		});
	}
}

</script>

<script type="text/javascript">

function signup()
{
	var valid=true;
	
	var name=$("#txtName");
	var company=$("#txtCompany");
	var username=$("#txtUsername");
	var password=$("#txtPassword");

	document.getElementById("lblErr").innerHTML="";
	name.removeClass( "ui-state-error");
	company.removeClass( "ui-state-error");
	username.removeClass( "ui-state-error");
	password.removeClass( "ui-state-error");
	document.getElementById('lblErr').style.visibility='hidden';

	if (document.getElementById("txtUsername").value == "") {
		
		username.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtPassword").value == "") {
		
		password.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtName").value == "") {
		
		name.addClass( "ui-state-error" );
		valid = false;
	}
	if(valid==false)
	{
		document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Fill all the required fields";
		$("#err").addClass("ui-state-error");	
		document.getElementById('lblErr').style.visibility='visible';
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "UserEdit",
				data: {operation : "add" ,name : name.val() ,company : company.val() , username : username.val() , password : password.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="usernameExist"){
					
					document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Username not available";
					$("#err").addClass("ui-state-error");	
					document.getElementById('lblErr').style.visibility='visible';
					
					
				}
				else if(response=="userAdded"){
					
					document.getElementById("lblErr").innerHTML="Account created, please login to continue!";
					document.getElementById('lblErr').style.visibility='visible';
					$("#err").removeClass("ui-state-error");
					$("#err").addClass("ui-state-highlight");
					document.reset();
					
					}
				else{
					
					document.location='welcome.jsp';

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;No response from server";
				$("#err").addClass("ui-state-error");	
				document.getElementById('lblErr').style.visibility='visible';
			});

		});
	}
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>

</head>
<body>



<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp;SMS management system</div>
	<div id="err">
		<label id="lblErr" ></label>
	</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center">

		
	
	<div class="ui-state-highlight ui-corner-all" style="width: 300px; margin-bottom: 20px;">
	
	<br/>
	
	<table>
		
		
		<tr>
			<td>Username</td><td style="width: 5px;"></td><td><input type="text" id="txtName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td>
		</tr>
		
		<tr>
			<td style="width: 50px;"></td><td style="width: 5px;"></td><td><button type="button" id="btnSign" value="edit" style="float: right; margin-bottom: 20px;">Submit</button></td>
		</tr>
	</table>
	</div>
	
	
	
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; izone developers</div>
	
</div>
</body>
</html>