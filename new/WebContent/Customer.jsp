<%@page session="true"%>
<%
if(session.getAttribute("user")==null)
{
response.sendRedirect("welcome.jsp");
}
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
    
    
<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(value, colname) {
			if (value.length != 10) 
			   return [false,"Mobile no is not valid"];
			else 
			   return [true,""];
		}

				    	
		    	 jQuery("#results").jqGrid({
				       	url:'EditCustomer',
				    	datatype: "json",
				       	colNames:['ID','Name', 'Mobile No', 'E-mail','Date Of Birth','Address'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:50,search:true,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"name",search:true,index:"name",width:150,search:true,editable:true,editrules: { edithidden: true,required: true }},
				              {name:"mobile",index:"mobile",width:80,editable:true,search:true,editrules: {custom:true,custom_func:mobilecheck,required: true,number:true},editoptions: { size : 10,maxlength:10 }},
				              {name:"email",index:"email",width:140,editable:true ,search:true,editrules:{email:true}},
				              {name:"dob",index:"dob",width:80,editable:true,search:true,edittype: 'text',
					       		    editoptions: {
						       		      size: 10, maxlengh: 10,
						       		      dataInit: function(element) {
						       		        $(element).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',maxDate: "-10Y"});
						       		      }
						       		    }},
				              {name:"address",index:"address",search:true,width:200,editable:true,editoptions:{maxlength:160,rows:"4",cols:"30"},edittype: 'textarea'}			       		
				       			
				       	],
				       	rowNum:10,
				       	height:300,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: true,
				        multiselect:false,
				        sortorder: "asc",
				        pager:'#pager',
				        editurl:"CustomerEdit",
				        caption:"Customers",
				        loadError: function(xhr,status,error){alert(status+" "+error);}
		    	 });
		    	 
		    	 //jQuery("#results").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		    	 
		    	 jQuery("#results").jqGrid('navGrid',"#pager",{refresh:false}, //options
		   			  {height:250,reloadAfterSubmit:true}, // edit options
		   			  {height:250,reloadAfterSubmit:true}, // add options
		   			  {reloadAfterSubmit:false}, // del options
		   			  {} // search options
		   			  );
		    	
	
});

</script>

<script type="text/javascript">
	$(function(){

		$( "#repeat" ).buttonset();
		$("#btnLogout").button({icons: {primary: "ui-icon-bookmark"}});
		
		});
</script>




<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Customers</title>

</head >
<body onload="getuser();">
<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center" align="center">

	<table>
		<tr>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all"> 
				<span id="repeat"> 
					<input type="radio" id="repeat0" name="repeat" onclick="return document.location='InstantSMS.jsp';" /><label for="repeat0"><a href=InstantSMS.jsp>Send SMS</a></label> 
					<input type="radio" id="repeat1" name="repeat" onclick="return document.location='ScheduledSMS.jsp';"/><label for="repeat1"><a href=ScheduledSMS.jsp>Schedule SMS</a></label> 
					<input type="radio" id="repeat2" name="repeat" checked="checked" /><label for="repeat2">Manage Customer</label> 
					<input type="radio" id="repeat3" name="repeat" onclick="return document.location='Group.jsp';"/><label for="repeat3"><a href=Group.jsp>Manage Groups</a></label>
					<input type="radio" id="repeat4" name="repeat" onclick="return document.location='ChangePassword.jsp';"/><label for="repeat4"><a href=ChangePassword.jsp>Settings</a></label>
				</span> 
			</span> 
			</td>
			<td style="width: 5px;"></td>
			<td>
			<span id="toolbar" class="ui-state-highlight ui-corner-all">
			<%=session.getAttribute("user") %> <button class="ui-state-default" type="button" id="btnLogout" onclick="return document.location='logout.jsp';" value="edit" >logout</button>
			</span>
			</td>
			<td></td>
		</tr>
	</table>
	
	<br/><br/>
	
	
	<div style="padding-bottom: 50px;" id="searchresults">
		
	<table id="results" width="700"></table>
	<div id="pager"> </div>	
		
	
	</div>
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; izone developers</div>
	
</div>
</body>
</html>