package servlet.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import orm.ScheduledSMSORM;
import pojo.ScheduledMessage;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class EditCustomerClass
 */
@WebServlet("/ViewScheduledSMS")
public class ViewScheduledSMS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewScheduledSMS() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		long userId=(Long) request.getSession().getAttribute("userid");
	
			
			
		//int page=Integer.parseInt(request.getParameter("page"));
		//int limit=Integer.parseInt(request.getParameter("limit"));
		//int sidx=Integer.parseInt(request.getParameter("sidx"));
		//String sord=request.getParameter("sord");
		//int total_pages=0;
		
		
		JSONObject responsedata=new JSONObject();
		
		JSONArray cellarray=new net.sf.json.JSONArray();
	    		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		ScheduledSMSORM ssmsORM=(ScheduledSMSORM)context.getBean("scheduledORMBean");
		List <ScheduledMessage> shsmsList =ssmsORM.fetchALLData();
		
		/*int count=customerList.size();
		
		if(count>0){
			
			total_pages=count/limit;
						
		}
		
		if(page>total_pages){
			
			page=total_pages;
		}
		*/
		
		try 
		{
			
			
									
		 	request.getSession().setAttribute("scheduledList",shsmsList);
		 	
	        //responsedata.put("total",totalrow);
	        //responsedata.put("page",cpage);
	        //responsedata.put("records",icount);

	        JSONArray cell=new net.sf.json.JSONArray();
	        JSONObject cellobj=new net.sf.json.JSONObject();
	        

			
	        
			if(!shsmsList.isEmpty())
			{
			
				for(ScheduledMessage scheduled:shsmsList)
				{
					
					
					
					long userid=scheduled.getUserId();
					
					if(userid==userId){
						
					
					cellobj.put("id", scheduled.getMessageId());
					cell.add(scheduled.getMessageId());
					cell.add(scheduled.getMobileNo());
					cell.add(scheduled.getMessagecontent());
					cell.add(scheduled.getDatescheduled());
					cell.add(scheduled.getTimescheduled());
					cell.add(scheduled.getStatus());
					
					
					
					
					cellobj.put("cell", cell);
					cell.clear();
					cellarray.add(cellobj);
					}
				}
				
				responsedata.put("rows",cellarray);

			}
		}
				
	
		catch (Exception e)
		{
			
		}
	
		
		response.getWriter().write(responsedata.toString());
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
	

}
