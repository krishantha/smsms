package servlet.edit;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.GroupCustomerORM;
import orm.UserORM;
import pojo.GroupCustomer;
import pojo.User;

/**
 * Servlet implementation class GCEdit
 */
@WebServlet("/GCEdit")
public class GCEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GCEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		System.out.println("request came");
		
		String operation;
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		GroupCustomerORM gcORM=(GroupCustomerORM)context.getBean("grpcusORMBean");
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		GroupCustomer gc;
		List <User> userList = userORM.fetchALLData();
		
		long userId=(Long) request.getSession().getAttribute("userid");
		
		
		operation=request.getParameter("operation");
		
		
		System.out.println(operation);
		
		
		if(userId!=-1){
		
				
		if(operation.contains("add"))
		{
			
			long gid=Long.parseLong(request.getParameter("gid"));
			long cid=Long.parseLong(request.getParameter("cid"));
						
			gc=new GroupCustomer();
			gc.setCustomerId(cid);
			gc.setGroupId(gid);
			
			gcORM.saveOrUpdate(gc);
			
			
		}
		
		else if(operation.contains("del"))
		{
			
			long gid=Long.parseLong(request.getParameter("gid"));
			long cid=Long.parseLong(request.getParameter("cid"));
			
			
			gc=new GroupCustomer();
			
			gc.setCustomerId(cid);
			gc.setGroupId(gid);
			gcORM.delete(gc);
			
			
			
		}
		
		response.setStatus(200);
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
