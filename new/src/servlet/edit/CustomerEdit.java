package servlet.edit;

import java.io.IOException;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import orm.UserORM;
import pojo.Customer;
import pojo.User;


/**
 * Servlet implementation class CustomerEdit
 */
@WebServlet("/CustomerEdit")
public class CustomerEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String operation;
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		CustomerORM customerORM=(CustomerORM)context.getBean("customerORMBean");
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		Customer customer=new Customer();
		List <User> userList = userORM.fetchALLData();
		
		long userId=(Long) request.getSession().getAttribute("userid");
		
		
		operation=request.getParameter("oper").toString();
		
		
		System.out.println(operation);
		
		
		if(userId!=-1){
		
		if(operation.contains("edit"))
		{
			
			long id=Long.parseLong(request.getParameter("id"));
			System.out.println(id);
			customer.setCustomerId(id);
			customer.setCustomerName(request.getParameter("name"));
			customer.setDateOfBirth(request.getParameter("dob"));
			customer.setMobile(request.getParameter("mobile"));
			customer.seteMail(request.getParameter("email"));
			customer.setAddress(request.getParameter("address"));
			customer.setUserId(userId);
			
			customerORM.update(customer);			
		}
		
		else if(operation.contains("add"))
		{
						
			customer.setCustomerName(request.getParameter("name"));
			customer.setDateOfBirth(request.getParameter("dob"));
			customer.setMobile(request.getParameter("mobile"));
			customer.seteMail(request.getParameter("email"));
			customer.setAddress(request.getParameter("address"));
			customer.setUserId(userId);
			
			customerORM.saveOrUpdate(customer);
		}
		
		else if(operation.contains("del"))
		{
			long id=Long.parseLong(request.getParameter("id"));
			
			System.out.println(id);
			
			customer.setCustomerId(id);
			customer.setCustomerName(request.getParameter("name"));
			customer.setDateOfBirth(request.getParameter("dob"));
			customer.setMobile(request.getParameter("mobile"));
			customer.seteMail(request.getParameter("email"));
			customer.setAddress(request.getParameter("address"));
			customer.setUserId(userId);
			
			customerORM.delete(customer);
			
			
		}
		
		response.setStatus(200);
		
		}
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
