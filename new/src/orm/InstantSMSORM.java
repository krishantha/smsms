package orm;
import java.util.List;
import org.springframework.orm.hibernate3.HibernateTemplate;
import pojo.InstantMessage;


public class InstantSMSORM {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final InstantMessage iSMS) {
		hibernateTemplate.save(iSMS);
		
	}
	
	public void update(final InstantMessage iSMS){
		
		hibernateTemplate.update(iSMS);		
	}

	public List<InstantMessage> fetchALLData() {
		return hibernateTemplate.loadAll(InstantMessage.class);
	}
	
	public void delete(final InstantMessage iSMS){
		
		hibernateTemplate.delete(iSMS);		
	}
	
}
