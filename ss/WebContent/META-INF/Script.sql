--<ScriptOptions statementTerminator=";"/>

ALTER TABLE user DROP PRIMARY KEY;

ALTER TABLE customer DROP PRIMARY KEY;

DROP TABLE user;

DROP TABLE customer;

CREATE TABLE user (
	USERID BIGINT NOT NULL auto_increment,
	NAME VARCHAR(255),
	COMPANY VARCHAR(255),
	USERNAME VARCHAR(255),
	PASSWORD VARCHAR(255),
	MOBILE VARCHAR(255),
	PRIMARY KEY (USERID)

) ENGINE=InnoDB;

alter table user add MOBILE VARCHAR(255);

CREATE TABLE customer (
	CUSTOMERID BIGINT NOT NULL auto_increment,
	CUSTOMERNAME VARCHAR(255),
	MOBILE VARCHAR(255),
	ADDRESS VARCHAR(255),
	USERID BIGINT,
	DATEOFBIRTH VARCHAR(255),
	EMAIL VARCHAR(255),
	PRIMARY KEY (CUSTOMERID),
	constraint fk_user foreign key(USERID) references User(USERID)
) ENGINE=InnoDB;


drop table groups;

create table groups(
	groupid bigint not null auto_increment,
	groupname varchar(255),
	description varchar(255),
	userid bigint,
	primary key(groupid),
	constraint fk_usr foreign key(userid) references User(userid)
)


drop table instantmessage;

CREATE TABLE `instantmessage` (
  `messageid` bigint NOT NULL AUTO_INCREMENT,
  `mobileno` varchar(50) NOT NULL,
  `messagecontent` varchar(500) DEFAULT NULL,
  `datecreated` date DEFAULT NULL,
  `timecreated` time DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  userid bigint,
  PRIMARY KEY (`messageid`),
  constraint fk_instantusr foreign key(userid) references User(userid)
)

drop table scheduledmessage;

CREATE TABLE `scheduledmessage` (
  `messageid` bigint NOT NULL AUTO_INCREMENT,
  `mobileno` varchar(50) NOT NULL,
  `messagecontent` varchar(500) DEFAULT NULL,
  `datescheduled` date DEFAULT NULL,
  `timescheduled` time DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  userid bigint,
  PRIMARY KEY (`messageid`),
  constraint fk_shedusr foreign key(userid) references User(userid)
)




