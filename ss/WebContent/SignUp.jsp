<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:1024px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
    
    


<script type="text/javascript">
	$(function()
	{
		$("#btnLogin").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnSign").button({icons: {primary: "ui-icon-circle-check"}});
		
	});
</script>

<script type="text/javascript">

function login()
{
	var valid=true;
	
	var user=$("#lgnUsername");
	var pw=$("#lgnPassword");

	
	document.getElementById("lblErr").innerHTML="";
	user.removeClass( "ui-state-error");
	pw.removeClass( "ui-state-error");
	document.getElementById('lblErr').style.visibility='hidden';

	if (document.getElementById("lgnUsername").value == "") {
		
		user.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("lgnPassword").value == "") {
		
		pw.addClass( "ui-state-error" );
		valid = false;
	}

	if(valid==false)
	{
		document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Invalid Username or Password";
		$("#err").addClass("ui-state-error");	
		document.getElementById('lblErr').style.visibility='visible';
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "Login",
				data: {username : user.val() , password : pw.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="invalid"){
					
					document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Invalid Username or Password";
					$("#err").addClass("ui-state-error");	
					document.getElementById('lblErr').style.visibility='visible';
					
					
				}
				else{
					document.getElementById("lblErr").innerHTML="";
					document.getElementById('lblErr').style.visibility='hidden';
					$("#err").removeClass("ui-state-error");
					document.location='login.jsp?user='+response;

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;No response from server";
				$("#err").addClass("ui-state-error");	
				document.getElementById('lblErr').style.visibility='visible';
			});

		});
	}
}

</script>

<script type="text/javascript">

function signup()
{
	var valid=true;
	var xmobile=true;
	
	var name=$("#txtName");
	var company=$("#txtCompany");
	var username=$("#txtUsername");
	var mobile=$("#txtMobile");
	var password=$("#txtPassword");

	document.getElementById("lblErr").innerHTML="";
	name.removeClass( "ui-state-error");
	company.removeClass( "ui-state-error");
	username.removeClass( "ui-state-error");
	mobile.removeClass( "ui-state-error");
	password.removeClass( "ui-state-error");
	document.getElementById('lblErr').style.visibility='hidden';

	if (document.getElementById("txtUsername").value == "") {
		
		username.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtMobile").value == "") {
		
		mobile.addClass( "ui-state-error" );
		valid = false;
	}
	
	if (document.getElementById("txtPassword").value == "") {
		
		password.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtName").value == "") {
		
		name.addClass( "ui-state-error" );
		valid = false;
	}
	if (!validmobile(document.getElementById("txtMobile").value)) {
		
		mobile.addClass( "ui-state-error" );
		valid = false;
		xmobile = false;
	}
	if(valid==false)
	{
		if(xmobile==false){

			document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Invalid mobile Ex: 0777123123 or +94777132123";
			$("#err").addClass("ui-state-error");	
			document.getElementById('lblErr').style.visibility='visible';
		}
		else{
			document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Fill all the required fields";
			$("#err").addClass("ui-state-error");	
			document.getElementById('lblErr').style.visibility='visible';
		}
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "UserEdit",
				data: {operation : "add" ,name : name.val() ,company : company.val() , username : username.val() , password : password.val(), mobile : mobile.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="usernameExist"){
					
					document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;Username not available";
					$("#err").addClass("ui-state-error");	
					document.getElementById('lblErr').style.visibility='visible';
					
					
				}
				else if(response=="userAdded"){
					
					document.getElementById("lblErr").innerHTML="Account created, please login to continue!";
					document.getElementById('lblErr').style.visibility='visible';
					$("#err").removeClass("ui-state-error");
					$("#err").addClass("ui-state-highlight");
					document.getElementById("txtUsername").value = "";
					document.getElementById("txtMobile").value = "";
					document.getElementById("txtPassword").value = "";
					document.getElementById("txtName").value = "";
					document.getElementById("txtCompany").value = "";
					
					}
				else{
					
					document.location='welcome.jsp';

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;No response from server";
				$("#err").addClass("ui-state-error");	
				document.getElementById('lblErr').style.visibility='visible';
			});

		});
	}

	function validmobile(mobile){

		
		var phoneRE=/\+94\d{9}$/;
		var phoneRX=/\d{10}$/;
		
		if(mobile.length==10 && mobile.match(phoneRX)){
			
			return true;
		}
		else if(mobile.length==12 && mobile.match(phoneRE)){
			return true;
		}
		else{
			return false;
		}
					
	}
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sign Up</title>

</head>
<body>



<div>
	<div class="ui-widget-header">&nbsp;&nbsp;SMS management system</div>

	
	<div class="ui-widget-content" align="center">
	
	

		
	
	<div class="ui-state-highlight ui-corner-all" style="width:400px;padding: 30px; margin-top: 30px; margin-bottom: 30px;" align="center">
	<table>
		
		<tr>
			<td><h1>Sign Up</h1></td>
		</tr>
		<tr>
			<td>Full Name</td><td style="width: 5px;"></td><td><input type="text" id="txtName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Organization</td><td style="width: 5px;"></td><td><input type="text" id="txtCompany" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Username</td><td style="width: 5px;"></td><td><input type="text" id="txtUsername" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Password</td><td style="width: 5px;"></td><td><input type="password" id="txtPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Mobile No</td><td style="width: 5px;"></td><td><input type="text" id="txtMobile" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
		<td></td><td style="width: 5px;"></td>
		<td>
		<div id="err">
		<label id="lblErr" ></label>
		</div>
		</td>
		</tr>
		<tr>
			<td><button type="button" id="btnLogin" onclick="return document.location='logout.jsp';" value="edit" style="float: right; margin-bottom: 20px;">Log In</button></td><td style="width: 5px;"></td><td><button type="button" id="btnSign" onclick="signup();" value="edit" style="float: right; margin-bottom: 20px;">Sign Up</button></td><td></td>
		</tr>
	</table>
	</div>
	
	
	
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; izone developers</div>
	

</body>
</html>