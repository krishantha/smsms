package servlet.edit;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import orm.InstantSMSORM;
import orm.UserORM;
import pojo.Customer;
import pojo.InstantMessage;
import pojo.User;


/**
 * Servlet implementation class CustomerEdit
 */
@WebServlet("/InstantSMSEdit")
public class InstantSMSEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InstantSMSEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String operation;
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		InstantSMSORM instantORM=(InstantSMSORM)context.getBean("instantORMBean");
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		InstantMessage instant=new InstantMessage();
		List <User> userList = userORM.fetchALLData();
		
		long userId=(Long) request.getSession().getAttribute("userid");
		
		  Calendar currentDate = Calendar.getInstance();
		  SimpleDateFormat dformatter= new SimpleDateFormat("yy/MM/dd");
		  SimpleDateFormat tformatter= new SimpleDateFormat("HH:mm:ss");
		  String dateNow = dformatter.format(currentDate.getTime());
		  String timeNow = tformatter.format(currentDate.getTime());
		
		operation=request.getParameter("oper").toString();
		
		
		System.out.println(operation);
		
		
		if(userId!=-1){
		
		if(operation.contains("add"))
		{
			System.out.println(request.getParameter("mobile"));			
			instant.setMobileNo(request.getParameter("mobile"));
			instant.setMessagecontent(request.getParameter("content"));
			instant.setDatecreated(dateNow);
			instant.setTimecreated(timeNow);
			instant.setStatus("sending");
			instant.setUserId(userId);
						
			instantORM.saveOrUpdate(instant);
		}
		
		else if(operation.contains("del"))
		{
			long id=Long.parseLong(request.getParameter("id"));
			
			System.out.println(id);
			
			instant.setMessageId(id);
			instant.setMobileNo(request.getParameter("mobile"));
			instant.setMessagecontent(request.getParameter("content"));
			instant.setDatecreated(request.getParameter("date"));
			instant.setTimecreated(request.getParameter("time"));
			instant.setStatus(request.getParameter("status"));
			instant.setUserId(userId);
			
			instantORM.delete(instant);
			
			
		}
		
		response.setStatus(200);
		
		}
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
