package orm;
import java.util.List;
import org.springframework.orm.hibernate3.HibernateTemplate;
import pojo.Customer;


public class CustomerORM {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final Customer customer) {
		hibernateTemplate.save(customer);
		
	}
	
	public void update(final Customer customer){
		
		hibernateTemplate.update(customer);		
	}

	public List<Customer> fetchALLData() {
		return hibernateTemplate.loadAll(Customer.class);
	}
	
	public void delete(final Customer customer){
		
		hibernateTemplate.delete(customer);		
	}
	
}
