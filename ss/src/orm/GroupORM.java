package orm;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import pojo.Customer;
import pojo.Groups;



public class GroupORM {
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final Groups groups) {
		hibernateTemplate.save(groups);
	}
	
	
	public void update(final Groups groups){
		
		hibernateTemplate.update(groups);		
	}

	public List<Groups> fetchALLData() {
		return hibernateTemplate.loadAll(Groups.class);
	}
	
		public void delete(final Groups groups){
		
		hibernateTemplate.delete(groups);		
	}
	
}