package pojo;

import java.io.Serializable;

import javax.persistence.Entity;


@Entity
public class GroupCustomer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long groupId;
	private long customerId;
	
	
	
	public GroupCustomer() {
		super();
	}
	

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	
	
	
}
