package orm;
import java.util.List;
import org.springframework.orm.hibernate3.HibernateTemplate;

import pojo.InstantMessage;
import pojo.User;


public class UserORM {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public void saveOrUpdate(final User user) {
		hibernateTemplate.save(user);
	}
	
	public void update(final User user){
		
		hibernateTemplate.update(user);		
	}

	public List<User> fetchALLData() {
		return hibernateTemplate.loadAll(User.class);
	}
}
