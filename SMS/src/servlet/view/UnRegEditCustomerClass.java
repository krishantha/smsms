package servlet.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import orm.GroupCustomerORM;
import pojo.Customer;
import pojo.GroupCustomer;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class EditCustomerClass
 */
@WebServlet("/UnRegEditCustomer")
public class UnRegEditCustomerClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UnRegEditCustomerClass() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		long userId=(Long) request.getSession().getAttribute("userid");
		
		System.out.println(userId);
		
		long keyword=Long.parseLong(request.getParameter("keyword"));
		
		System.out.println("keyword is "+keyword);
			
		
		
		int page=Integer.parseInt(request.getParameter("page"));
				
		int total_pages=0;
		
		System.out.println(page);
		
		
		int rid=Integer.parseInt(page-1+"0");
		
		
		JSONObject responsedata=new JSONObject();
		
		JSONArray cellarray=new net.sf.json.JSONArray();
	    		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		CustomerORM customerORM=(CustomerORM)context.getBean("customerORMBean");
		List <Customer> customerList = customerORM.fetchALLData();
		
		GroupCustomerORM grpcusORM=(GroupCustomerORM)context.getBean("grpcusORMBean");
		List <GroupCustomer> grpcustomerList = grpcusORM.fetchALLData();
		
		int count=0;
		
		try 
		{
			
			
									
		 	request.getSession().setAttribute("customers",customerList);
		 	
	      

	        JSONArray cell=new net.sf.json.JSONArray();
	        JSONObject cellobj=new net.sf.json.JSONObject();
	        

			
	        
			if(!customerList.isEmpty())
			{
			
				for(Customer customer:customerList)
				{
					
					long userid=customer.getUserId();
					
					if(userid==userId){
						
						long cid=customer.getCustomerId();
						
						if(!grpcustomerList.isEmpty()){
						
						boolean res=false;
							
						for(GroupCustomer gc:grpcustomerList)
						{
							
							if(cid==gc.getCustomerId() && gc.getGroupId() == keyword){
								
								res=true;
							}
						}
						
						if(!res){
							
							count++;
							
							if(count>rid){
							
							System.out.println();
							cellobj.put("id", customer.getCustomerId());
							cell.add(customer.getCustomerId());
							cell.add(customer.getCustomerName());
							cell.add(customer.getMobile());
							cell.add(customer.geteMail());
							cell.add(customer.getDateOfBirth());
							cell.add(customer.getAddress());
							
							
							
							cellobj.put("cell", cell);
							cell.clear();
							cellarray.add(cellobj);
							}
							
						}
						
						}
						else{
							
							count++;
							
							if(count>rid){
							cellobj.put("id", customer.getCustomerId());
							cell.add(customer.getCustomerId());
							cell.add(customer.getCustomerName());
							cell.add(customer.getMobile());
							cell.add(customer.geteMail());
							cell.add(customer.getDateOfBirth());
							cell.add(customer.getAddress());
							
							
							
							cellobj.put("cell", cell);
							cell.clear();
							cellarray.add(cellobj);
							}
						}
					
					}
				}
					if(count>10){
					total_pages=count/10;
					
					if(count%10>0){
						
						total_pages++;
						
					}
								
				}
				
				if(page>total_pages){
					
					page=total_pages;
				}
				responsedata.put("total",total_pages);
		        responsedata.put("page",page);
		        responsedata.put("records",count);
				responsedata.put("rows",cellarray);

			}
		}
				
	
		catch (Exception e)
		{
			
		}
	
		
		response.getWriter().write(responsedata.toString());
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
	

}
