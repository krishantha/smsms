package servlet.view;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import orm.ScheduledSMSORM;
import pojo.ScheduledMessage;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class EditCustomerClass
 */
@WebServlet("/SearchViewScheduledSMS")
public class SearchViewScheduledSMS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchViewScheduledSMS() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		long userId=(Long) request.getSession().getAttribute("userid");
	
			
		String name=request.getParameter("keyword");
		int page=Integer.parseInt(request.getParameter("page"));
		
		int total_pages=0;
		int rid=Integer.parseInt(page-1+"0");
		
		JSONObject responsedata=new JSONObject();
		
		JSONArray cellarray=new net.sf.json.JSONArray();
	    		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		ScheduledSMSORM ssmsORM=(ScheduledSMSORM)context.getBean("scheduledORMBean");
		List <ScheduledMessage> shsmsList =ssmsORM.fetchALLData();
		
		Collections.reverse(shsmsList);
		int count=0;
		
		try 
		{
			
			
									
		 	request.getSession().setAttribute("scheduledList",shsmsList);
		 		     
	        JSONArray cell=new net.sf.json.JSONArray();
	        JSONObject cellobj=new net.sf.json.JSONObject();
	        

			
	        
			if(!shsmsList.isEmpty())
			{
			
				for(ScheduledMessage scheduled:shsmsList)
				{
					
					
					String mobile=scheduled.getMobileNo();
					String content=scheduled.getMessagecontent();
					long userid=scheduled.getUserId();
					
					if(userid==userId && (mobile.contains(name) || content.contains(name))){
						count++;
						
						if(count>rid){
					
							cellobj.put("id", scheduled.getMessageId());
							cell.add(scheduled.getMessageId());
							cell.add(scheduled.getMobileNo());
							cell.add(scheduled.getMessagecontent());
							cell.add(scheduled.getDatescheduled());
							cell.add(scheduled.getTimescheduled());
							cell.add(scheduled.getStatus());
							
							
							
							
							cellobj.put("cell", cell);
							cell.clear();
							cellarray.add(cellobj);
						}
					}
				}
					if(count>10){
					
					
					
					total_pages=count/10;
					
					if(count%10>0){
						
						total_pages++;
						
					}
								
				}
				
				if(page>total_pages){
					
					page=total_pages;
				}
				responsedata.put("total",total_pages);
		        responsedata.put("page",page);
		        responsedata.put("records",count);
				responsedata.put("rows",cellarray);

			}
		}
				
	
		catch (Exception e)
		{
			
		}
	
		
		response.getWriter().write(responsedata.toString());
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
	

}
