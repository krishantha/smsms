package servlet.edit;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.UserORM;
import pojo.User;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/UserEdit")
public class UserEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation;
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		User user=new User();
		List <User> userList = userORM.fetchALLData();
	
		operation=request.getParameter("operation");
		
		
		System.out.println(operation);
		
		
		
		
		if(operation.equalsIgnoreCase("edit"))
		{
			long userId=(Long) request.getSession().getAttribute("userid");
			
			String password=request.getParameter("password");
			
			String name="";
			String company="";
			String username="";
			String mobile="";
			
			
			boolean valid=false;
			for(User ouser : userList){
				
				if ((ouser.getUserId()==userId)  && (ouser.getPassword().equals(password))){
					
					valid=true;
					name=ouser.getName();
					company=ouser.getCompany();
					username=ouser.getUsername();
					mobile=ouser.getMobile();
					
				}
			}
			
			if(valid){
				
			
			user.setUserId(userId);
			user.setName(name);
			user.setCompany(company);
			user.setUsername(username);
			user.setPassword(request.getParameter("newpassword"));
			user.setMobile(mobile);
			userORM.update(user);
			
			
			response.getWriter().write("changed");
			}
			else{
				
				response.getWriter().write("invalid");
				
			}
			
						
		}
		
		else if(operation.equalsIgnoreCase("add"))
		{
			
			boolean userExist=false;
			
			for(User ouser : userList){
				
				if (ouser.getUsername().equalsIgnoreCase(request.getParameter("username"))){
					
					userExist=true;
				}
			}
			if(userExist){
				
				response.getWriter().write("usernameExist");
			}
			else{
				
				String mobile=request.getParameter("mobile");
				
				if(mobile.startsWith("+94")){
					
					user.setName(request.getParameter("name"));
					user.setCompany(request.getParameter("company"));
					user.setUsername(request.getParameter("username"));
					user.setPassword(request.getParameter("password"));
					user.setMobile(request.getParameter("mobile"));
					
				}
				else{
					
					user.setName(request.getParameter("name"));
					user.setCompany(request.getParameter("company"));
					user.setUsername(request.getParameter("username"));
					user.setPassword(request.getParameter("password"));
					user.setMobile("+94"+mobile.substring(1));
					
				}
				
			
			
			userORM.saveOrUpdate(user);
			
			response.getWriter().write("userAdded");
			
			}
		}
		
		else if(operation.equalsIgnoreCase("del"))
		{
			long id=Long.parseLong(request.getParameter("id"));
			
			System.out.println(id);
			
			
		}
		
		response.setStatus(200);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
