package servlet.edit;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import orm.CustomerORM;
import orm.ScheduledSMSORM;
import orm.UserORM;
import pojo.Customer;
import pojo.ScheduledMessage;
import pojo.User;


/**
 * Servlet implementation class CustomerEdit
 */
@WebServlet("/ScheduledSMSEdit")
public class ScheduledSMSEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScheduledSMSEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String operation;
		
		ApplicationContext context=WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		ScheduledSMSORM scheduledORM=(ScheduledSMSORM)context.getBean("scheduledORMBean");
		UserORM userORM=(UserORM)context.getBean("userORMBean");
		ScheduledMessage scheduled=new ScheduledMessage();
		List <User> userList = userORM.fetchALLData();
		
		long userId=(Long) request.getSession().getAttribute("userid");
		
		  Calendar currentDate = Calendar.getInstance();
		  SimpleDateFormat dformatter= new SimpleDateFormat("yy/MM/dd");
		  SimpleDateFormat tformatter= new SimpleDateFormat("HH:mm:ss");
		  String dateNow = dformatter.format(currentDate.getTimeInMillis());
		  String timeNow = tformatter.format(currentDate.getTimeInMillis());
		  
		  
		
		operation=request.getParameter("oper").toString();
		
		
		System.out.println(operation);
		
		
		if(userId!=-1){
		
		if(operation.contains("add"))
		{
			
			String mobile=request.getParameter("mobile");
			
			if(mobile.startsWith("+94")){
						
				scheduled.setMobileNo(request.getParameter("mobile"));
				scheduled.setMessagecontent(request.getParameter("content"));
				scheduled.setDatescheduled(request.getParameter("date"));
				scheduled.setTimescheduled(request.getParameter("time"));
				scheduled.setStatus("sending");
				scheduled.setUserId(userId);
			}
			else{
										
				scheduled.setMobileNo("+94"+mobile.substring(1));
				scheduled.setMessagecontent(request.getParameter("content"));
				scheduled.setDatescheduled(request.getParameter("date"));
				scheduled.setTimescheduled(request.getParameter("time"));
				scheduled.setStatus("sending");
				scheduled.setUserId(userId);
			}
			
						
			scheduledORM.saveOrUpdate(scheduled);
		}
		
		if(operation.contains("edit"))
		{
			
			String mobile=request.getParameter("mobile");
			System.out.println(mobile);
			
			if(mobile.startsWith("+94")){
					
				long messageid=Long.parseLong(request.getParameter("id"));
				scheduled.setMessageId(messageid);
				scheduled.setMobileNo(request.getParameter("mobile"));
				scheduled.setMessagecontent(request.getParameter("content"));
				scheduled.setDatescheduled(request.getParameter("date"));
				scheduled.setTimescheduled(request.getParameter("time"));
				scheduled.setStatus(request.getParameter("status"));
				scheduled.setUserId(userId);
			}
			else{
				long messageid=Long.parseLong(request.getParameter("id"));
				scheduled.setMessageId(messageid);						
				scheduled.setMobileNo("+94"+mobile.substring(1));
				scheduled.setMessagecontent(request.getParameter("content"));
				scheduled.setDatescheduled(request.getParameter("date"));
				scheduled.setTimescheduled(request.getParameter("time"));
				scheduled.setStatus(request.getParameter("status"));
				scheduled.setUserId(userId);
			}
			
						
			scheduledORM.update(scheduled);
			
		}
		
		else if(operation.contains("del"))
		{
			String rec=request.getParameter("id");
			
			String[] ids=rec.split(",");
			
			for(int i=0;i<ids.length;i++){
			
			scheduled.setMessageId(Long.parseLong(ids[i]));
			scheduled.setMobileNo(request.getParameter("mobile"));
			scheduled.setMessagecontent(request.getParameter("content"));
			scheduled.setDatescheduled(request.getParameter("date"));
			scheduled.setTimescheduled(request.getParameter("time"));
			scheduled.setStatus(request.getParameter("status"));
			scheduled.setUserId(userId);
			
			scheduledORM.delete(scheduled);
			}
			
		}
		
		response.setStatus(200);
		
		}
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
