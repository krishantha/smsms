<%@page session="true"%>
<%
if(session==null || session.getAttribute("user")==null)
{
response.sendRedirect("welcome.jsp");
}
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:990px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-top: 0;
	margin-left: auto;
	margin-right: auto;
	}
</style>


<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>


    
<script type="text/javascript">

		function loadall(){
			document.getElementById("txtContact").value="";
			jQuery("#addtable").jqGrid('setGridParam',{url:"EditCustomer"}).trigger("reloadGrid");
			
		}
		
</script>


    
<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(cvalue, colname) {
			var i;
			var valid=false;
			var arr=cvalue.split(",");

			for(i=0;i<arr.length;i++){
				
				
				var phoneRE=/\+94\d{9}$/;
				var phoneRX=/\d{10}$/;
				
				if(arr[i].match(phoneRX) || (arr[i].match(phoneRE) && arr[i].length==12)){
					
					valid=true;
				}
				
				else{
					valid=false;
				}
			}

			if(valid==true){
				return [true,""];
				}
			else{
				return [false,"Mobile No is not valid"];
				}
			
			
		}

				    	
		    	 jQuery("#results").jqGrid({
				       	url:'ViewInstantSMS?',
				    	datatype: "json",
				       	colNames:['ID', 'Mobile No', 'Content','Date','Time','Status'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:100,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"mobile",index:"mobile",width:120,editable:true,editrules: {custom:true,custom_func:mobilecheck,required: true},formoptions:{ rowpos:1,elmsuffix:'<button type=\"button\" id=\"btnAdd\" >Add</button>' },editoptions: { size : 68,maxlength:70 }},
				              {name:"content",index:"content",width:300,editable:true ,editrules:{},editoptions:{maxlength:160,rows:"5",cols:"80"},edittype: 'textarea'},
				              {name:"date",index:"date",width:100,editable:false},
				              {name:"time",index:"time",width:100,editable:false},
				              {name:"status",index:"status",width:100,editable:false}			       		
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: true,
				        multiselect:true,
				        sortorder: "desc",
				        pager:'#pager',
				        editurl:"InstantSMSEdit",
				        caption:"Instant SMS",
				        loadError: function(xhr,status,error){
				        	return document.location='logout.jsp';
					        }
		    	 		
		    	 });
		    	 
		    	
		    	 jQuery("#results").jqGrid('navGrid',"#pager",{edit:false,addicon: "ui-icon-mail-closed",refresh:true,search:false}, //options
			   			  {}, // edit options
			   			  {height:205,width:600,reloadAfterSubmit:true,closeAfterAdd:true,savekey: [true, 13],recreateForm: true,
				   			  
			   				  afterShowForm : function (formid) {
				   				  
			   						$("#btnAdd").button({icons: {primary: "ui-icon-plus"}});
			   						
				   			  },
				   			  beforeShowForm: function () {
					   			  
					   				$("#btnAdd").click(function(){
					   					$( "#dialogContacts" ).dialog( "open" );
					   	          	});
				   	          	
				   		      },
				   			  beforeSubmit : function(postdata, formid) { 
					   				var i;
					   				var value=postdata.mobile;
					   				var m=value.split(",");
					   			
					   				for(i=0;i<m.length;i++){
					   				
					   				
					   				$(function(){ //jquery function
										
					   					var jq=$.ajax({  // ajax call
					   						
					   						type: "GET",
					   						url: "InstantSMSEdit",
					   						data: {oper : "add" , mobile :m[i], content: postdata.content}
					   					
					   					});
					   						
					   					
					   					jq.success(function (response,status){
					   						jQuery("#results").trigger("reloadGrid");
					   						});
					   				});
					   				
					   				}
									
					   				
					   				jQuery('.ui-jqdialog-titlebar-close').click();
					   			 	

					   			 	
					   			 	
					   			 	
					   			 	
				   			  }	   			  
				   			  
				   			}, // add options
			   			  {reloadAfterSubmit:false}, // del options
			   			  {} // search options
			   			  );


		    	 var grid = $("#results"), intervalId = setInterval(
		    	     function() {
		    	         grid.trigger("reloadGrid",[{current:true}]);
		    	     },
		    	     30000);	

	    	        	 	
	
});

 

</script>

<script>

	function search(){
		
		
		var keyword=document.getElementById("txtContact").value;
		
		jQuery("#addtable").jqGrid('setGridParam',{url:"SearchEditCustomer?keyword="+keyword}).trigger("reloadGrid");
	
	}

</script>

<script>

	function searchSMS(){
		
		
		var keyword=document.getElementById("txtSearch").value;
		
		jQuery("#results").jqGrid('setGridParam',{url:"SearchViewInstantSMS?keyword="+keyword}).trigger("reloadGrid");
	
	}

</script>

<script>

	function loadallSMS(){
		
		document.getElementById("txtSearch").value="";
		jQuery("#results").jqGrid('setGridParam',{url:"ViewInstantSMS"}).trigger("reloadGrid");
	
	}

</script>


<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		
		function gridReload(gid){

			jQuery("#addtable").jqGrid('setGridParam',{url:"RegEditCustomer?keyword="+gid}).trigger("reloadGrid");
		}
		
		
			
				    	
		    	 jQuery("#grptable").jqGrid({
				       	url:'ViewGroups',
				    	datatype: "json",
				       	colNames:['ID','Name','Description'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:50,search:true,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"name",search:true,index:"name",width:150,search:true,editable:true,editrules: { edithidden: false,required: true },editoptions:{size:30}},
				              {name:"description",index:"description",width:80,editable:true,search:true,hidden:true,editrules: {edithidden: true},editoptions: {size : 10,maxlength:160,rows:"7",cols:"30"},edittype: 'textarea'}
				              			       		
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: false,
				        multiselect:false,
				        sortorder: "asc",
				        pgbuttons: false,
				        pgtext: null,
				        pager:'#grppager',
				        editurl:"GroupEdit",
				        caption:"Groups",
				        gridComplete: function (id) {
				        	var top_rowid = $('#grptable tbody:first-child tr:nth-child(2)').attr('id');
					        $("#grptable").setSelection(top_rowid);
					        },
				        onSelectRow: function(id){		    		
					    	var selr = jQuery('#grptable').jqGrid('getGridParam','selrow');
					    	
					    	gid=selr;
					    	gridReload(selr);
					    	
				        },
				        loadError: function(xhr,status,error){return document.location='logout.jsp';
				        
					    }
		    	 });
		    	
		    	 jQuery("#grptable").jqGrid('navGrid',"#grppager",{add:false,edit:false,del:false,refresh:false,search:false}, //options
		   			  {}, // edit options
		   			  {}, // add options
		   			  {}, // del options
		   			  {}  // search options
		   			  );
		    	
	
});

</script>



<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

	
 	
		    	 jQuery("#addtable").jqGrid({
				       	url:'EditCustomer?',
				    	datatype: "json",
				       	colNames:['ID', 'Name', 'Mobile No'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:80,editable:false,editoptions:{readonly:true,size:20}},  
				       		  {name:"name",index:"name",width:200,editable:false,editoptions:{readonly:true,size:20}},
				              {name:"mobile",index:"mobile",width:100,editable:true,editoptions:{readonly:true,size:20}}
				              
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        multiselect:true,
				        sortorder: "desc",
				        pager:'#addpager',
				        editurl:"InstantSMSEdit",
				        caption:"Add Customer",
				        loadError: function(xhr,status,error){
				        	return document.location='logout.jsp';
					        }
		    	 		
		    	 });
		    	 
		    	 
		    	 jQuery("#addtable").jqGrid('navGrid',"#addpager",{edit:false,add:false,del:false,refresh:false,search:false}, //options
			   			  {}, // edit options
			   			  {}, // add options
			   			  {}, // del options
			   			  {}  // search options
			   	 );
	
});

 

</script>


<script type="text/javascript">
	
	$(function() {
		var added=0;
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
				
			tips = $( ".validateTips" );

		$( "#dialogContacts" ).dialog({
			autoOpen: false,
			height: 500,
			width: 650,
			
			buttons: {
				
				"Add Contact(s)": function() {
					
					var selectedrows = $("#addtable").jqGrid('getGridParam','selarrrow');
					if(selectedrows.length) {
						for(var i=0;i<selectedrows.length; i++) {
							
						var selecteddata = $("#addtable").jqGrid('getRowData',selectedrows[i]);
							if($('#mobile').val()==""){
								$('#mobile').val(selecteddata.mobile);
								added++;
								document.getElementById("noc").innerHTML=added;
								loadall();
							}
							else{
								$('#mobile').val($('#mobile').val() +','+selecteddata.mobile);
								added++;
								document.getElementById("noc").innerHTML=added;
								loadall();
							}
						}
					}

						
				},
				"Close": function() {
					$( this ).dialog( "close" );
					document.getElementById("noc").innerHTML="";
					added=0;
				}
				
			},
			close: function() {
				
			}
		});
		
	});
	
	</script>


<script type="text/javascript">
	$(function(){

		$("#btnAll").button({icons: {primary: "ui-icon-carat-2-n-s"}});
		$("#btnAllSMS").button({icons: {primary: "ui-icon-carat-2-n-s"}});
		$("#repeat").buttonset();
		$("#btnLogout").button({icons: {primary: "ui-icon-bookmark"}});
				
		});
</script>

<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Send SMS</title>

</head>
<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">
<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center" align="center">
	
	<table width="900" style="padding-left: 22px;">
		<tr>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all"> 
			
				<span id="repeat"> 
					<input type="radio" id="repeat0" name="repeat" checked="checked" /><label for="repeat0">Send SMS</label> 
					<input type="radio" id="repeat1" name="repeat" onclick="return document.location='ScheduledSMS.jsp';"/><label for="repeat1"><a href=ScheduledSMS.jsp>Schedule SMS</a></label> 
					<input type="radio" id="repeat2" name="repeat" onclick="return document.location='Customer.jsp';"/><label for="repeat2"><a href=Customer.jsp>Manage Customer</a></label> 
					<input type="radio" id="repeat3" name="repeat" onclick="return document.location='Group.jsp';"/><label for="repeat3"><a href=Group.jsp>Manage Groups</a></label>
					<input type="radio" id="repeat4" name="repeat" onclick="return document.location='ChangePassword.jsp';"/><label for="repeat4"><a href=ChangePassword.jsp>Settings</a></label>
				</span> 
			</span> 
			</td>
			<td style="width: 5px;"></td>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all">
			<%=session.getAttribute("user") %> <button class="ui-state-default" type="button" id="btnLogout" onclick="return document.location='logout.jsp';" value="edit" >logout</button>
			</span>
			</td>
			
		</tr>
	</table>
	
	<br/><br/>
	
	
	<div style="padding-bottom: 50px;" id="searchresults">
	
		<div align="left" class="ui-widget-content ui-corner-all" style="padding: 10px; margin-right: 50px; margin-left: 50px; margin-bottom: 10px;">
							<Strong>&nbsp;&nbsp;Search : &nbsp;&nbsp;</Strong><input placeholder="Use Mobile No or Content" type="text" id="txtSearch" onkeyup="searchSMS();" name="txt_search" style="outline: none;padding: 5px; width: 500px;" class="text ui-widget-content ui-corner-all"/><button type="button" id="btnAllSMS" value="edit" onclick="loadallSMS();" style="float: right; margin-bottom: 5px;">All</button>
		</div>
		
	<table id="results" width="900"></table>
	<div id="pager"> </div>	
	</div>
	
		<div>
		
		<p>
			&copy;2012 - Izone Developers&trade;
		</p>
		</div>
					<div align="left" id="dialogContacts" title="Add contacts" style="display: none;">
						
						<div class="ui-widget-content ui-corner-all" style="padding: 2px;">
							<Strong>&nbsp;&nbsp;Search </Strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact Name : <input type="text" id="txtContact" onkeyup="search();" name="txt_contact" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/><button type="button" id="btnAll" value="edit" onclick="loadall();" style="float: right; margin-bottom: 5px;">All contacts</button>
						</div>
						
						<table>
							<tr>
								<td>
									<table id="grptable" width="50"></table>
									<div id="grppager"> </div>
								</td>
								<td>
									<table id="addtable" width="200"></table>
									<div id="addpager"> </div>
								</td>
							</tr>	
						</table>
						
						<div class="ui-widget-content ui-corner-all" style="padding: 2px;">
								<Strong>&nbsp;&nbsp;Contacts added : </Strong><label id="noc"></label>
							
						</div>
						</div>
						
					
	</div>
	</div>
	</div>
	
	<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
	
</body>
</html>