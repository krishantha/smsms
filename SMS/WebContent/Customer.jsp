<%@page session="true"%>
<%
if(session==null || session.getAttribute("user")==null)
{
response.sendRedirect("welcome.jsp");
}
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:990px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-top: 0;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>



<script>

	function searchCustomer(){
		
		
		var keyword=document.getElementById("txtSearch").value;
		
		jQuery("#results").jqGrid('setGridParam',{url:"SearchEditCustomer?keyword="+keyword}).trigger("reloadGrid");
	
	}

</script>

<script>

	function loadallCustomers(){
		
		document.getElementById("txtSearch").value="";
		jQuery("#results").jqGrid('setGridParam',{url:"EditCustomer"}).trigger("reloadGrid");
	
	}

</script>
    
    
<script type="text/javascript">

$.jgrid.no_legacy_api = true;

$.jgrid.useJSON = true;

$(function() {

		function mobilecheck(value, colname) {
			var phoneRE=/\+94\d{9}$/;
			var phoneRX=/\d{10}$/;
			
			if(value.length==10 && value.match(phoneRX)){
				
				return [true,""];
			}
			else if(value.length==12 && value.match(phoneRE)){
				return [true,""];
			}
			else{
				return [false,"Mobile No is not valid"];
			}
		}

				    	
		    	 jQuery("#results").jqGrid({
				       	url:'EditCustomer',
				    	datatype: "json",
				       	colNames:['ID','Name', 'Mobile No', 'E-mail','Date Of Birth','Address'],
				       	colModel:[
							       	
				       		  {name:"id",index:"id",width:100,search:true,editable:false,editoptions:{readonly:true,size:20}},  
				              {name:"name",search:true,index:"name",width:200,search:true,editable:true,editrules: { edithidden: true,required: true },editoptions:{size:30}},
				              {name:"mobile",index:"mobile",width:120,editable:true,search:true,editrules: {custom:true,custom_func:mobilecheck,required: true,number:true},editoptions: { size : 30,maxlength:12 }},
				              {name:"email",index:"email",width:140,editable:true ,search:true,editrules:{email:true,required: true},editoptions:{size:30}},
				              {name:"dob",index:"dob",width:100,editable:true,search:true,edittype: 'text',
					       		    editoptions: {
						       		      size: 30, maxlengh: 30,
						       		      dataInit: function(element) {
						       		        $(element).datepicker({changeMonth: true,changeYear: true,dateFormat: 'yy-mm-dd',showAnim: 'fold',maxDate: "-10Y"});
						       		      }
						       		    }},
				              {name:"address",index:"address",search:true,width:160,editable:true,editoptions:{maxlength:160,rows:"4",cols:"30"},edittype: 'textarea'}			       		
				       			
				       	],
				       	rowNum:10,
				       	height:235,
				       	sortname: 'id',
				       	gridview:true,
				        viewrecords: true,
				        multiselect:true,
				        sortorder: "asc",
				        pager:'#pager',
				        editurl:"CustomerEdit",
				        caption:"Customers",
				        loadError: function(xhr,status,error){return document.location='logout.jsp';}
		    	 });
		    	 
		    	 //jQuery("#results").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		    	 
		    	 jQuery("#results").jqGrid('navGrid',"#pager",{search:false}, //options
		   			  {height:250,reloadAfterSubmit:true,closeAfterAdd:true}, // edit options
		   			  {height:260,reloadAfterSubmit:true,closeAfterAdd:true}, // add options
		   			  {reloadAfterSubmit:false}, // del options
		   			  {} // search options
		   			  );

	   			  
		    	
	
});

</script>

<script type="text/javascript">
	$(function(){
		$("#btnAllCustomer").button({icons: {primary: "ui-icon-carat-2-n-s"}});
		$( "#repeat" ).buttonset();
		$("#btnLogout").button({icons: {primary: "ui-icon-bookmark"}});
		
		});
</script>


<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Customers</title>

</head >
<body onload="getuser();noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">
<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center" align="center">


	<table width="900" style="padding-left: 22px;">
		<tr>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all"> 
				<span id="repeat"> 
					<input type="radio" id="repeat0" name="repeat" onclick="return document.location='InstantSMS.jsp';" /><label for="repeat0"><a href=InstantSMS.jsp>Send SMS</a></label> 
					<input type="radio" id="repeat1" name="repeat" onclick="return document.location='ScheduledSMS.jsp';"/><label for="repeat1"><a href=ScheduledSMS.jsp>Schedule SMS</a></label> 
					<input type="radio" id="repeat2" name="repeat" checked="checked" /><label for="repeat2">Manage Customer</label> 
					<input type="radio" id="repeat3" name="repeat" onclick="return document.location='Group.jsp';"/><label for="repeat3"><a href=Group.jsp>Manage Groups</a></label>
					<input type="radio" id="repeat4" name="repeat" onclick="return document.location='ChangePassword.jsp';"/><label for="repeat4"><a href=ChangePassword.jsp>Settings</a></label>
				</span> 
			</span> 
			</td>
			
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all">
			<%=session.getAttribute("user") %> <button class="ui-state-default" type="button" id="btnLogout" onclick="return document.location='logout.jsp';" value="edit" >logout</button>
			</span>
			</td>
			
		</tr>
	</table>
<br/><br/>
	
		
	<div style="padding-bottom: 50px;" id="searchresults">
	
		<div align="left" class="ui-widget-content ui-corner-all" style="padding: 10px; margin-right: 50px; margin-left: 50px; margin-bottom: 10px;">
							<Strong>&nbsp;&nbsp;Search : &nbsp;&nbsp;</Strong><input placeholder="Customer name" type="text" id="txtSearch" onkeyup="searchCustomer();" name="txt_search" style="outline: none;padding: 5px; width: 500px;" class="text ui-widget-content ui-corner-all"/><button type="button" id="btnAllCustomer" value="edit" onclick="loadallCustomers();" style="float: right; margin-bottom: 5px;">All Customers</button>
		</div>
		
	<table id="results" width="900"></table>
	<div id="pager"> </div>	
		
	
	</div>
	
		<div>
		
		<p>
			&copy;2012 - Izone Developers&trade;
		</p>
		</div>
	
	</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
	
</div>
</body>
</html>