<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:990px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-top: 0;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>


<script type="text/javascript">
	$(function()
	{
		$("#btnLogin").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnSign").button({icons: {primary: "ui-icon-circle-check"}});
		
	});
</script>


<script type="text/javascript">
	
	$(function() {
		
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
				
			tips = $( ".validateTips" );

		$( "#dialogSign" ).dialog({
			autoOpen: false,
			height: 170,
			width: 300,
			
			buttons: {
				
				"Log In": function() {
					
					document.location='logout.jsp';
						
				},
				"Cancel": function() {
					$( this ).dialog( "close" );
				}
				
			},
			close: function() {
				
			}
		});
		
	});
	
	</script>


<script type="text/javascript">

function signup()
{
	var valid=true;
	var xmobile=true;
	var conf=true;
	
	var name=$("#txtName");
	var company=$("#txtCompany");
	var username=$("#txtUsername");
	var mobile=$("#txtMobile");
	var password=$("#txtPassword");
	var cpassword=$("#txtcPassword");
	

	document.getElementById("lblErr").innerHTML="";
	name.removeClass( "ui-state-error");
	company.removeClass( "ui-state-error");
	username.removeClass( "ui-state-error");
	mobile.removeClass( "ui-state-error");
	password.removeClass( "ui-state-error");
	cpassword.removeClass( "ui-state-error");
	document.getElementById('err').style.visibility='hidden';

	if (document.getElementById("txtUsername").value == "") {
		
		username.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtMobile").value == "") {
		
		mobile.addClass( "ui-state-error" );
		valid = false;
	}
	
	if (document.getElementById("txtPassword").value == "") {
		
		password.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtcPassword").value == "") {
		
		cpassword.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtcPassword").value != document.getElementById("txtPassword").value) {
		
		cpassword.addClass( "ui-state-error" );
		password.addClass( "ui-state-error" );
		conf = false;
	}
	if (document.getElementById("txtName").value == "") {
		
		name.addClass( "ui-state-error" );
		valid = false;
	}
	if (!validmobile(document.getElementById("txtMobile").value)) {
		
		mobile.addClass( "ui-state-error" );
		xmobile = false;
	}
	if(valid==false)
	{
			document.getElementById("lblErr").innerHTML="Fill all the required fields";
			$("#err").addClass("ui-state-error ui-corner-all");	
			document.getElementById('err').style.visibility='visible';
		
	}
	else if(xmobile==false){

			document.getElementById("lblErr").innerHTML="Invalid mobile number</br>Please use following format</br>0777123123 or +94777123123";
			$("#err").addClass("ui-state-error ui-corner-all");	
			document.getElementById('err').style.visibility='visible';
	}
	else if(conf==false){

		document.getElementById("lblErr").innerHTML="Passwords do not match";
		$("#err").addClass("ui-state-error ui-corner-all");	
		document.getElementById('err').style.visibility='visible';
}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "UserEdit",
				data: {operation : "add" ,name : name.val() ,company : company.val() , username : username.val() , password : password.val(), mobile : mobile.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="usernameExist"){
					username.addClass( "ui-state-error" );
					document.getElementById("lblErr").innerHTML="Username not available";
					$("#err").addClass("ui-state-error ui-corner-all");	
					document.getElementById('err').style.visibility='visible';
					
					
				}
				else if(response=="userAdded"){
					
					document.getElementById("lblErr").innerHTML="Account created!";
					document.getElementById('lblErr').style.visibility='visible';
					
					$("#err").addClass("ui-widget-content ui-corner-all");
					document.getElementById("txtUsername").value = "";
					document.getElementById("txtMobile").value = "";
					document.getElementById("txtPassword").value = "";
					document.getElementById("txtName").value = "";
					document.getElementById("txtCompany").value = "";
					document.getElementById("txtcPassword").value = "";
					$( "#dialogSign" ).dialog( "open" );
					
					}
				else{
					
					document.location='welcome.jsp';

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="No response from server";
				$("#err").addClass("ui-state-error ui-corner-all");	
				document.getElementById('err').style.visibility='visible';
			});

		});
	}

	function validmobile(mobile){

		
		var phoneRE=/\+94\d{9}$/;
		var phoneRX=/\d{10}$/;
		
		if(mobile.length==10 && mobile.match(phoneRX)){
			
			return true;
		}
		else if(mobile.length==12 && mobile.match(phoneRE)){
			return true;
		}
		else{
			return false;
		}
					
	}
}
</script>

	<script>
		$(function() {
			$( "#accordion1" ).accordion({
				autoHeight: false,
				icons: false
				});
			
		});
	</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sign Up</title>

</head>
<body>

<div>
	<div class="ui-widget-header">&nbsp;&nbsp;SMS management system</div>

	
	<div class="ui-widget-content" align="center">
	
	<br/>
	<br/>

	<div style="padding: 5px; width: 500px;" align="center">
									<div id="accordion1">
										<h3 align="left"><a href="#">Sign Up</a></h3>
									<div>	
	
	<table>
		
		<tr>
			<td colspan = "3"><p></p></td>
		</tr>
		<tr>
			<td>Full Name</td><td style="width: 5px;"></td><td><input type="text" id="txtName" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Organization</td><td style="width: 5px;"></td><td><input type="text" id="txtCompany" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Username</td><td style="width: 5px;"></td><td><input type="text" id="txtUsername" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Password</td><td style="width: 5px;"></td><td><input type="password" id="txtPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Confirm Password</td><td style="width: 5px;"></td><td><input type="password" id="txtcPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Mobile No</td><td style="width: 5px;"></td><td><input type="text" id="txtMobile" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
		<td  colspan = "3">
		<div id="err" class="ui-widget" style="padding: 0 .7em; visibility: hidden;">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Alert:</strong> <label id="lblErr" ></label></p>
		</div>
		</td>
		</tr>
		<tr>
			<td><button type="button" id="btnLogin" onclick="return document.location='logout.jsp';" value="edit" style="float: right; margin-bottom: 20px;">Log In</button></td><td style="width: 5px;"></td><td><button type="button" id="btnSign" onclick="signup();" value="edit" style="float: right; margin-bottom: 20px;">Sign Up</button></td><td></td>
		</tr>
	</table>
	
	</div>
	</div>
				<div align="left" id="dialogSign" title="Account Created" style="display: none;">
						
						<div class="ui-widget-content ui-corner-all" style="padding: 2px;">
									<p>Account created successfully.Please login to continue.</p>
						</div>
												
						</div>
	</div>
	
	<br/>
	<br/>
	
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
	
</div>
</body>
</html>