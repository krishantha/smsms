<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:990px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-top: 0;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    
    <script type="text/javascript">

    $(function() {
        $(".input").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $('btnLogin').click();
                return false;
            } else {
                return true;
            }
        });
    });
    
    </script>
    
    


<script type="text/javascript">
	$(function()
	{
		$("#btnLogin").button({icons: {primary: "ui-icon-bookmark"}});
		$("#btnSign").button({icons: {primary: "ui-icon-circle-check"}});
	});
</script>

	<script>
	$(function() {
		$( "#accordion1" ).accordion({
			autoHeight: false,
			icons: false
			});
		
	});
	</script>
	
	<script>
	$(function() {
		$( "#accordion2" ).accordion({
			autoHeight: false,
			icons: false
			});
		
	});
	</script>
	
	<script>
	$(function() {
		$( "#accordion3" ).accordion({
			autoHeight: false,
			icons: false
			});
		
	});
	</script>
	
	<script>
	$(function() {
		$( "#accordion4" ).accordion({
			autoHeight: false,
			icons: false
			});
		
	});
	</script>

<script type="text/javascript">

function login()
{
	var uvalid=true;
	var pvalid=true;
	
	var user=$("#lgnUsername");
	var pw=$("#lgnPassword");

	
	document.getElementById("lblErr").innerHTML="";
	user.removeClass( "ui-state-error");
	pw.removeClass( "ui-state-error");
	document.getElementById('err').style.visibility='hidden';

	if (document.getElementById("lgnUsername").value == "") {
		
		user.addClass( "ui-state-error" );
		uvalid = false;
	}
	if (document.getElementById("lgnPassword").value == "") {
		
		pw.addClass( "ui-state-error" );
		pvalid = false;
	}

	if(uvalid==false && pvalid==false)
	{
		document.getElementById("lblErr").innerHTML="Enter Username and Password";
		$("#err").addClass("ui-state-error ui-corner-all");		
		document.getElementById('err').style.visibility='visible';
	}
	else if(uvalid==false)
		{
			document.getElementById("lblErr").innerHTML="Enter Username";
			$("#err").addClass("ui-state-error ui-corner-all");		
			document.getElementById('err').style.visibility='visible';
		}
	else if(pvalid==false)
		{
			document.getElementById("lblErr").innerHTML="Enter Password";
			$("#err").addClass("ui-state-error ui-corner-all");		
			document.getElementById('err').style.visibility='visible';
		}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "Login",
				data: {username : user.val() , password : pw.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="invalid"){
					
					document.getElementById("lblErr").innerHTML="Invalid Username or Password";
					$("#err").addClass("ui-state-error ui-corner-all");	
					document.getElementById('err').style.visibility='visible';
					
					
				}
				else{
					document.getElementById("lblErr").innerHTML="";
					document.getElementById('err').style.visibility='hidden';
					$("#err").addClass("ui-state-error ui-corner-all");	
					document.location='login.jsp?user='+response;

				}
				});
			
			jq.error(function (request,error){	//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="No response from server";
				$("#err").addClass("ui-state-error ui-corner-all");	
				document.getElementById('err').style.visibility='visible';
			});

		});
	}
}

</script>



<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>

</head>


<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">



<div>
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	
	<div class="ui-widget-content">

	
	<table>
		<tr>
			<td>
			
				<table>
					<tr>
						<td>
							<div style="padding: 5px; margin-left:18px;margin-top: 10px; width: 580px;" align="left">
								<div id="accordion4">
									<h3><a href="#">SMS management system</a></h3>
									<div style="height: 145px;">
										<table>
											<tr>
												<td><label class="ui-icon ui-icon-mail-closed" style="float: left;"></label><br/><br/><br/></td>
												<td>
													<span>
														SMS management system is a concept of integrating information system with mobile phone as well as using short message service (SMS) as a medium of communication in the business process.
													</span>
												</td>
											</tr>
											<tr>
												<td><label class="ui-icon ui-icon-mail-closed" style="float: left;"></label><br/><br/><br/></td>
												<td >
													<p>
													SMS is an emerging and increasingly popular interaction channel in todays world.
													SMS management system is a perfect solution for managing SMS interactions with customers.
													</p>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div style="padding: 5px; margin-left:18px; width: 580px;" align="left">
								<div id="accordion2">
									<h3><a href="#">Why you need this system?</a></h3>
									<div>
										<table>
											<tr>
												<td>
													<div class="ui-widget-content ui-corner-all" style="width: 260px;">
														<table>
															<tr>
																<td>
																	<img src="images/mail_send.png" alt="image not found" width="80px"/>
																</td>
																<td>
																	<span>Send <Strong>unlimited</Strong> SMS to your customers, business partners or whoever you want.</span>
																</td>
															</tr>
														</table>
													</div>
												</td>
												<td>
													<div class="ui-widget-content ui-corner-all" style="width: 260px;">
														<table>
															<tr>
																<td>
																	<img src="images/user_manage.png" alt="image not found" width="80px"/>
																</td>
																<td>
																	<span>Save your customer contacts and <Strong>manage</Strong> them properly.</span>
																</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="ui-widget-content ui-corner-all" style="width: 260px;">
														<table>
															<tr>
																<td>
																	<img src="images/lock.png" alt="image not found" width="80px"/>
																</td>
																<td>
																	<span>Your data is <Strong>safe</Strong> and secured.Confidentiality is guaranteed.</span>
																</td>
															</tr>
														</table>
													</div>
												</td>
												<td>
													<div class="ui-widget-content ui-corner-all" style="width: 260px;">
														<table>
															<tr>
																<td>
																	<img src="images/calculator.png" alt="image not found" width="80px"/>
																</td>
																<td>
																	<span>Experience the <Strong>cheapest</Strong> SMS solution you could ever have.</span>
																</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<table>
					<tr>
						<td>
							<div style="padding: 5px; margin-right: 10px; margin-top: 10px; width: 330px;" align="right">
									<div id="accordion1">
										<h3 align="left"><a href="#">Log In</a></h3>
										<div  style="height: 220px;">
										
											<table>
												
												<tr>
													<td>Username</td><td style="width: 5px;"></td><td><input type="text" id="lgnUsername" name="lgnUsername" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td>
												</tr>
												<tr>
													<td>Password<td/><td style="width: 5px;"><input type="password" id="lgnPassword" name="lgnPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td>
												</tr>
												<tr>
													<td  colspan = "3">
														<div id="err" class="ui-widget" style=" visibility: hidden;">
																<p>&nbsp;<label class="ui-icon ui-icon-alert" style="float: left;"></label>
																<label id="lblErr" ></label></p>
														</div>
													</td>
												</tr>
												<tr>
													<td></td><td style="width: 5px;"></td><td><button type="button" id="btnLogin" onclick="login();" value="edit" style="float: right; margin-bottom: 5px;">Log In</button></td>
												</tr>
												<tr>
													<td></td><td style="width: 5px;"></td><td><a href=ForgotPassword.jsp style="float: right;">Forgot password</a></td><td></td>
												</tr>
												<tr>
													<td></td><td style="width: 5px;"></td><td><a href=SignUp.jsp style="float: right;">Sign up</a></td><td></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="padding: 5px; margin-right: 10px;width: 330px;" align="right">
									<div id="accordion3">
										<h3 align="left"><a href="#">Powerd by - Izone Developers&trade;</a></h3>
											<div>
												<p align="left">
													Subscribe now to recieve latest updates on our services
												</p>
												<div style="float: left;">
												<a href="https://www.facebook.com/izonedevelopers02" target="_blank"><img src="images/facebook.png" alt="image not found" width="50px"/></a>
												<a href="https://plus.google.com/104539347575272649556/posts" target="_blank"><img src="images/google.png" alt="image not found" width="50px"/></a>
												<a href="https://twitter.com/izonedevelopers" target="_blank"><img src="images/twitter.png" alt="image not found" width="50px"/></a>
												<a href="http://www.izonedevelopers.com/" target="_blank"><img src="images/izone.jpg" alt="image not found" width="70px"/></a>
												</div>
											</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
			</td>
		</tr>
		<tr>
			<td colspan = "2">
				
			</td>
		</tr>
	</table>
	
</div>
<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
</div>
</body>
</html>