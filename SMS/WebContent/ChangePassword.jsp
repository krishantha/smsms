<%@page session="true"%>
<%
if(session.getAttribute("user")==null)
{
response.sendRedirect("welcome.jsp");
}
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
	body{
	width:990px;
	font: 12px/18px Arial, Tahoma, Verdana, sans-serif;
	margin-top: 0;
	margin-left: auto;
	margin-right: auto;
	}
</style>

<style> 
	#toolbar {
		padding: 10px 4px;
	}
</style>

	<!-- imported script files -->
	<script src="js/jquery-1.8.0.min.js"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.toastmessage.js"></script>
    <script type="text/javascript" src="js/jquery.blockUI.js"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
	<script src="js/jquery.jqGrid.min.js" type="text/javascript"></script>
	<script src="js/jquery.bxSlider.min.js" type="text/javascript"></script>
	
	
    
    <!-- imported CSS files -->
    <link href="css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="css/jquery.toastmessage.css" rel="Stylesheet">
    <link href="css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

    
<script type="text/javascript">
  $(document).ready(function(){
    $('#slider').bxSlider();
  });
</script>
    


<script type="text/javascript">
	$(function()
	{
		$("#btnChangePw").button({icons: {primary: "ui-icon-circle-check"}});
		$( "#repeat" ).buttonset();
		$("#btnLogout").button({icons: {primary: "ui-icon-bookmark"}});
	});
</script>

	<script>
		$(function() {
			$( "#accordion1" ).accordion({
				autoHeight: false,
				icons: false
				});
			
		});
	</script>



<script type="text/javascript">

function change()
{
	var valid=true;
	
	
	var username=$("#txtUsername");
	var password=$("#txtPassword");
	var newPassword=$("#txtNewPassword");
	var conPassword=$("#txtConfPassword");
	

	document.getElementById("lblErr").innerHTML="";
	newPassword.removeClass( "ui-state-error");
	username.removeClass( "ui-state-error");
	password.removeClass( "ui-state-error");
	conPassword.removeClass( "ui-state-error");
	document.getElementById('err').style.visibility='hidden';

	if (document.getElementById("txtConfPassword").value == "") {
		
		conPassword.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtPassword").value == "") {
		
		password.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtNewPassword").value == "") {
		
		newPassword.addClass( "ui-state-error" );
		valid = false;
	}
	if (document.getElementById("txtNewPassword").value != document.getElementById("txtConfPassword").value) {
		
		newPassword.addClass( "ui-state-error" );
		conPassword.addClass( "ui-state-error" );
		valid = false;
	}
	if(valid==false)
	{
		if(document.getElementById("txtNewPassword").value =="" || document.getElementById("txtConfPassword").value == "" || document.getElementById("txtPassword").value == "" ){
			document.getElementById("lblErr").innerHTML="Fill all the fields";
			document.getElementById('err').style.visibility='visible';
			$("#err").addClass("ui-state-error ui-corner-all");	
		}
		else if (document.getElementById("txtNewPassword").value != document.getElementById("txtConfPassword").value) {
			
			document.getElementById("lblErr").innerHTML="Passwords do not match";
			document.getElementById('err').style.visibility='visible';
			$("#err").addClass("ui-state-error ui-corner-all");
		}
		
	}
	else
	{
		$(function(){ //jquery function
						
			var jq=$.ajax({  // ajax call
				
				type: "GET",
				url: "UserEdit",
				data: {operation : "edit" ,username : username.val() , password : password.val(), newpassword:newPassword.val()}
			
			});
				
			
			jq.success(function (response,status){
				if(response=="invalid"){
					
					document.getElementById("lblErr").innerHTML="Incorrect password";
					
					document.getElementById('err').style.visibility='visible';
					$("#err").addClass("ui-state-error ui-corner-all");	
					password.addClass( "ui-state-error" );
					
				}
				else if(response=="changed"){
					
					document.getElementById("lblErr").innerHTML="Password Changed";
					document.getElementById('err').style.visibility='visible';
					$("#err").removeClass("ui-state-error");	
					$("#err").addClass("ui-state-highlight ui-corner-all");	

					document.getElementById("txtConfPassword").value="";
					document.getElementById("txtNewPassword").value="";
					document.getElementById("txtPassword").value="";
					
					
					
					}
				else{
					
					document.location='welcome.jsp';

				}
				});
			
			jq.error(function (request,error){//show error message, if any ajax request failure
				
				document.getElementById("lblErr").innerHTML="&nbsp;&nbsp;No response from server";
				$("#err").addClass("ui-state-error ui-corner-all");	
				document.getElementById('err').style.visibility='visible';
			});

		});
	}
}
</script>


<SCRIPT type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</SCRIPT>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>

</head>
<body onload="getuser();noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="">
<div class="ui-widget">
	<div class="ui-widget-header">&nbsp;&nbsp; SMS management system</div>
	
	<div class="ui-widget-content">
	<br>
	<br>
	<div style="margin-left:20px;margin-right:20px;" align="center">


	
	<table width="900" style="padding-left: 22px;">
		<tr>
						
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all"> 
				<span id="repeat"> 
					<input type="radio" id="repeat0" name="repeat" onclick="return document.location='InstantSMS.jsp';"/><label for="repeat0"><a href=InstantSMS.jsp>Send SMS</a></label> 
					<input type="radio" id="repeat1" name="repeat" onclick="return document.location='ScheduledSMS.jsp';"/><label for="repeat1"><a href=ScheduledSMS.jsp>Schedule SMS</a></label> 
					<input type="radio" id="repeat2" name="repeat" onclick="return document.location='Customer.jsp';"/><label for="repeat2"><a href=Customer.jsp>Manage Customer</a></label> 
					<input type="radio" id="repeat3" name="repeat" onclick="return document.location='Group.jsp';"/><label for="repeat3"><a href=Group.jsp>Manage Groups</a></label>
					<input type="radio" id="repeat4" name="repeat" checked="checked"/><label for="repeat4">Settings</label>
				</span> 
			</span> 
			</td>
			<td>
			<span id="toolbar" class="ui-widget-header ui-corner-all">
			<%=session.getAttribute("user") %> <button class="ui-state-default" type="button" id="btnLogout" onclick="return document.location='logout.jsp';" value="edit" >logout</button>
			</span>
			</td>
			
		</tr>
	</table>
	
	<br/><br/>
	
	
	
	<div style="padding: 5px; width: 500px;" align="center">
									<div id="accordion1">
										<h3 align="left"><a href="#">Change Password</a></h3>
									<div>
	<table>		
		
		
		<tr>
			<td>Current Password</td><td style="width: 5px;"></td><td><input type="password" id="txtPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>New Password</td><td style="width: 5px;"></td><td><input type="password" id="txtNewPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
			<td>Confirm New Password</td><td style="width: 5px;"></td><td><input type="password" id="txtConfPassword" style="outline: none;padding: 5px;" class="text ui-widget-content ui-corner-all"/></td><td></td>
		</tr>
		<tr>
		<td  colspan = "3">
		<div id="err" class="ui-widget" style="padding: 0 .7em; visibility: hidden;">
				<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
				<strong>Alert:</strong> <label id="lblErr" ></label></p>
		</div>
		</td>
		</tr>
		<tr>
			<td></td><td style="width: 5px;"></td><td><button type="button" id="btnChangePw" onclick="change();" value="edit" style="float: right; margin-bottom: 20px;">Change</button></td><td></td>
		</tr>
	</table>

	
		
	
	
	</div>
	
	
	</div>
	</div>
	
	
	</div>
	
	<br/>
	<br/>
	
		<div align="center">
		<p>
			&copy;2012 - Izone Developers&trade;
		</p>
		</div>
	</div>
	<div class="ui-widget-header">&nbsp;&nbsp; Izone developers</div>
	
</div>
</body>
</html>